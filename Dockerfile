FROM openjdk:12-alpine
VOLUME /data
COPY ./caselogik-service/build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]