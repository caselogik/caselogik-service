package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Case {

    @JsonCreator
    public Case(
            @JsonProperty("id") Long id,
            @JsonProperty("status") String status,
            @JsonProperty("processId") String processId,
            @JsonProperty("key") String key,
            @JsonProperty("createdAt") String createdAt,
            @JsonProperty("caseNotes") List<CaseNote> caseNotes) {
        this.id = id;
        this.status = status;
        this.processId = processId;
        this.key = key;
        this.createdAt = createdAt;
        this.caseNotes = caseNotes;
    }

    public final Long id;
    public final String status;
    public final String processId;
    public final String key;
    public final String createdAt;
    public final List<CaseNote> caseNotes;
}
