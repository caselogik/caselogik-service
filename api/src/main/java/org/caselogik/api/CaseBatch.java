package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CaseBatch {
    @JsonCreator
    public CaseBatch(
            @JsonProperty("id") Long id,
            @JsonProperty("status") String status,
            @JsonProperty("processId") String processId,
            @JsonProperty("key") String key,
            @JsonProperty("cases")List<Case> cases) {
        this.id = id;
        this.status = status;
        this.processId = processId;
        this.key = key;
        this.cases = cases;
    }

    public final Long id;
    public final String status;
    public final String processId;
    public final String key;
    public final List<Case> cases;
}
