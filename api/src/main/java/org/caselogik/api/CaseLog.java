package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.*;

import java.util.List;

@JsonInclude(Include.NON_NULL)
public class CaseLog {

    public final long caseId;
    public GraphViz graphViz;
    public List<CaseLogEntry> entries;

    public CaseLog(long caseId) {
        this.caseId = caseId;
    }
}
