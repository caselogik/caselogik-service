package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class CaseNote {

    @JsonCreator
    public CaseNote(
            @JsonProperty("id") Long id,
            @JsonProperty("content") String content,
            @JsonProperty("timestamp") LocalDateTime timestamp,
            @JsonProperty("authorId") String authorId,
            @JsonProperty("parentNoteId") Long parentNoteId) {
        this.id = id;
        this.content = content;
        this.timestamp = timestamp;
        this.authorId = authorId;
        this.parentNoteId = parentNoteId;
    }

    public final Long id;
    public final String content;
    public final LocalDateTime timestamp;
    public final String authorId;
    public final Long parentNoteId;
}
