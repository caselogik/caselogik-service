package org.caselogik.api;

import java.time.LocalDateTime;

public interface Event {
    LocalDateTime getTimestamp();
}
