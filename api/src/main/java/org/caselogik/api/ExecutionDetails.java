package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ExecutionDetails {

    public boolean isRetry; //set by the execution gateway
    public boolean isWorkDataUpdated;
    public String lastFailureReason;
    public String lastAttemptAt;
    public String lastProgressUpdateAt;

    @JsonCreator
    public ExecutionDetails(
            @JsonProperty("isRetry") Boolean isRetry,
            @JsonProperty("isWorkDataUpdated") Boolean isWorkDataUpdated,
            @JsonProperty("lastFailureReason") String lastFailureReason,
            @JsonProperty("lastAttemptAt") String lastAttemptAt,
            @JsonProperty("lastProgressUpdateAt") String lastProgressUpdateAt) {
        this.isRetry = isRetry != null? isRetry : false;
        this.isWorkDataUpdated = isWorkDataUpdated != null ? isWorkDataUpdated : false;
        this.lastFailureReason = lastFailureReason;
        this.lastAttemptAt = lastAttemptAt;
        this.lastProgressUpdateAt = lastProgressUpdateAt;

    }
}
