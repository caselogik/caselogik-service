package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class ExternalEvent implements Event {

    //----------------------------
    public ExternalEvent(
            @JsonProperty("name") String name,
            @JsonProperty("key") String key) {

        this.name = name;
        this.key = key;  //anything that identifies this instance of the event
                         // e.g. date of eod event or workdata key for case related event

        this._timeStamp = LocalDateTime.now();
    }

    //----------------------------
    private final String name;
    public String getName() {
        return name;
    }

    //-----------------------------------
    private LocalDateTime _timeStamp;
    @Override
    public LocalDateTime getTimestamp() {
        return _timeStamp;
    }

    //----------------------------
    private final String key;
    public String getKey() {
        return key;
    }

    //--------------------------------
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ExternalEvent { name='")
                .append(name).append("', key='").append(key).append("' }");
        return sb.toString();
    }
}
