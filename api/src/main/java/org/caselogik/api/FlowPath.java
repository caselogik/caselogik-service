package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.*;

@JsonInclude(Include.NON_NULL)
public class FlowPath {

    public final String fromNodeId;
    public final String toNodeId;
    public final String guard;

    public FlowPath(String fromNodeId, String toNodeId, String guard) {
        this.fromNodeId = fromNodeId;
        this.toNodeId = toNodeId;
        this.guard = guard;
    }
}
