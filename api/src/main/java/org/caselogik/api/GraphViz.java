package org.caselogik.api;

public class GraphViz {
    public final String content;
    public final String format;

    public GraphViz(String content, String format) {
        this.content = content;
        this.format = format;
    }
}
