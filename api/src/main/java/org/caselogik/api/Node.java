package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.*;

@JsonInclude(Include.NON_NULL)
public class Node {

    @JsonCreator
    public Node(
            @JsonProperty("id") String id,
            @JsonProperty("name") String name,
            @JsonProperty("type") String type) {
        this.id = id;
        this.type = type;
        this.name = name;
    }

    public final String id;
    public final String type;
    public final String name;
}
