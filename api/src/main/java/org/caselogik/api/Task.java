package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Task {

    @JsonCreator
    public Task(
            @JsonProperty("id") Long id,
            @JsonProperty("processId") String processId,
            @JsonProperty("taskNodeId") String taskNodeId,
            @JsonProperty("workDataType") String workDataType,
            @JsonProperty("workDataJson") String workDataJson,
            @JsonProperty("worker") Worker worker,
            @JsonProperty("referrer") Worker referrer,
            @JsonProperty("status") String status,
            @JsonProperty("isSystemTask") boolean isSystemTask,
            @JsonProperty("isBatchTask") boolean isBatchTask,
            @JsonProperty("isReferred") boolean isReferred,
            @JsonProperty("priority") Integer priority,
            @JsonProperty("createdAt") String createdAt,
            @JsonProperty("executionUrl") String executionUrl,
            @JsonProperty("executionDetails") ExecutionDetails executionDetails,
            @JsonProperty("_links") Map<String, String> _links) {
        this.id = id;
        this.processId = processId;
        this.taskNodeId = taskNodeId;
        this.workDataType = workDataType;
        this.workDataJson = workDataJson;
        this.worker = worker;
        this.referrer = referrer;
        this.status = status;

        this.isSystemTask = isSystemTask;
        this.isBatchTask = isBatchTask;
        this.isReferred = isReferred;

        this.priority = priority;
        this.createdAt = createdAt;
        this.executionUrl = executionUrl;
        this.executionDetails = executionDetails;
        this._links = _links;
    }

    public final Long id;
    public final String processId;
    public final String taskNodeId;
    public final String workDataType;
    public final String workDataJson;
    public final Worker worker;
    public final Worker referrer;
    public final String status;

    public final boolean isSystemTask;
    public final boolean isBatchTask;
    public final boolean isReferred;

    public final Integer priority;
    public final String createdAt;
    public final String executionUrl;
    public final ExecutionDetails executionDetails;
    public final Map<String, String> _links;

    //transient
    public WorkData workData;

    //-------------------------------------
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("Task{");
        s.append("id=").append(id);
        s.append(", processId=").append(processId);
        s.append(", taskNodeId=").append(taskNodeId);
        s.append(", workDataType=").append(workDataType);
        s.append(", workDataJson=").append(workDataJson);
        s.append(", worker=").append(worker);
        s.append(", referrer=").append(referrer);
        s.append(", status=").append(status);
        s.append(", isSystemTask=").append(isSystemTask);
        s.append(", isBatchTask=").append(isBatchTask);
        s.append(", priority=").append(priority);
        s.append(", createdAt=").append(createdAt);
        s.append(", executionUrl=").append(executionUrl);
        s.append("}");
        return s.toString();
    }
}
