package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TaskQueue {

    @JsonCreator
    public TaskQueue(
            @JsonProperty("taskNodeId") String taskNodeId,
            @JsonProperty("taskNodeName") String taskNodeName,
            @JsonProperty("processId") String processId,
            @JsonProperty("actorType") String actorType,
            @JsonProperty("taskCount") long taskCount) {

        this.taskNodeId = taskNodeId;
        this.taskNodeName = taskNodeName;
        this.processId = processId;
        this.actorType = actorType;
        this.taskCount = taskCount;
    }

    public final String taskNodeId;
    public final String taskNodeName;
    public final String processId;
    public final String actorType;
    public final long taskCount;

}
