package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.*;

import java.util.List;

@JsonInclude(Include.NON_NULL)
public class WfProcess {

    public WfProcess(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public final String id;
    public final String name;
    public List<Node> nodes;
    public List<FlowPath> paths;

    public GraphViz graphViz;
}
