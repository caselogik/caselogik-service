package org.caselogik.api;

import java.util.List;

public interface WorkBatch extends WorkData {

    public void setKey(String key);
    public List<? extends WorkItem> getItems();
    public void setItems(List<? extends WorkItem> workItems);
}
