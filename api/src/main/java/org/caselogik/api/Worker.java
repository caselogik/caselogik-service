package org.caselogik.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class Worker {

    @JsonCreator
    public Worker(
            @JsonProperty("id") String id,
            @JsonProperty("type") String type,
            @JsonProperty("attributes") Map<String, String> attributes) {
        this.id = id;
        this.type = type;
        this.attributes = attributes != null? attributes : new HashMap<>();
    }

    public final String id;
    public final String type;
    public final Map<String, String> attributes;

    //-------------------------------------
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("Worker{");
        s.append("id=").append(id);
        s.append(", type=").append(type);
        s.append("}");
        return s.toString();
    }


    //-----------------------------------
    public static Worker human(String id){
        return new Worker(id, "Human", null);
    }

    //-----------------------------------
    public static Worker human(String id, Map<String, String> attributes){
        return new Worker(id,"Human", attributes);
    }

    //---------------------------------------
    public static Worker group(String groupId){
        return new Worker(groupId, "Group", null);
    }

    //-----------------------------------
    public static Worker system(String id){
        return new Worker(id,"System", null);
    }

    //-----------------------------------
    public static Worker externalParty(String id){
        return new Worker(id, "ExternalParty", null);
    }
}