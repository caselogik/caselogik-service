package org.caselogik.api.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ExternalEventResponse {

    public Code code;

    @JsonCreator
    public ExternalEventResponse(
        @JsonProperty("message") Code code) {
        this.code = code;
    }

    public enum Code{
        OK,
        DuplicateEvent
    }
}
