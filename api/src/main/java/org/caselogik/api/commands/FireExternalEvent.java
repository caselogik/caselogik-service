package org.caselogik.api.commands;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FireExternalEvent {

    //----------------------------
    public FireExternalEvent(
            @JsonProperty("eventName") String eventName,
            @JsonProperty("eventKey") String eventKey) {

        this.eventName = eventName;
        this.eventKey = eventKey;
    }

    //----------------------------
    public String eventName;
    public String eventKey;
}
