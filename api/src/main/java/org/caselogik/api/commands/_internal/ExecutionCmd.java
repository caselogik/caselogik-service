package org.caselogik.api.commands._internal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.caselogik.api.Task;

import java.util.Map;

public class ExecutionCmd {

    public final Task task;
    public final Map<String, String> _links;

    @JsonCreator
    public ExecutionCmd(@JsonProperty("task") Task task,
                        @JsonProperty("_links") Map<String, String> _links) {

        this.task = task;
        this._links = _links;
    }

}




