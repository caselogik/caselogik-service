package org.caselogik.api.commands.task;

public class ActivateTask implements TaskCmd {

    public Long taskId;

    @Override
    public Long getTaskId() {
        return taskId;
    }
}
