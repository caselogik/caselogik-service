package org.caselogik.api.commands.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.caselogik.api.Worker;

public class AllocateTask implements TaskCmd {

    @JsonCreator
    public AllocateTask(
            @JsonProperty("toWorker") Worker toWorker) {
        this.toWorker = toWorker;
    }

    public Long taskId;
    public Worker toWorker;

    @Override
    public Long getTaskId() {
        return taskId;
    }
}
