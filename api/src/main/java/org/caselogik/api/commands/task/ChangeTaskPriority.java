package org.caselogik.api.commands.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChangeTaskPriority implements TaskCmd {

    @JsonCreator
    public ChangeTaskPriority(
            @JsonProperty("newPriority") int newPriority) {
        this.newPriority = newPriority;
    }

    public int newPriority;
    public Long taskId;

    @Override
    public Long getTaskId() {
        return taskId;
    }
}
