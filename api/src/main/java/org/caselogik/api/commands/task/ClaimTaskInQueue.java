package org.caselogik.api.commands.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.caselogik.api.Worker;

public class ClaimTaskInQueue implements TaskCmd {

    @JsonCreator
    public ClaimTaskInQueue(
            @JsonProperty("byWorker") Worker byWorker) {

        this.byWorker = byWorker;
    }

    public Long taskId;
    public String processId;
    public String taskNodeId;
    public Worker byWorker;

    @Override
    public Long getTaskId() {
        return taskId;
    }

}
