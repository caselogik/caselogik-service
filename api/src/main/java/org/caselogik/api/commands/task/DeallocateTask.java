package org.caselogik.api.commands.task;

public class DeallocateTask implements TaskCmd {

    public Long taskId;

    @Override
    public Long getTaskId() {
        return taskId;
    }
}
