package org.caselogik.api.commands.task;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.*;
import org.caselogik.api.Task;
import org.caselogik.api.WorkData;

@JsonTypeInfo(use = Id.NAME, property = "_type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = RecordTaskCompleted.class, name = "completed"),
        @JsonSubTypes.Type(value = RecordTaskFailed.class, name = "failed"),
        @JsonSubTypes.Type(value = RecordTaskProgress.class, name = "progress")
})
public abstract class ExecutionStatusUpdate implements TaskCmd {

    public Long taskId;
    public String workDataJson;

    @Override
    public Long getTaskId() {
        return taskId;
    }
}
