package org.caselogik.api.commands.task;

public class InactivateTask implements TaskCmd {

    public Long taskId;

    @Override
    public Long getTaskId() {
        return taskId;
    }
}
