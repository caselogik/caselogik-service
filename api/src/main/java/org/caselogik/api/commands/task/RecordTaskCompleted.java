package org.caselogik.api.commands.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RecordTaskCompleted extends ExecutionStatusUpdate {

    @JsonCreator
    public RecordTaskCompleted(
            @JsonProperty("workDataJson") String workDataJson) {

        this.workDataJson = workDataJson;
    }
}
