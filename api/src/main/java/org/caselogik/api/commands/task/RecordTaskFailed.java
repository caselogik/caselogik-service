package org.caselogik.api.commands.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class RecordTaskFailed extends ExecutionStatusUpdate {

    @JsonCreator
    public RecordTaskFailed(
            @JsonProperty("reason") String reason,
            @JsonProperty("workDataJson") String workDataJson) {

        this.reason = reason;
        this.workDataJson = workDataJson;
    }

    public final String reason;



}
