package org.caselogik.api.commands.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RecordTaskProgress extends ExecutionStatusUpdate {

    @JsonCreator
    public RecordTaskProgress(
            @JsonProperty("workDataJson") String workDataJson) {

        this.workDataJson = workDataJson;
    }

}
