package org.caselogik.api.commands.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RecordTaskReferralCompleted extends ExecutionStatusUpdate {

    @JsonCreator
    public RecordTaskReferralCompleted(
        @JsonProperty("workDataJson") String workDataJson) {

        this.workDataJson = workDataJson;
    }

}
