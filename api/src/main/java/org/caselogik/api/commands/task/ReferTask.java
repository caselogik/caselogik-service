package org.caselogik.api.commands.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.caselogik.api.Worker;

public class ReferTask implements TaskCmd {

    @JsonCreator
    public ReferTask(
            @JsonProperty("toWorker") Worker toWorker) {
        this.toWorker = toWorker;
    }

    public Long taskId;
    public Worker toWorker;

    @Override
    public Long getTaskId() {
        return taskId;
    }
}
