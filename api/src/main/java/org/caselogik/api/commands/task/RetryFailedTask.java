package org.caselogik.api.commands.task;

public class RetryFailedTask implements TaskCmd {

    public Long taskId;

    @Override
    public Long getTaskId() {
        return taskId;
    }

}

