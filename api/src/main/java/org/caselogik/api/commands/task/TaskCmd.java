package org.caselogik.api.commands.task;

public interface TaskCmd {
    Long getTaskId();
}
