package org.caselogik.api.commands.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TaskCmdResponse implements TaskCmd {

    @JsonCreator
    public TaskCmdResponse(
            @JsonProperty("taskId") Long taskId,
            @JsonProperty("message") String message) {
        this.taskId = taskId;
        this.message = message;
    }

    public Long taskId;
    public String message;

    @Override
    public Long getTaskId() {
        return taskId;
    }

}
