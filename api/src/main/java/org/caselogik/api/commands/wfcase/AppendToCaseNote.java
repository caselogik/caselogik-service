package org.caselogik.api.commands.wfcase;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AppendToCaseNote implements CaseCmd {

    @JsonCreator
    public AppendToCaseNote(
            @JsonProperty("noteContent") String noteContent,
            @JsonProperty("noteAuthorId") String noteAuthorId,
            @JsonProperty("parentNoteId") Long parentNoteId,
            @JsonProperty("caseId") Long caseId) {

        this.noteContent = noteContent;
        this.noteAuthorId = noteAuthorId;
        this.parentNoteId = parentNoteId;
        this.caseId = caseId;
    }

    public String noteContent;
    public String noteAuthorId;
    public Long parentNoteId;
    public Long caseId;

    @Override
    public Long getCaseId() {
        return caseId;
    }
}

