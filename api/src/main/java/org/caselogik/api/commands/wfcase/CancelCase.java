package org.caselogik.api.commands.wfcase;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CancelCase implements CaseCmd {

    @JsonCreator
    public CancelCase(@JsonProperty("caseId") Long caseId) {
        this.caseId = caseId;
    }

    public Long caseId;

    @Override
    public Long getCaseId() {
        return caseId;
    }
}

