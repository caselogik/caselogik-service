package org.caselogik.api.commands.wfcase;

public interface CaseBatchCmd {
    Long getCaseBatchId();
}
