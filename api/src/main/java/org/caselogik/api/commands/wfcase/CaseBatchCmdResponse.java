package org.caselogik.api.commands.wfcase;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CaseBatchCmdResponse {

    @JsonCreator
    public CaseBatchCmdResponse(
            @JsonProperty("caseBatchId") Long caseBatchId,
            @JsonProperty("message") String message) {
        this.caseBatchId = caseBatchId;
        this.message = message;
    }

    public Long caseBatchId;
    public String message;

}


