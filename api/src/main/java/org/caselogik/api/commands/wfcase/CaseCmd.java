package org.caselogik.api.commands.wfcase;

public interface CaseCmd {
    Long getCaseId();
}
