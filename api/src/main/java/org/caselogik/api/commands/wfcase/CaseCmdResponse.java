package org.caselogik.api.commands.wfcase;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CaseCmdResponse {

    @JsonCreator
    public CaseCmdResponse(
            @JsonProperty("caseId") Long caseId,
            @JsonProperty("message") String message) {
        this.caseId = caseId;
        this.message = message;
    }

    public Long caseId;
    public String message;
}


