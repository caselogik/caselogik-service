package org.caselogik.api.commands.wfcase;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateCase implements CaseCmd {

    @JsonCreator
    public CreateCase(
            @JsonProperty("processId") String processId,
            @JsonProperty("workItemJson") String workItemJson,
            @JsonProperty("caseId") Long caseId) {

        this.processId = processId;
        this.workItemJson = workItemJson;
        this.caseId = caseId;
    }

    public String processId;
    public String workItemJson;
    public Long caseId;

    @Override
    public Long getCaseId() {
        return caseId;
    }
}

