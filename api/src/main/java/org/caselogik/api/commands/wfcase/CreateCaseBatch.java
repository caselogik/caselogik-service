package org.caselogik.api.commands.wfcase;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateCaseBatch implements CaseBatchCmd {

    @JsonCreator
    public CreateCaseBatch(
            @JsonProperty("processId") String processId,
            @JsonProperty("workBatchJson") String workBatchJson,
            @JsonProperty("caseBatchId") Long caseBatchId) {

        this.processId = processId;
        this.workBatchJson = workBatchJson;
        this.caseBatchId = caseBatchId;

    }

    public String processId;
    public String workBatchJson;
    public Long caseBatchId;

    @Override
    public Long getCaseBatchId() { return caseBatchId;  }
}

