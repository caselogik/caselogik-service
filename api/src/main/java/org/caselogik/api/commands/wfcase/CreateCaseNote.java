package org.caselogik.api.commands.wfcase;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateCaseNote implements CaseCmd {

    @JsonCreator
    public CreateCaseNote(
            @JsonProperty("noteContent") String noteContent,
            @JsonProperty("noteAuthorId") String noteAuthorId,
            @JsonProperty("caseId") Long caseId) {

        this.noteContent = noteContent;
        this.noteAuthorId = noteAuthorId;
        this.caseId = caseId;
    }

    public String noteContent;
    public String noteAuthorId;
    public Long caseId;

    @Override
    public Long getCaseId() {
        return caseId;
    }
}

