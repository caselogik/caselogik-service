Clear tables
--------------------------
delete from task;
delete from token;
delete from wi_envelope;
delete from wf_case;
delete from case_batch;

**Make sure FK 'On DELETE' constraint is set to 'CASCADE'


Select data
--------------------------
select * from wf_case;
select * from token;
select * from task;
select * from case_batch;
select * from wi_envelope;



Setup Queue
-------------------------

1. Create broker
set ARTEMIS_HOME=C:\Dev\Programs\apache-artemis-2.6.3
set CASELOGIK_HOME=C:\Dev\Code\Caselogik
cd %CASELOGIK_HOME%
md artemis
cd artemis
"%ARTEMIS_HOME%\bin\artemis" create caselogik

2. Install as windows service
  "%CASELOGIK_HOME%\artemis\caselogik\bin\artemis-service.exe" install
  "%CASELOGIK_HOME%\artemis\caselogik\bin\artemis-service.exe" start

3. Stop the windows service
   "%CASELOGIk_HOME%\artemis\caselogik\bin\artemis-service.exe" stop

4. To uninstall the windows service
   "%CASELOGIk_HOME%\artemis\caselogik\bin\artemis-service.exe" uninstall
