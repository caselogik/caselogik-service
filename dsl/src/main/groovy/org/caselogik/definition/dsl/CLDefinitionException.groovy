package org.caselogik.definition.dsl

class CLDefinitionException extends RuntimeException{

    int code;
    String [] args

    //-----------------------------------------------------------------
    CLDefinitionException(int code, String msgTemplate, String ... args) {
        super(String.format(msgTemplate, args))
        this.code = code
        this.args = args
    }

    //-----------------------------------------------------------------
    CLDefinitionException(int code, String msg) {
        super(msg)
        this.code = code
    }

    //-----------------------------------------------------------------
    CLDefinitionException(String script, Throwable cause) {
        super("Error while loading script" + script, cause)
        this.code = UNKNOWN_ERROR
    }
    //************************************************
    //Process
    static final int MULTIPLE_PROCS_IN_FILE = 1
    static final int INVALID_PROC_ID = 2

    //WorkItem
    static final int WORKDATA_MISSING_KEY = 3
    static final int WORKDATA_KEY_TYPE_WRONG = 4
    static final int WORKBATCH_MISSING_WORKITEMS = 5
    static final int WORKBATCH_WORKITEMS_TYPE_WRONG = 6
    static final int CLASS_NOT_WORKITEM_OR_WORKBATCH = 7
    static final int WORKDATA_CLASS_NOT_FOUND = 8

    //Variables
    static final int DUPLICATE_VARIABLE = 11
    static final int BAD_VARIABLE_NAME = 12

    //Swimlanes

    //Node
    static final int INVALID_NODE_ID = 30
    static final int SWIMLANE_INVALID = 31
    static final int DUPLICATE_NODE = 32
    static final int EXECUTION_BY_INVALID = 33
    static final int UNKNOWN_PROPERTY_IN_RULE = 35
    static final int UNKNOWN_PROPERTY_IN_WORKDATA = 36
    static final int UNKNOWN_METHOD_IN_RULE = 37
    static final int END_HAS_BODY = 38

    //Start node
    static final int START_MISSING_WORKITEM = 40

    //Sync Node
    static final int INVALID_SPLIT_NODE = 41
    static final int WRONG_SPLIT_NODE_TYPE = 42

    //Task Node
    static final int EXECUTION_BLOCK_MISSING = 51
    static final int ACTOR_TYPE_MISSING = 52
    static final int WHEN_EVENT_IN_HUMAN_TASK = 54
    static final int WHEN_EVENT_IN_EXTERNAL_TASK = 55
    static final int EXECUTION_URL_IN_EXTERNAL_TASK = 56

    //Wait Node
    static final int MISSING_TRIGGER_IN_WAIT = 61

    //Flow
    static final int FLOW_NODE_INVALID = 81
    static final int VARIABLE_INVALID = 82
    static final int DUPLICATE_PATH = 83
    static final int MAX_IN_PATHS_EXCEEDED = 84
    static final int MAX_OUT_PATHS_EXCEEDED = 85
    static final int IN_PATH_MISSING = 86
    static final int OUT_PATH_MISSING = 87
    static final int POSSIBLE_INFINITE_LOOP_IN_FLOW = 88
    static final int LESS_IN_PATHS_IN_SYNC_OF_ANDGATE = 89
    static final int LESS_OUT_PATHS = 90
    static final int FLOW_SYNTAX_BAD = 91
    static final int OUTPATHS_MUST_HAVE_GUARD = 92
    static final int DUPLICATE_GUARDS = 93
    static final int NOT_END_NODE = 94

    //General errors
    static final int UNKNOWN_TOKEN = 201
    static final int PROPERTY_INVALID = 202
    static final int UNKNOWN_ERROR = 203
}
