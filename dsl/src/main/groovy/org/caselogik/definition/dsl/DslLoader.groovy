package org.caselogik.definition.dsl


import org.caselogik.definition.dsl.model.ProcessDefn
import org.codehaus.groovy.control.CompilerConfiguration
import org.codehaus.groovy.control.MultipleCompilationErrorsException
import org.codehaus.groovy.control.customizers.ImportCustomizer

class DslLoader {


    //-----------------------------------------------------------------------
    void inspectScript(ScriptSource source, List<String> workDataScriptNames) {

        ProcessDefn procDefn = new ProcessDefn()
        procDefn._sourceLocation = source.location
        procDefn._validator = new ProcessDefnValidator(procDefn)
        def binding = new Binding()
        binding.setVariable("procDefn", procDefn)

        // Add imports for script.
        def importCustomizer = new ImportCustomizer()
        importCustomizer.addImport 'org.caselogik.api.WorkItem'
        importCustomizer.addImport 'org.caselogik.api.WorkBatch'

        //Set the base script and import customiser
        def compilerConfiguration = new CompilerConfiguration()
        compilerConfiguration.scriptBaseClass = ProcessScript.class.name
        compilerConfiguration.addCompilationCustomizers(importCustomizer)

        //Create a new groovy shell with the binding and the base script
        def shell = new GroovyShell(binding, compilerConfiguration)

        //Now parse the script again - this time the classes are already loaded
        def script
        try {
            script = shell.parse(source.text)
        }
        catch(Exception e){
            procDefn._validator.translateScriptParseException(e)
        }

        //Run it - to load the process definition
        try{ script.run() }
        catch (MissingMethodException e){} //Expected to fail for pure work data files

        source.containsProcDefn = script.procDefn.id != null
        if(source.containsProcDefn) workDataScriptNames.addAll(script.workDataSources)
    }

    //-------------------------------------------------------------------------------------------------------
    ProcessDefn loadProcessDefinition(ScriptSource source, List<ScriptSource> wiSources, ClassLoader parentCL) {

        //Create a new ProcessDefn. This will hold the process model
        ProcessDefn procDefn = new ProcessDefn()
        procDefn._sourceLocation = source.location
        procDefn._validator = new ProcessDefnValidator(procDefn)

        // Add imports for script.
        def importCustomizer = new ImportCustomizer()
        importCustomizer.addImport 'org.caselogik.api.WorkItem'
        importCustomizer.addImport 'org.caselogik.api.WorkBatch'


        //Set the base script and import customiser
        def compilerConfiguration = new CompilerConfiguration()
        compilerConfiguration.scriptBaseClass = ProcessScript.class.name
        compilerConfiguration.addCompilationCustomizers(importCustomizer)

        //Set the model and the class loader to the binding object
        def binding = new Binding()
        binding.setVariable("procDefn", procDefn)

        //Create a new groovy shell with the binding and the base script
        def shell = new GroovyShell(parentCL, binding, compilerConfiguration)
        procDefn._shell = shell

        //Load the workitems classes
        for(src in wiSources) shell.classLoader.parseClass(src.text)

        //Now parse the script again - this time the classes are already loaded
        def script = shell.parse(source.text)

        //Run it - to load the process definition
        script.run()

        //Now actually load the process
        script.loadProcess()

        procDefn._validator.validateWorkItemTypes()

        return procDefn
    }

}
