package org.caselogik.definition.dsl

import org.caselogik.api.WorkBatch
import org.caselogik.api.WorkItem
import org.caselogik.definition.dsl.model.TaskDefn
import org.caselogik.definition.dsl.model.AndGateDefn
import org.caselogik.definition.dsl.model.EndDefn
import org.caselogik.definition.dsl.model.ExecutionDefn
import org.caselogik.definition.dsl.model.FlowNode
import org.caselogik.definition.dsl.model.SyncDefn
import org.caselogik.definition.dsl.model.NodeDefn
import org.caselogik.definition.dsl.model.OrGateDefn
import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.model.StartDefn
import org.caselogik.definition.dsl.model.WaitDefn
import org.caselogik.definition.dsl.model.WorkSplitDefn
import org.codehaus.groovy.control.MultipleCompilationErrorsException
import org.codehaus.groovy.control.messages.SyntaxErrorMessage

//For things about the process that can be validated only when the whole
// groovy file has been parsed. Also for capturing groovy script exceptions
// and turning them into DefinitionExceptions with meaningful events
class ProcessDefnValidator {

    private ProcessDefn proc

    //----------------------------------
    ProcessDefnValidator(ProcessDefn proc) {
        this.proc = proc
    }

    //----------------------------------------------
    //Validations on nodes that must done at the end
    void validateNodes(){
        for(NodeDefn defn : proc.nodesMap.values()){
            if(defn instanceof StartDefn){
                if(defn.workItemType == null)
                    throw new CLDefinitionException(CLDefinitionException.START_MISSING_WORKITEM, "Start node '%s' is has no workItem type specficied. %s", defn.id, defn._scriptLocation)
            }
            if(defn instanceof SyncDefn){
                // Sync must point to a valid node that it is watching for flow splits ...
                if(!proc.nodesMap.containsKey(defn.splitNodeId)){
                    throw new CLDefinitionException(CLDefinitionException.INVALID_SPLIT_NODE, "Sync node '%s' refers to an invalid node '%s' in its splitNode property. %s", defn.id, defn.splitNodeId, defn._scriptLocation)
                }
                else {
                    // and the split must be either AndGate or WorkSplit
                    def sn = proc.nodesMap[defn.splitNodeId]
                    if(!(sn instanceof AndGateDefn || sn instanceof WorkSplitDefn)){
                        throw new CLDefinitionException(CLDefinitionException.WRONG_SPLIT_NODE_TYPE, "The node '%s' referred to by the splitNode property of the sync node '%s' must either be an AndGate node or a WorkSplit node. %s", sn.id, defn.id, defn._scriptLocation)
                    }
                }
            }
            else if(defn instanceof TaskDefn){
                // TaskNodes must have an execution block defined
                if(defn.execution==null) throw new CLDefinitionException(CLDefinitionException.EXECUTION_BLOCK_MISSING, "Task node '%s' must have an 'execution' block. %s", defn.id, defn._scriptLocation)
                else {

                    if(defn.execution.actorType == -1) {
                        throw new CLDefinitionException(CLDefinitionException.ACTOR_TYPE_MISSING, "Task node '%s' must have 'by = <human|system|externalParty>' in the execution block. %s", defn.id, defn._scriptLocation)
                    }
                    else {

                        if (defn.execution.actorType == ExecutionDefn.HUMAN) {
                            // Execution trigger cannot be event based
                            if (defn.execution.event != null) throw new CLDefinitionException(CLDefinitionException.WHEN_EVENT_IN_HUMAN_TASK, "Task node '%s' is executed by a 'human' and cannot have a 'when' specified in the 'execution' block. %s", defn.id, defn.execution._scriptLocation)
                        } else if (defn.execution.actorType == ExecutionDefn.EXTERNAL_PARTY) {
                            // Cannot have either event or url
                            if (defn.execution.event != null) throw new CLDefinitionException(CLDefinitionException.WHEN_EVENT_IN_EXTERNAL_TASK, "Task node '%s' is executed by a 'externalParty' and cannot have a 'when' specified in the 'execution' block. %s", defn.id, defn.execution._scriptLocation)
                            if (!defn.execution.url.isEmpty()) throw new CLDefinitionException(CLDefinitionException.EXECUTION_URL_IN_EXTERNAL_TASK, "Task node '%s' is executed by a 'externalParty' and cannot have a 'url' specified in the 'execution' block. %s", defn.id, defn.execution._scriptLocation)
                        }
                    }
                }
            }
            else if(defn instanceof WaitDefn){
                // For a Wait either event or timeout or rule must be specified
                if(defn.timeOut==null && defn.event == null && defn.rule == null) {
                    throw new CLDefinitionException(CLDefinitionException.MISSING_TRIGGER_IN_WAIT, "Wait node '%s' must have one of 'till', 'timeOut' or 'rule' specified. %s", defn.id, defn._scriptLocation)
                }
            }

        }

    }

    //--------------------------------------------------
    //Validations on the flow that must done at the end
    void validateFlow(){
        for(FlowNode flowNode : proc._flowNodesMap.values()) {

            def node = flowNode.node
            // All nodes other than a Start must have an inpath
            if(node._pathCount[0] == 0 && ! (node instanceof StartDefn)){
                throw new CLDefinitionException(CLDefinitionException.IN_PATH_MISSING, "In the flow block the node '%s' has no paths going into it. %s", node.id, flowNode._scriptLocation)
            }
            // All nodes, other than Ends must have at least one outpath
            if(node._pathCount[1] == 0 && ! (node instanceof EndDefn)){
                throw new CLDefinitionException(CLDefinitionException.OUT_PATH_MISSING, "In the flow block the node '%s' has no paths coming out from it. %s", node.id, flowNode._scriptLocation)
            }

            // OrGates and AndGates ...
            if(node instanceof OrGateDefn || node instanceof AndGateDefn) {
                 // must have at least 2 outpaths
                if(node._pathCount[1] < 2)
                    throw new CLDefinitionException(CLDefinitionException.LESS_OUT_PATHS, "In the flow block the sync node '%s' synchronises on a split node but has less than 2 paths going into it. %s", node.id, flowNode._scriptLocation)

                // outpaths must not have duplicate guards
                def guards = flowNode.outPaths.findAll{ !it.guard.isEmpty()}.collect{ it.guard }
                def size = guards.size()
                if(size != guards.unique().size())
                    throw new CLDefinitionException(CLDefinitionException.DUPLICATE_GUARDS, "Paths coming out of '%s' have duplicate guards on them. %s",node.id, flowNode._scriptLocation)
            }
            // For OrGates and AndGates with rules, all paths must have guards
            if(node instanceof OrGateDefn || (node instanceof AndGateDefn && node.rule != null)){
                flowNode.outPaths.forEach{
                    if(it.guard.isEmpty())
                        throw new CLDefinitionException(CLDefinitionException.OUTPATHS_MUST_HAVE_GUARD,"All paths coming out of '%s' must have guards defined on them. %s" ,node.id, flowNode._scriptLocation)
                }
            }

            // For a Sync ...
            if(node instanceof SyncDefn){
                def splitNode = proc.nodesMap[node.splitNodeId]
                // if it is merging an AndGate, it must have at least 2 inpaths
                if(splitNode instanceof AndGateDefn && node._pathCount[0] < 2) {
                    throw new CLDefinitionException(CLDefinitionException.LESS_IN_PATHS_IN_SYNC_OF_ANDGATE, "In the flow block the split node '%s' has less than 2 paths going out from it. %s}", node.id, flowNode._scriptLocation)
                }
            }

        }

        //testFlowForLoop(proc.startFlowNode) -TODO


    }

    //-------------------------------------------------
    //TODO
    private void testFlowForLoop(FlowNode fn){
        if(!fn._isVisited){
            fn.outPaths.forEach{
                fn._isVisited = true
                testFlowForLoop(it.to)
            }
        }
        else {
            if(fn.node instanceof OrGateDefn) {
                fn.outPaths.forEach{
                    testFlowForLoop(it.to)
                }
            }
            else {
                throw new CLDefinitionException(CLDefinitionException.POSSIBLE_INFINITE_LOOP_IN_FLOW, "Infinite loop detected in flow. Node '%s' being visited repeatedly. %s",fn.id, fn._scriptLocation)
            }
        }
    }

    //-------------------------------------------
    void validateWorkItemTypes(){
        def wiMap = proc.workItems as HashMap
        for(clazz in wiMap.values()){
            if(!WorkItem.class.isAssignableFrom(clazz) && !WorkBatch.class.isAssignableFrom(clazz)){
                throw new CLDefinitionException(CLDefinitionException.CLASS_NOT_WORKITEM_OR_WORKBATCH, "The class '%s' must implement either 'WorkItem' or 'WorkBatch'. %s", clazz.canonicalName, proc._sourceLocation)
            }
        }
    }

    //---------------------------------------------------------
    void translateScriptParseException(Exception e) {
        int code = -1
        Throwable cause = e
        def wiClass = ""
        def errorMsgTemplate = ""

        if(e instanceof MultipleCompilationErrorsException){
            def mce = e as MultipleCompilationErrorsException
            def ex = (mce.errorCollector.errors[0] as SyntaxErrorMessage).cause

            //Field id is missing
            if(ex.message.contains("the method 'java.lang.String getKey()' must be implemented")){
                code = CLDefinitionException.WORKDATA_MISSING_KEY
                errorMsgTemplate = "'%s' is a WorkItem and must have a field 'key' of type 'String'. %s"
                wiClass = (ex.message =~ /.*The class '([a-zA-Z0-9_\.]*)' must be.*/)[0][1]
            }

            //WorkBatch workItems is missing
            else if(ex.message.contains("the method 'java.util.List getItems()' must be implemented")){
                code = CLDefinitionException.WORKBATCH_MISSING_WORKITEMS
                errorMsgTemplate = "'%s' is a WorkBatch and must have a field 'items' of type 'List<T implements WorkItem>'. %s"
                wiClass = (ex.message =~ /.*The class '([a-zA-Z0-9_\.]*)' must be.*/)[0][1]
            }

            //Field id has wrong type
            else if(ex.message.contains("incompatible with java.lang.String in WorkItem")){
                code = CLDefinitionException.WORKDATA_KEY_TYPE_WRONG
                errorMsgTemplate = "The field 'key' of '%s' must be of type 'String'. %s"
                wiClass = (ex.message =~ /.*getId\(\) in ([a-zA-Z0-9_\.]*) is incompatible with java\.lang\.String.*/)[0][1]
            }

            //Field workItems field has wrong type
            else if(ex.message.contains("incompatible with java.util.List in WorkBatch")){
                code = CLDefinitionException.WORKBATCH_WORKITEMS_TYPE_WRONG
                errorMsgTemplate = "The field 'items' of '%s' must be of type 'List<T implements WorkItem>'. %s"
                wiClass = (ex.message =~ /.*getWorkItems\(\) in ([a-zA-Z0-9_\.]*) is incompatible with java\.util\.List.*/)[0][1]
            }

            //Couldn't find work data class definition
            else if(ex.message.contains("unable to resolve class")){
                code = CLDefinitionException.WORKDATA_CLASS_NOT_FOUND
                errorMsgTemplate = "The class '%s' could not be resolved. Have you defined/included it as a WorkItem or WorkBatch?'. %s"
                wiClass = (ex.message =~ /.*unable to resolve class ([a-zA-Z0-9_\.]*).*/)[0][1]
            }
        }

        throw new CLDefinitionException(code, errorMsgTemplate, wiClass, proc._sourceLocation)

    }

   //---------------------------------------------------------
    void translateScriptLoadException(Exception e){

        def loc = getScriptLocation(e)

        if(e instanceof CLDefinitionException){
            throw new CLDefinitionException(e.code, e.message+". "+loc)
        }
        else {
            int code = -1
            String errMsgTemplate = ""
            String [] args = null
            boolean handled = false

            if (e instanceof MissingMethodException) {
                if (e.type.simpleName == "NodeSetDelegate"
                        && e.method == "end"
                        && e.arguments.length > 1
                        && e.arguments[1].class.simpleName.startsWith("_closure")) {
                    code = CLDefinitionException.END_HAS_BODY
                    errMsgTemplate = "The node 'end' cannot have a body. %s"
                    args = [loc]
                } else if (e.type.simpleName == "FlowNode"
                        && e.method == "rightShift") {
                    code = CLDefinitionException.FLOW_SYNTAX_BAD
                    errMsgTemplate = "Invalid syntax in flow (%s). Correct syntax is - node('node_1') >> node('node_2'). %s"
                    args = [e.arguments[0].toString(), loc]
                } else {
                    code = CLDefinitionException.UNKNOWN_TOKEN
                    errMsgTemplate = "Invalid syntax - unknown token '%s'. %s"
                    args = [e.method, loc]
                }

                handled = true

            }
            else if (e instanceof MissingPropertyException) {
                if (e.property == "outputType" || e.property == "inputType") {
                    def item = e.type.simpleName.replace("Delegate", "")
                    code = CLDefinitionException.PROPERTY_INVALID
                    errMsgTemplate = "%s cannot have '%s' in its definition. %s"
                    args = [item, e.property, loc]
                } else if (e.type.simpleName == "FlowDelegate") {
                    code = CLDefinitionException.FLOW_SYNTAX_BAD
                    errMsgTemplate = "Invalid syntax in flow (%s). Correct syntax is - node('node_1') >> node('node_2'). %s"
                    args = [e.property, loc]
                } else if (e.type.simpleName == "StartDelegate"){
                    code = CLDefinitionException.UNKNOWN_TOKEN
                    errMsgTemplate = "Unknown token '%s'. Is this the name of a WorkItem or WorkBatch? If so please define or import it. %s"
                    args = [e.property, loc]
                } else {
                    code = CLDefinitionException.UNKNOWN_TOKEN
                    errMsgTemplate = "Invalid syntax - unknown token '%s'. %s"
                    args = [e.property, loc]
                }

                handled = true
            }

            if(handled) throw new CLDefinitionException(code, errMsgTemplate, args)
            else throw new CLDefinitionException(e)

        }
    }


    //------------------------------------------------------
    // Figures out the file and line number in the script
    // at which this method was called. Used for computing
    // the _sourceLocation value of defn elements so that
    // they can be used later by validation methods that run
    // after the whole script has been processed.
    String getScriptLocationForThisPoint(){
        return getScriptLocation(new RuntimeException("dummy exception"))
    }

    //-----------------------------------------------
    //Given an exception figures out the file and line
    // number at which the error happened
    // TODO - doesn't work for embedded script files
    private String getScriptLocation(Exception ex){
        StackTraceElement posEl = null
        for(StackTraceElement el : ex.stackTrace) {
            //doCall() seems to be the method groovy uses to execute closure
            if(el.methodName=="doCall") {
                posEl = el //that is the StackTraceElement with the correct line number
                break
            }
        }
        return "($proc._sourceLocation:$posEl.lineNumber)" //_scriptFile is set when the process gets loaded
    }

    //---------------------------------
    static boolean isValidId(String id){
        return (id ==~ /[a-zA-Z0-9_\-]*/)
    }
}
