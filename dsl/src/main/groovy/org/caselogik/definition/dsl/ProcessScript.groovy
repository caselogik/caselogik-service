package org.caselogik.definition.dsl


import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.delegates.ProcessDelegate
import org.caselogik.definition.dsl.delegates.ProcessDelegate

abstract class ProcessScript extends Script {

    //---------------------------------------
    Closure processClosure
    ProcessDefn procDefn

    //Sets up the proc defn
    def process(String id, Closure cl) {

        if(procDefn!=null)
            throw new CLDefinitionException(
                    CLDefinitionException.MULTIPLE_PROCS_IN_FILE,
                    "Process '%s' cannot be parsed as this file already contains a process defintion for '%s'. Each file must define a single process.",
                    id, procDefn.id
            )

        procDefn = this.binding.procDefn

        if(!ProcessDefnValidator.isValidId(id)){
            throw new CLDefinitionException(
                    CLDefinitionException.INVALID_PROC_ID,
                    "Process id '%s' is invalid. Id can only contain letters, numbers and '_' .",
                    id
            )
        }

        procDefn.id = id
        cl.delegate = new ProcessDelegate(procDefn)
        cl.resolveStrategy = Closure.DELEGATE_ONLY
        processClosure = cl
    }

    //--------------------------------
    Set<String> workDataSources = new HashSet<>()
    def workDataFrom(String sourceName){
        workDataSources.add(sourceName)
    }


    //---------------------------------
    //Actually evaluate the proc defn closure
    def loadProcess(){
        try {
            processClosure()
        }
        catch(Exception e){
            procDefn._validator.translateScriptLoadException(e)
        }
    }

}

