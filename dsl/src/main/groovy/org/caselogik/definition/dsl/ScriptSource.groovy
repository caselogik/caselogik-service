package org.caselogik.definition.dsl

abstract class ScriptSource {
    String type
    String id
    String location
    boolean containsProcDefn;
    abstract String getText()
}

//===========================================
class FileScriptSource extends ScriptSource {

    FileScriptSource(String path){
        type = "file"
        location = path
        source = new File(path)
        id = source.name
    }

    File source;

    @Override
    def String getText() {
        return source.text
    }
}

//===========================================
class TestScriptSource extends ScriptSource {

    private String text;
    TestScriptSource(String text, String name){
        this.text = text
        this.type = "test"
        this.id = name
    }

    @Override
    String getText() {
        return text
    }
}