package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.AndGateDefn
import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.model.RuleDefn

class AndGateDelegate extends NodeDelegate {

    //-------------------------------------------
    private AndGateDefn andGate
    private ProcessDefn proc

    AndGateDelegate(AndGateDefn andGate, ProcessDefn proc) {
        super(andGate)
        this.andGate = andGate
        this.proc = proc
    }

    //-----------------------------------
    void rule(Closure cl){
        andGate.rule = new RuleDefn(cl)
        andGate.rule._scriptLocation = proc._validator.scriptLocationForThisPoint
    }
}