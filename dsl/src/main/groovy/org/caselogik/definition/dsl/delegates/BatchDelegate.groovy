package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.BatchDefn
import org.caselogik.definition.dsl.model.EventDefn
import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.model.RuleDefn

class BatchDelegate extends NodeDelegate {

    //---------------------------------
    private BatchDefn batch
    private ProcessDefn proc

    BatchDelegate(BatchDefn batch, ProcessDefn proc) {
        super(batch)
        this.batch = batch
        this.proc = proc
    }

    //----------------------------
    void setWhen(EventDefn event){
        batch.event = event
    }

    //-----------------------------
    void setWorkBatch(Class clazz){
        batch.workBatchType = clazz
    }
    //----------------------------
    EventDefn onEvent(String eventName){
        def e = new EventDefn(eventName)
        e._scriptLocation = proc._validator.scriptLocationForThisPoint
        return e
    }

    //-----------------------------------
    void rule(Closure cl){
        batch.rule = new RuleDefn(cl)
        batch.rule._scriptLocation = proc._validator.scriptLocationForThisPoint
    }

}