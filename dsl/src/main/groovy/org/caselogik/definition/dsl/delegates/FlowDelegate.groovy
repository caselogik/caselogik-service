package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.EndDefn
import org.caselogik.definition.dsl.model.FlowNode
import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.CLDefinitionException

class FlowDelegate  {

    private ProcessDefn proc

    //------------------------------
    FlowDelegate(ProcessDefn proc){
        this.proc = proc
        start = proc.startFlowNode
        end = proc._defaultEndFlowNode
    }

    //-------------------------
    static FlowNode start
    static FlowNode end

    //-------------------------
    FlowNode N(String id){

        FlowNode node
        if(!proc._flowNodesMap.containsKey(id)) {

            if(!proc.nodesMap.containsKey(id)){
                throw new CLDefinitionException(CLDefinitionException.FLOW_NODE_INVALID, "Unknown node '%s' used in the flow block. Check that it has been defined under nodes", id)
            }
            node = new FlowNode(id, proc.nodesMap[id])
            node._scriptLocation = proc._validator.scriptLocationForThisPoint
            proc._flowNodesMap.put(id, node)
        }
        return proc._flowNodesMap[id]
    }

    //---------------------------
    FlowNode end(String id){
        FlowNode endFn = N(id)
        if(!(endFn.node instanceof EndDefn)){
            throw new CLDefinitionException(CLDefinitionException.NOT_END_NODE, "Node '%s' used in the flow block is not of type 'end'", id)
        }
        return endFn
    }
}


