package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.NodeDefn

abstract class NodeDelegate {

    NodeDefn node

    //-------------------
    void setName(String name){
        node.name = name
    }

    //-------------------
    NodeDelegate(NodeDefn node){
        this.node = node
    }

}




