package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.ProcessDefnValidator
import org.caselogik.definition.dsl.model.BatchDefn
import org.caselogik.definition.dsl.model.Constants
import org.caselogik.definition.dsl.model.StartDefn
import org.caselogik.definition.dsl.model.TaskDefn
import org.caselogik.definition.dsl.model.AndGateDefn
import org.caselogik.definition.dsl.model.EndDefn
import org.caselogik.definition.dsl.model.SyncDefn
import org.caselogik.definition.dsl.model.NodeDefn
import org.caselogik.definition.dsl.model.OrGateDefn
import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.model.WaitDefn
import org.caselogik.definition.dsl.model.WorkSplitDefn
import org.caselogik.definition.dsl.CLDefinitionException

class NodeSetDelegate {

    //-------------------------------------------
    private ProcessDefn proc
    NodeSetDelegate(ProcessDefn proc) {
        this.proc = proc
    }

    //--------------------------------------
    void start(Closure cl) {
        def node = proc.startDefn
        node._scriptLocation =  proc._validator.scriptLocationForThisPoint
        cl.delegate = new StartDelegate(node)
        cl.resolveStrategy = Closure.DELEGATE_ONLY
        cl()
    }

    //-------------------------------
    void end(String id) {
        def node = new EndDefn(id)
        registerNode(node, id)
    }

    //-----------------------------------------
    void orGate(String id, Closure cl) {
        def node = new OrGateDefn()
        registerNode(node, id, cl, new OrGateDelegate(node, proc))
    }

    //-----------------------------------------
    void andGate(String id) {
        def node = new AndGateDefn()
        registerNode(node, id)
    }

    void andGate(String id, Closure cl) {
        def node = new AndGateDefn()
        registerNode(node, id, cl, new AndGateDelegate(node, proc))
    }

    //-----------------------------------------
    void workSplit(String id, Closure cl) {
        def node = new WorkSplitDefn()
        registerNode(node, id, cl, new WorkSplitDelegate(node, proc))
    }

    //-----------------------------------------
    void sync(String id, Closure cl) {
        def node = new SyncDefn()
        registerNode(node, id, cl, new SyncDelegate(node, proc))
    }

    //-----------------------------------------
    void wait(String id, Closure cl) {
        def node = new WaitDefn()
        registerNode(node, id, cl, new WaitDelegate(node, proc))
    }

    //-----------------------------------------
    void batch(String id, Closure cl) {
        def node = new BatchDefn()
        registerNode(node, id, cl, new BatchDelegate(node, proc))
    }

    //-----------------------------------------
    void task(String id, Closure cl) {
        def node = new TaskDefn()
        registerNode(node, id, cl, new TaskNodeDelegate(node, proc))
    }

    //----------------------------------------------------------------------------------
    private void registerNode(NodeDefn node, String id, Closure cl = null, NodeDelegate del = null){

        validateNodeId(node, id) //check id is valid

        node.id = id
        node.proc = proc
        node._scriptLocation =  proc._validator.scriptLocationForThisPoint
        if (proc.nodesMap.containsKey(node.id))
            throw new CLDefinitionException(CLDefinitionException.DUPLICATE_NODE, "Duplicate node with id '%s' in nodes block.", node.id)
        proc.nodesMap.put(node.id, node)
        if(cl!= null) {
            cl.delegate = del
            cl.resolveStrategy = Closure.DELEGATE_ONLY
            cl()
        }
    }

    //---------------------------------------------------
    private void validateNodeId(NodeDefn node, String id){

        if(!(this instanceof StartDefn || this instanceof EndDefn)
                && [Constants.NodeIds.startNode, Constants.NodeIds.defaultEndNode].contains(id)) {
            throw new CLDefinitionException(
                    CLDefinitionException.INVALID_NODE_ID,
                    "Node id '%s' is reserved for internal use",
                    id
            )
        }

        if(!ProcessDefnValidator.isValidId(id)){
            throw new CLDefinitionException(
                    CLDefinitionException.INVALID_NODE_ID,
                    "Node id '%s' is invalid. Id can only contain letters, numbers and these characters - '_', '-'.",
                    id
            )
        }

    }

}