package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.OrGateDefn
import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.model.RuleDefn

class OrGateDelegate extends NodeDelegate {

    //-------------------------------------------
    private OrGateDefn orGate
    private ProcessDefn proc

    OrGateDelegate(OrGateDefn orGate, ProcessDefn proc) {
        super(orGate)
        this.orGate = orGate
        this.proc = proc
    }

    //-----------------------------------
    void rule(Closure cl){
        orGate.rule = new RuleDefn(cl)
        orGate.rule._scriptLocation = proc._validator.scriptLocationForThisPoint
    }
}