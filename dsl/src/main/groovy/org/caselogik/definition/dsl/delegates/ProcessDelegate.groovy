package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.ProcessDefn

class ProcessDelegate {

    ProcessDefn proc

    //---------------
    void setName(String name){
        proc.name = name
    }

    //-------------------------------
    ProcessDelegate(ProcessDefn proc){
        this.proc = proc
    }

    //--------------------------------------------
    void variables(Closure cl) {
        cl.delegate = new VariableSetDelegate(proc)
        cl.resolveStrategy = Closure.DELEGATE_ONLY
        cl()
    }

    //--------------------------------------------
    void nodes(Closure cl) {
        cl.delegate = new NodeSetDelegate(proc)
        cl.resolveStrategy = Closure.DELEGATE_ONLY
        cl()
        proc._validator.validateNodes()
    }

    //--------------------------------------------
    void swimlanes(Closure cl) {
        cl.delegate = new SwimlaneSetDelegate(proc)
        cl.resolveStrategy = Closure.DELEGATE_ONLY
        cl()
    }

    //---------------------------------------------
    void flow(Closure cl) {
        cl.delegate = new FlowDelegate(proc)
        cl.resolveStrategy = Closure.DELEGATE_ONLY
        cl()
        proc._validator.validateFlow()
    }
}
