package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.Constants
import org.caselogik.definition.dsl.model.EventDefn
import org.caselogik.definition.dsl.model.StartDefn

class StartDelegate extends NodeDelegate {

    private StartDefn startNode
    static EventDefn immediately = new EventDefn(Constants.EventNames.caseCreated)

    StartDelegate(StartDefn startNode){
        super(startNode)
        this.startNode = startNode
    }

    //-------------------------
    void setWorkItem(Class clazz){
        startNode.workItemType = clazz
    }

    //----------------------------
    EventDefn onEvent(String eventName){
        def e = new EventDefn(eventName)
        e._scriptLocation = proc._validator.scriptLocationForThisPoint
        return e
    }
}