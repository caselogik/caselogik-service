package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.model.SwimlaneDefn

class SwimlaneDelegate {

    private SwimlaneDefn swimlane
    private ProcessDefn proc

    SwimlaneDelegate(SwimlaneDefn swimlane, ProcessDefn proc) {
        this.proc = proc
        this.swimlane = swimlane
    }

    //------------------------------
    void setGroups(List<String> groups){
        swimlane.groups = groups
        swimlane.type = SwimlaneDefn.TYPE_INTERNAL
    }

    //------------------------------
    void setCalendarUrl(String calUrl){
        swimlane.calendarUrl = calUrl
        swimlane.type = SwimlaneDefn.TYPE_INTERNAL
    }

}