package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.model.SwimlaneDefn

class SwimlaneSetDelegate {

    private ProcessDefn proc

    //-------------------------------------------
    SwimlaneSetDelegate(ProcessDefn proc) {
        this.proc = proc
    }

    //-------------------------
    void swimlane(String id, Closure cl){
        SwimlaneDefn swimlane = new SwimlaneDefn()
        swimlane.id = id
        swimlane._scriptLocation = proc._validator.scriptLocationForThisPoint
        proc.swimlanesMap.put(id, swimlane)

        if(cl != null) {
            cl.delegate = new SwimlaneDelegate(swimlane, proc)
            cl.resolveStrategy = Closure.DELEGATE_ONLY
            cl()
        }

    }

    //-------------------------
    void swimlane(String id) {
        swimlane(id, null)
    }
}
