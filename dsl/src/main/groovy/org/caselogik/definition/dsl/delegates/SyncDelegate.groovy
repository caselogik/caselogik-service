package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.SyncDefn
import org.caselogik.definition.dsl.model.ProcessDefn

class SyncDelegate extends NodeDelegate {

    //---------------------------------
    private SyncDefn sync
    private ProcessDefn proc

    SyncDelegate(SyncDefn sync, ProcessDefn proc) {
        super(sync)
        this.sync = sync
        this.proc = proc
    }

    //------------------------------
    void setSplitNode(String nodeId){
        sync.splitNodeId = nodeId
    }

}