package org.caselogik.definition.dsl.delegates


import org.caselogik.definition.dsl.model.PriorityDefn
import org.caselogik.definition.dsl.model.Constants
import org.caselogik.definition.dsl.model.EventDefn
import org.caselogik.definition.dsl.model.ExecutionDefn
import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.model.RuleDefn
import org.caselogik.definition.dsl.model.SwimlaneDefn
import org.caselogik.definition.dsl.model.TaskDefn
import org.caselogik.definition.dsl.CLDefinitionException

class TaskNodeDelegate extends NodeDelegate {

    private TaskDefn task
    private ProcessDefn proc

    //-----------------------------------------------
    TaskNodeDelegate(TaskDefn task, ProcessDefn proc) {
        super(task)
        this.task = task
        this.proc = proc
    }

    //------------------------------------
    void setSwimlane(String swimlaneName) {
        SwimlaneDefn slDefn = task.proc.swimlanesMap[swimlaneName]
        if(slDefn == null) throw new CLDefinitionException(CLDefinitionException.SWIMLANE_INVALID, "Swimlane '%s' not defined", swimlaneName)
        task.swimlane = slDefn
    }

    //----------------------------------
    void execution(Closure cl){
        task.execution = new ExecutionDefn()
        task.execution._scriptLocation = proc._validator.scriptLocationForThisPoint
        cl.delegate = new ExecutionDelegate(task, proc)
        cl.resolveStrategy = Closure.DELEGATE_ONLY
        cl()
    }

    //----------------------------
    void priority(Closure cl){
        task.priority = new PriorityDefn()
        task.priority._scriptLocation = proc._validator.scriptLocationForThisPoint
        cl.delegate = new PriorityDelegate(task, proc)
        cl.resolveStrategy = Closure.DELEGATE_ONLY
        cl()
    }
}

//======================================
class ExecutionDelegate {

    static int human = ExecutionDefn.HUMAN
    static int system = ExecutionDefn.SYSTEM
    static int externalParty = ExecutionDefn.EXTERNAL_PARTY

    static EventDefn immediately = new EventDefn(Constants.EventNames.taskCreated)

    private  TaskDefn task
    private ProcessDefn proc

    //------------------------------
    ExecutionDelegate(TaskDefn task, ProcessDefn proc) {
        this.task = task
        this.proc = proc
    }

    //----------------------------
    void setBy(int actorType) {
        if(![ExecutionDefn.HUMAN, ExecutionDefn.SYSTEM, ExecutionDefn.EXTERNAL_PARTY].contains(actorType)){
            throw new CLDefinitionException(CLDefinitionException.EXECUTION_BY_INVALID, "Task node '%s' has an invalid value for 'by' in the 'execution' block. Should be 'human', 'system' or 'external'.", task.id)
        }

        task.execution.actorType = actorType
    }

    //----------------------------
    void setUrl(String url) {
        VariableSetDelegate.checkVariablesInText(url, task.proc.variablesMap.keySet())
        task.execution.url = url
    }

    //----------------------------
    void setWhen(EventDefn event){
        task.execution.event = event
    }

    //----------------------------
    EventDefn onEvent(String eventName){
        def e = new EventDefn(eventName)
        e._scriptLocation = proc._validator.scriptLocationForThisPoint
        return e
    }

}

//===========================================
class PriorityDelegate {

    private  TaskDefn task
    private ProcessDefn proc

    //------------------------------
    PriorityDelegate(TaskDefn task, ProcessDefn proc) {
        this.task = task
        this.proc = proc
    }

    //-----------------------------------
    void rule(Closure cl){
        task.priority.rule = new RuleDefn(cl)
        task.priority.rule._scriptLocation = proc._validator.scriptLocationForThisPoint
    }

}