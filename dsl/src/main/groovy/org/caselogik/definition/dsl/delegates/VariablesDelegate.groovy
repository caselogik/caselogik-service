package org.caselogik.definition.dsl.delegates


import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.CLDefinitionException
import org.caselogik.definition.dsl.model.ConstValueSource
import org.caselogik.definition.dsl.model.EnvVariableSource
import org.caselogik.definition.dsl.model.PropertySource
import org.caselogik.definition.dsl.model.ValueSource
import org.caselogik.definition.dsl.model.VariableDefn

class VariableSetDelegate {

    private ProcessDefn proc
    //-------------------------------------
    VariableSetDelegate(ProcessDefn proc) {
        this.proc = proc
    }

    //-------------------------------------
    Var var(String name){
        if(proc.variablesMap.containsKey(name)) throw new CLDefinitionException(CLDefinitionException.DUPLICATE_VARIABLE,"Duplicate variable '%s'", name)

        //TODO: check for other conditions
        if(name.contains("."))throw new CLDefinitionException(CLDefinitionException.BAD_VARIABLE_NAME,"Bad variable name '%s'", name)

        VariableDefn v = new VariableDefn()
        v.name = name
        v._scriptLocation = proc._validator.scriptLocationForThisPoint
        proc.variablesMap.put(name, v)

        return new Var(v)
    }

    //---------------------------------------------
    EnvVariableSource envVariable(String envVarName){
        EnvVariableSource s = new EnvVariableSource()
        s.envVarName = envVarName
        return s
    }

    //----------------------------------------------
    PropertySource property(String propName){
        PropertySource p = new PropertySource();
        p.propertyName = propName
        return p
    }

    //------------------------------------------------------
    private final static varPattern = ~/\$\{[a-zA-Z0-9-_]*\}/
    static void checkVariablesInText(String txt, Set<String> variables){
        def matcher = varPattern.matcher(txt)
        while (matcher.find()) {
            def varName = txt.substring(matcher.start()+2, matcher.end()-1)
            if(!variables.contains(varName)){
                throw new CLDefinitionException(CLDefinitionException.VARIABLE_INVALID, "Variable '%s' not defined.", varName)
            }
        }

    }
}

//====================================================
class Var {

    private VariableDefn variable

    Var(VariableDefn variable){
        this.variable = variable
    }


    void setTo(String value){
        ConstValueSource src = new  ConstValueSource()
        src.value = value
        variable.source = src
    }

    void setTo(ValueSource source){
        variable.source = source
    }
}

