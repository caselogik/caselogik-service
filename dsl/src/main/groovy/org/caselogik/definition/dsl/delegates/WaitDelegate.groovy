package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.EventDefn
import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.model.WaitDefn

class WaitDelegate extends NodeDelegate {

    //---------------------------------
    private WaitDefn wait
    private ProcessDefn proc

    WaitDelegate(WaitDefn wait, ProcessDefn proc) {
        super(wait)
        this.wait = wait
        this.proc = proc
    }

    void setTimeOut(String duration){
        wait.timeOut = duration
    }

    void setUntil(EventDefn event) {
        wait.event = event
    }

    //----------------------------
    EventDefn onEvent(String eventName){
        def e = new EventDefn(eventName)
        e._scriptLocation = proc._validator.scriptLocationForThisPoint
        return e
    }


}