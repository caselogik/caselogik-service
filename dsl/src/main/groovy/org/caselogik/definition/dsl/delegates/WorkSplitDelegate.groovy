package org.caselogik.definition.dsl.delegates

import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.definition.dsl.model.RuleDefn
import org.caselogik.definition.dsl.model.WorkSplitDefn

class WorkSplitDelegate extends NodeDelegate {

    //-------------------------------------------
    private WorkSplitDefn workSplit
    private ProcessDefn proc

    WorkSplitDelegate(WorkSplitDefn workSplit, ProcessDefn proc) {
        super(workSplit)
        this.workSplit = workSplit
        this.proc = proc
    }

    //-----------------------------------
    void rule(Closure cl){
        workSplit.rule = new RuleDefn(cl)
        workSplit.rule._scriptLocation = proc._validator.scriptLocationForThisPoint
    }
}