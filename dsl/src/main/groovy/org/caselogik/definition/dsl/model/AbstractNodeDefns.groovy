package org.caselogik.definition.dsl.model

import org.caselogik.api.WorkItem
import org.caselogik.definition.dsl.CLDefinitionException

abstract class NodeDefn extends DefnItem {

    //------------------------
    String id

    String getId() { return id }
    void setId(String id){
        this.id = id
    }

    //------------------------
    String name
    Class<? extends WorkItem> workItemType
    ProcessDefn proc


    //-------------------------
    protected MANY = -1

    int[] _pathCount = [0,0] // 0=IN, 1=OUT
    abstract int[] _maxPaths()

    //-------------------------------------------
    void checkPathAddition(int inout, String otherId){
        _pathCount[inout]++
        String n = id.replace('$','')
        String other = otherId.replace('$','')
        int max = _maxPaths()[inout]
        if(max!= MANY) {
            if (_pathCount[inout] > _maxPaths()[inout]) {
                if(inout==0) throw new CLDefinitionException(CLDefinitionException.MAX_IN_PATHS_EXCEEDED, "Cannot flow from '%s' to node '%s' as the max number of paths going into node '%s' is %s.", other, n, n, max.toString())
                else if(inout==1) throw new CLDefinitionException(CLDefinitionException.MAX_OUT_PATHS_EXCEEDED, "Cannot flow out from node '%s' to '%s' as the max number of paths coming out from node '%s' is %s", n, other, n, max.toString)
            }
        }
    }

}



