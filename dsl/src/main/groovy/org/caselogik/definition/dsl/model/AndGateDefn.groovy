package org.caselogik.definition.dsl.model

class AndGateDefn extends NodeDefn {
    RuleDefn rule
    @Override int[] _maxPaths() {  return [MANY,MANY] }
}