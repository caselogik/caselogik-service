package org.caselogik.definition.dsl.model

import org.caselogik.api.WorkBatch

class BatchDefn extends NodeDefn {
    EventDefn event
    Class<WorkBatch> workBatchType
    RuleDefn rule

    @Override int[] _maxPaths() {  return [MANY,1] }
}