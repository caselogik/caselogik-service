package org.caselogik.definition.dsl.model

class Constants {
    static class NodeIds {
        static final String startNode = "start"
        static final String defaultEndNode = "end"
    }

    static class EventNames {
        static final String taskCreated = "TaskCreated"
        static final String caseCreated = "CaseCreated"
    }

}
