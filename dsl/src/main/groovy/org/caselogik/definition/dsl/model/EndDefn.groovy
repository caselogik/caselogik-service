package org.caselogik.definition.dsl.model

class EndDefn extends NodeDefn {

    EndDefn(String id = Constants.NodeIds.defaultEndNode) {
        this.id = id
    }

    @Override int[] _maxPaths() {  return [MANY,0] }

}