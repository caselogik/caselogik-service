package org.caselogik.definition.dsl.model

import org.caselogik.api.Event
import org.caselogik.api.ExternalEvent

class EventDefn extends DefnItem{

    //-----------------------
    String name
    EventDefn(String name) {
        this.name = name
    }

    //---------------------------
    boolean matches(Event event){
        if(event.class == ExternalEvent)
            return (event as ExternalEvent).name == name
        else
            return event.class.simpleName == name
    }

    //---------------------
    @Override
    String toString() {
        return name
    }
}
