package org.caselogik.definition.dsl.model

import org.caselogik.definition.dsl.CLDefinitionException
import org.caselogik.definition.dsl.CLDefinitionException

class FlowNode extends DefnItem {

    String id
    List<FlowPath> outPaths = new ArrayList<>()
    NodeDefn node

    FlowPath _currentPath
    boolean _isVisited = false

    FlowNode(String id, NodeDefn node){
        this.id = id
        this.node = node
    }

    //---------------------------------
    FlowNode rightShift(FlowNode toNode) {

        node.checkPathAddition(1, "node '$toNode.id'")
        toNode.node.checkPathAddition(0, "node '$id'")

        if(_currentPath==null) {  //For node1 >> node2 cases
            _currentPath =  new FlowPath()
            _currentPath.from = this
            if(outPaths.find { it.to.id == toNode.id} != null //if any outpath already leads to this 'to node'
                    && toNode.id != Constants.NodeIds.defaultEndNode) { //and the to node is not a default end

                throw new CLDefinitionException(CLDefinitionException.DUPLICATE_PATH, "Path from node('%s') to node('%s') already exists in flow block", id, toNode.id)
            }
            outPaths.add(_currentPath)
        }
        // else it is a node1 - [guard] >> node2 case. _currentPath was set in minus()

        _currentPath.to = toNode

        //Clear the current path so that it
        // doesn't get used for the next >>
        _currentPath = null

        return toNode
    }

    //------------------------------------
    FlowNode minus(ArrayList<String> guard){

        /*
        Minus has a higher operator precedence than rightShift. So this will be evaluated
        first. As such need to save the path being used as the _currentPath in the node, so that
        when the >> operator supplies the toNode, we know which path to add it to
        */
        node.checkPathAddition(1, "guard [$guard]")

        _currentPath = new FlowPath()
        _currentPath.guard = guard[0]
        _currentPath.from = this
        outPaths.add(_currentPath)
        return this
    }

}