package org.caselogik.definition.dsl.model

class FlowPath extends DefnItem {
    String guard = ""
    FlowNode from
    FlowNode to

    //---------------------------------
    FlowNode rightShifts(FlowNode toNode) {
        toNode.node.checkPathAddition(0, "guard [$guard]")

        this.to = toNode
        return toNode
    }

}
