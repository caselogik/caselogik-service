package org.caselogik.definition.dsl.model

class OrGateDefn extends NodeDefn {
    RuleDefn rule
    @Override int[] _maxPaths() {  return [MANY,MANY] }
}