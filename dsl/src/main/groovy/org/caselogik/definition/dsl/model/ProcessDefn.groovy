package org.caselogik.definition.dsl.model

import org.caselogik.api.WorkItem
import org.caselogik.definition.dsl.ProcessDefnValidator
import org.caselogik.definition.dsl.ProcessDefnValidator

class ProcessDefn {

    String id
    String name
    Map<String, VariableDefn> variablesMap = new HashMap<>()
    Map<String, SwimlaneDefn> swimlanesMap = new HashMap<>()
    Map<String, NodeDefn> nodesMap = new HashMap<>()
    Map<String, FlowNode> _flowNodesMap = new HashMap<>()
    StartDefn startDefn
    FlowNode startFlowNode

    String _sourceLocation
    ProcessDefnValidator _validator
    EndDefn _defaultEndDefn
    FlowNode _defaultEndFlowNode

    //------------------------
    GroovyShell _shell

    //--------------------------------------------------
    private Map<String, Class<WorkItem>> workItems
    Map<String, Class<WorkItem>> getWorkItems(){
        if(workItems == null) {
            workItems = new HashMap<String, Class<WorkItem>>()

            //Get all classes loaded so far and filter out those related to scripts
            def classes = _shell.classLoader.loadedClasses
            for (c in classes) {
                if (!c.name.contains("Script"))
                    workItems.put(c.simpleName, c)
            }
        }
        return workItems
    }

    //------------------------------------------
    ProcessDefn() {
        startDefn = new StartDefn()
        startDefn.proc = this
        nodesMap.put(startDefn.id, startDefn)

        _defaultEndDefn = new EndDefn()
        _defaultEndDefn.proc = this
        nodesMap.put(_defaultEndDefn.id, _defaultEndDefn)

        startFlowNode = new FlowNode(Constants.NodeIds.startNode, startDefn)
        _defaultEndFlowNode = new FlowNode(Constants.NodeIds.defaultEndNode, _defaultEndDefn)
    }
}


