package org.caselogik.definition.dsl.model

class RuleDefn extends DefnItem {

    Closure closure

    RuleDefn(Closure closure){
        this.closure = closure
        this.closure.resolveStrategy = Closure.DELEGATE_ONLY
    }

}

