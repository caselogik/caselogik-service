package org.caselogik.definition.dsl.model

class StartDefn extends NodeDefn {

    @Override int[] _maxPaths() {  return [0,1] }
    StartDefn() {
        this.id = Constants.NodeIds.startNode
    }

}
