package org.caselogik.definition.dsl.model

class SwimlaneDefn extends DefnItem {

    static final int TYPE_EXTERNAL = 0
    static final int TYPE_INTERNAL = 1

    String id
    List<String> groups
    String calendarUrl
    int type
}
