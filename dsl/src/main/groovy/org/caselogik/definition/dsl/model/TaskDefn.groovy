package org.caselogik.definition.dsl.model

class TaskDefn extends NodeDefn {

    SwimlaneDefn swimlane
    ExecutionDefn execution
    PriorityDefn priority

    @Override int[] _maxPaths() {  return [MANY,1] }

}

//====================================
class ExecutionDefn extends DefnItem{
    static final int HUMAN = 1
    static final int SYSTEM = 2
    static final int EXTERNAL_PARTY = 3

    String url = ""
    EventDefn event
    int actorType  = -1
}

//====================================
class PriorityDefn extends DefnItem{
    RuleDefn rule
}