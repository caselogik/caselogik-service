package org.caselogik.definition.dsl.model


class VariableDefn extends DefnItem {

    String name
    ValueSource source

}

//===========================================
interface ValueSource {}

class ConstValueSource implements ValueSource {
    String value
}

class EnvVariableSource implements ValueSource {
    String envVarName
}

class PropertySource implements ValueSource {
    String propertyName
}