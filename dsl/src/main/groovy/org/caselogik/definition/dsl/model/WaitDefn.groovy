package org.caselogik.definition.dsl.model

class WaitDefn extends NodeDefn {
    String timeOut
    EventDefn event
    @Override int[] _maxPaths() {  return [MANY,1] }
}