package dsl


import org.junit.Test
import util.BaseDslTest

import static junit.framework.Assert.assertEquals

class Flow_Tests extends BaseDslTest{

    @Test
    void test1(){


        def s = '''
        process('test') { 
            nodes {
                start { workItem = SimpleWork }
                andGate('and_1')
                end('end_1')
            }
            
            flow {
                start >> N('and_1')-['ok'] >> end
                N('and_1')-['somewhatOK'] >> end('end_1')
            }
        }
        '''

        loadScript(addWorkItemScript(s))

        assertEquals("start", procDefn.getStartFlowNode().node.id)
        assertEquals(1, procDefn.getStartFlowNode().outPaths.size())

        def toNode1 = procDefn.getStartFlowNode().outPaths[0].to
        assertEquals("and_1", toNode1.node.id)
        assertEquals(2, toNode1.outPaths.size())
        assertEquals("ok", toNode1.outPaths[0].guard)
        assertEquals("somewhatOK", toNode1.outPaths[1].guard)

        def toNode2 = toNode1.outPaths[0].to
        def toNode3 = toNode1.outPaths[1].to
        assertEquals("end", toNode2.node.id)
        assertEquals("end_1", toNode3.node.id)
        assertEquals(0, toNode2.outPaths.size)
        assertEquals(0, toNode3.outPaths.size)


    }
}
