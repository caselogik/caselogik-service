package dsl

import org.caselogik.definition.dsl.model.EndDefn
import org.caselogik.definition.dsl.model.StartDefn
import org.junit.Test
import util.BaseDslTest

import static junit.framework.Assert.assertEquals

class Process_Tests extends BaseDslTest {

    @Test
    void test1(){


        def s = '''
        process('test') { }
        '''

        loadScript(s)

        assertEquals("test", procDefn.id)
        assertEquals(2, procDefn.nodesMap.size())
        assertEquals(StartDefn, procDefn.nodesMap["start"].class)
        assertEquals(EndDefn, procDefn.nodesMap["end"].class)
        assertEquals("tests", procDefn._sourceLocation)
        assertEquals("end", procDefn._defaultEndDefn.id)
        assertEquals("end", procDefn._defaultEndFlowNode.id)
        assertEquals("start", procDefn.startDefn.id)

    }

}
