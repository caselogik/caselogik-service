package dsl

import org.caselogik.definition.dsl.model.SwimlaneDefn
import org.junit.Test
import util.BaseDslTest

import static junit.framework.Assert.assertEquals

class Swimlanes_Tests extends BaseDslTest {

    @Test
    void test1(){
        def s = '''
        process('test') { 
            swimlanes {
                swimlane('customer')
                swimlane('channel') {
                    groups = ['customerSupport']
                    calendarUrl = '${calendarUrl}'
                }
                swimlane('finance') {
                    groups = ['registrationFinance']
                    calendarUrl = 'http://calendar'
                }
            }        
        }
        '''

        loadScript(s)

        assertEquals(3, procDefn.swimlanesMap.size())

        SwimlaneDefn s1 = procDefn.swimlanesMap["customer"]
        assertEquals "customer", s1.id

        SwimlaneDefn s2 = procDefn.swimlanesMap["channel"]
        assertEquals("channel", s2.id)
        assertEquals(["customerSupport"], s2.groups)
        assertEquals('''${calendarUrl}''', s2.calendarUrl)

        SwimlaneDefn s3 = procDefn.swimlanesMap["finance"]
        assertEquals("finance", s3.id)
        assertEquals(["registrationFinance"], s3.groups)
        assertEquals("http://calendar", s3.calendarUrl)
    }
}
