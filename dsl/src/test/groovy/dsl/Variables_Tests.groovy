package dsl

import org.caselogik.definition.dsl.model.ConstValueSource
import org.caselogik.definition.dsl.model.EnvVariableSource
import org.caselogik.definition.dsl.model.PropertySource
import org.junit.Test
import util.BaseDslTest

import static junit.framework.Assert.assertEquals

class Variables_Tests extends BaseDslTest {

    @Test
    void test1() {

        def s = '''
        process('test') { 
            variables {
                var('financeUrl') setTo 'https://finance.ros.rov.uk'
                var('channelUrl') setTo property('channel.url')
                var('codeFolder') setTo envVariable('CASELOGIK_CODE_HOME')
            }
        }
        '''
        loadScript(s)
        assertEquals("test", procDefn.id)
        assertEquals(3, procDefn.variablesMap.size())
        assertEquals("https://finance.ros.rov.uk", ((ConstValueSource)procDefn.variablesMap["financeUrl"].source).value)
        assertEquals("channel.url", ((PropertySource)procDefn.variablesMap["channelUrl"].source).propertyName)
        assertEquals("CASELOGIK_CODE_HOME", ((EnvVariableSource)procDefn.variablesMap["codeFolder"].source).envVarName)

    }
}
