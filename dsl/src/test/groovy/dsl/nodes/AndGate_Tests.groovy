package dsl.nodes


import org.caselogik.definition.dsl.model.AndGateDefn
import org.caselogik.definition.dsl.model.RuleDefn

import org.junit.Test
import util.BaseDslTest

import static junit.framework.Assert.assertEquals

class AndGate_Tests extends BaseDslTest{

    @Test
    void test1() {
        def s = '''
        process('test') { 
            nodes {
                start { workItem = SimpleWork }
                andGate('and_1') {
                    rule {
                        if (_workData.isOK) _goVia('ok', 'somewhatOK')
                        else _goVia('notOK')
                    }
                }

            }
        }
        '''
        loadScript(addWorkItemScript(s))

        assertEquals("AndGateDefn", procDefn.nodesMap["and_1"].class.simpleName)

        RuleDefn rule = ((AndGateDefn)procDefn.nodesMap["and_1"]).rule
        def d = new TestRuleDelgate();
        rule.closure.delegate = d
        rule.closure()
        assertEquals( "ok, somewhatOK", d.guards.join(", "))

        d._workData.isOK = false
        rule.closure()
        assertEquals( "notOK", d.guards.join(", "))
    }

    //--------------------------------------
    class TestRuleDelgate {
        String [] guards
        def _workData = new TestWI()
        def _goVia(String ... guards){
            this.guards = guards
        }
    }

    //--------------------------------
    class TestWI {
        boolean isOK = true;
    }
}
