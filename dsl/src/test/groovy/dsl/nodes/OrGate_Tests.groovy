package dsl.nodes


import org.caselogik.definition.dsl.model.OrGateDefn
import org.caselogik.definition.dsl.model.RuleDefn
import org.junit.Test
import util.BaseDslTest

import static junit.framework.Assert.assertEquals

class OrGate_Tests extends BaseDslTest{

    @Test
    public void test1() {
        def s = '''
        process('test') { 
            nodes {
                start { workItem = SimpleWork }
                orGate('or_1') {
                    rule {
                        if(_workData.isOK) _goVia('ok')
                        else _goVia('notOK')
                    }
                }
            }
        }
        '''
        loadScript(addWorkItemScript(s))

        assertEquals("OrGateDefn", procDefn.nodesMap["or_1"].class.simpleName)

        RuleDefn rule = ((OrGateDefn)procDefn.nodesMap["or_1"]).rule
        def d = new TestRuleDelgate();
        rule.closure.delegate = d
        rule.closure()
        assertEquals( "ok", d.guard)

        d._workData.isOK = false
        rule.closure()
        assertEquals( "notOK", d.guard)

    }

    //--------------------------------------
    class TestRuleDelgate {
        String guard
        def _workData = new TestWI()
        def _goVia(String guard){
            this.guard = guard
        }
    }

//--------------------------------
    class TestWI {
        boolean isOK = true;
    }
}

