package dsl.nodes


import org.caselogik.definition.dsl.CLDefinitionException
import org.junit.Assert
import org.junit.Test
import util.BaseDslTest

import static junit.framework.Assert.assertEquals

class Start_Tests extends BaseDslTest{

    @Test
    void test1() {
        def s = '''
        package test
        
        class SimpleWork implements WorkItem{
            String key
            String name
        }
        
        process('test') { 
            nodes {
                start {
                    workItem = SimpleWork
                }
            }
        }
        '''
        loadScript(s)

        assertEquals "start", procDefn.startDefn.id
        assertEquals("SimpleWork", procDefn.startDefn.workItemType.simpleName)
        assertEquals("test", procDefn.startDefn.proc.id)
    }

    @Test
    void testInvalidStart() {
        def s = '''
        package test
        
        process('test') { 
            nodes {
                start {
                    workItem = SimpleWork
                }
            }
        }
        '''
        try {
            loadScript(s)
        }
        catch(CLDefinitionException e){
            Assert.assertEquals(CLDefinitionException.UNKNOWN_TOKEN, e.code)
        }
    }

    @Test
    void testInvalidStart2() {
        def s = '''
        package test
        
        process('test') { 
            nodes {
                start {}
            }
        }
        '''
        try {
            loadScript(s)
        }
        catch(CLDefinitionException e){
            Assert.assertEquals(CLDefinitionException.START_MISSING_WORKITEM, e.code)
        }
    }
}
