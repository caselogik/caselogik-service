package dsl.nodes

import org.caselogik.definition.dsl.model.SyncDefn
import org.junit.Test
import util.BaseDslTest

import static junit.framework.Assert.assertEquals

class Sync_Tests extends BaseDslTest {

    @Test
    void test(){
        def p = """
        process('test') {
            nodes {
                start { workItem = SimpleWork }
                sync('sync_1') {
                    splitNode = 'and_1'
                }
                
                andGate('and_1')
            }
        }
        """

        loadScript(addWorkItemScript(p))

        assertEquals(SyncDefn, procDefn.nodesMap['sync_1'].class)
        SyncDefn md = (SyncDefn)procDefn.nodesMap['sync_1']
        assertEquals("and_1", md.splitNodeId)
        assertEquals("sync_1", md.id)



    }
}
