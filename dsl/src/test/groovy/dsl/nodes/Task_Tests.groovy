package dsl.nodes

import org.caselogik.definition.dsl.CLDefinitionException
import org.caselogik.definition.dsl.model.ExecutionDefn
import org.caselogik.definition.dsl.model.TaskDefn
import org.junit.Test
import util.BaseDslTest

import static junit.framework.Assert.assertEquals

class Task_Tests extends BaseDslTest{

    @Test
    void testSystemTask() {
        def s = '''
        process('test') { 
            swimlanes {
                swimlane('finance')
            }
            
            nodes {
                start { workItem = SimpleWork }
                            
                task('collectPayments') {
                    swimlane = 'finance'
                    execution {
                        by = system
                        when = immediately
                        url = 'abc/1/da.html'
                    }
                }
            }
        }
        '''
        loadScript(addWorkItemScript(s))

        assertEquals("TaskDefn", procDefn.nodesMap["collectPayments"].class.simpleName)

        TaskDefn td = procDefn.nodesMap["collectPayments"]

        assertEquals("finance", td.swimlane.id)
        assertEquals(ExecutionDefn.SYSTEM, td.execution.actorType)
        assertEquals("TaskCreated", td.execution.event.name)
        assertEquals("abc/1/da.html", td.execution.url)

    }

    @Test
    void testHumanTask() {
        def s = '''
        process('test') { 
            swimlanes {
                swimlane('finance')
            }
            
            nodes {
                start { workItem = SimpleWork }
                
                task('collectPayments') {
                    swimlane = 'finance'
                    execution {
                        by = human
                        url = 'http://localhost:8081/prepare-docs\'
                    }
                }
            }
        }
        '''
        loadScript(addWorkItemScript(s))

        assertEquals("TaskDefn", procDefn.nodesMap["collectPayments"].class.simpleName)

        TaskDefn td = procDefn.nodesMap["collectPayments"]

        assertEquals("finance", td.swimlane.id)
        assertEquals(ExecutionDefn.HUMAN, td.execution.actorType)
        assertEquals("http://localhost:8081/prepare-docs", td.execution.url)

    }


    @Test
    void testExternalTask() {
        def s = '''
        process('test') { 
            swimlanes {
                swimlane('finance')
            }
            
            nodes {
                start { workItem = SimpleWork }
                
                task('collectPayments') {
                    swimlane = 'finance'
                    execution {
                        by = externalParty
                    }
                }
            }
        }
        '''
        loadScript(addWorkItemScript(s))

        assertEquals("TaskDefn", procDefn.nodesMap["collectPayments"].class.simpleName)

        TaskDefn td = procDefn.nodesMap["collectPayments"]

        assertEquals("finance", td.swimlane.id)
        assertEquals(ExecutionDefn.EXTERNAL_PARTY, td.execution.actorType)

    }

    @Test
    void testInvalidHumanTask() {
        def s = '''
        process('test') { 
            swimlanes {
                swimlane('finance')
            }
            
            nodes {
                start { workItem = SimpleWork }
                            
                task('collectPayments') {
                    swimlane = 'finance'
                    execution {
                        by = human
                        <PLACEHOLDER>
                    }
                }
            }
        }
        '''

        def s1 = s.replace("<PLACEHOLDER>", "when = immediately") //add 'when = immediately'
        try { loadScript(addWorkItemScript(s1)) }catch(CLDefinitionException e){ assertEquals(CLDefinitionException.WHEN_EVENT_IN_HUMAN_TASK, e.code) }

        def s2 = s.replace("<PLACEHOLDER>", "").replace("by = human", "") //remove 'by = human'
        try { loadScript(addWorkItemScript(s2)) }catch(CLDefinitionException e){ assertEquals(CLDefinitionException.ACTOR_TYPE_MISSING, e.code) }

    }

    @Test
    void testInvalidExternalTask() {
        def s = '''
        process('test') { 
            swimlanes {
                swimlane('finance')
            }
            
            nodes {
                start { workItem = SimpleWork }
                            
                task('collectPayments') {
                    swimlane = 'finance'
                    execution {
                        by = externalParty
                        <PLACEHOLDER>
                    }
                }
            }
        }
        '''

        def s1 = s.replace("<PLACEHOLDER>", "when = immediately")
        try { loadScript(addWorkItemScript(s1)) }catch(CLDefinitionException e){ assertEquals(CLDefinitionException.WHEN_EVENT_IN_EXTERNAL_TASK, e.code) }

        def s2 = s.replace("<PLACEHOLDER>", "url = 'some/url'")
        try { loadScript(addWorkItemScript(s2)) }catch(CLDefinitionException e){ assertEquals(CLDefinitionException.EXECUTION_URL_IN_EXTERNAL_TASK, e.code) }
    }

}
