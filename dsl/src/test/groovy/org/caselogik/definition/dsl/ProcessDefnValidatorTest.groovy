package org.caselogik.definition.dsl;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProcessDefnValidatorTest {

    @Test
    void test1() {

        assertFalse(ProcessDefnValidator.isValidId("test.proc"))
        assertTrue(ProcessDefnValidator.isValidId("test_01"))
        assertTrue(ProcessDefnValidator.isValidId("start"))

    }

}