package util

import org.caselogik.definition.dsl.ProcessDefnValidator
import org.caselogik.definition.dsl.ProcessScript
import org.caselogik.definition.dsl.model.ProcessDefn
import org.codehaus.groovy.control.CompilerConfiguration
import org.codehaus.groovy.control.customizers.ImportCustomizer


abstract class BaseDslTest {

    protected ProcessDefn procDefn;

    protected void loadScript(String scriptSource){

        procDefn = new ProcessDefn()
        procDefn._sourceLocation = "tests"
        procDefn._validator = new ProcessDefnValidator(procDefn)

        def importCustomizer = new ImportCustomizer()
        importCustomizer.addImport 'org.caselogik.api.WorkItem'
        importCustomizer.addImport 'org.caselogik.api.WorkBatch'

        def compilerConfiguration = new CompilerConfiguration()
        compilerConfiguration.scriptBaseClass = ProcessScript.class.name
        compilerConfiguration.addCompilationCustomizers(importCustomizer)
        def binding = new Binding()
        binding.setVariable("procDefn", procDefn)

        //Create a new groovy shell with the binding and the base script
        def shell = new GroovyShell(this.class.classLoader, binding, compilerConfiguration)
        procDefn._shell = shell

        def script = shell.parse(scriptSource)
        script.run()
        script.loadProcess()

    }

    protected String addWorkItemScript(String scriptSource){

        def s = '''
            class SimpleWork implements WorkItem{
                String key
                String name
            }\n
            '''
        return s + scriptSource
    }


}
