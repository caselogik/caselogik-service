package sample

workDataFrom('simple_wis.cl')

//process
process('simple_2') {

    name = 'Simple Application'

    swimlanes {
        swimlane('risk')
        swimlane('finance')
        swimlane('legal')

    }

    //nodes
    nodes {

        start {
            workItem = Payment
        }

        task('evaluateRisk') {
            name = 'Evaluate Risk'
            swimlane = 'risk'
            execution {
                by = human
                url = 'http://localhost:8081/evaluate-risk'
            }

        }

        orGate('or_1'){
            rule {
                if(_workData.isRisky) _goVia('risky')
                else _goVia('notRisky')
            }
        }


        andGate('and_1')

        task('collectPayment') {

            name = 'Collect Payment'
            swimlane = 'finance'
            execution {
                by = system
                when = immediately
                url = 'http://localhost:8081/collect-payment'
            }
        }

        task('prepareDocs') {

            name = 'Prepare Documents'
            swimlane = 'legal'
            execution {
                by = human
                url = 'http://localhost:8081/prepare-docs'
            }
        }

        sync('sync_1'){
            splitNode = 'and_1'
        }
    }

    //flow
    flow {
        start >> N('evaluateRisk') >> N('or_1') - ['risky'] >> end
        N('or_1') - ['notRisky'] >> N('and_1')
        N('and_1') >> N('collectPayment') >> N('sync_1')
        N('and_1') >> N('prepareDocs') >> N('sync_1') >> end
    }

}
