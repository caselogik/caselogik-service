package sample

// workItems
class PaymentBatch implements WorkBatch{
    String key
    List<Payment> items
}

class Payment implements WorkItem {
    String key
    String bankCode
    boolean isRisky
    float amount
}

class SimpleWork implements WorkItem {
    String key
    boolean isOK
    List<Payment> payments
}

class SimpleWorkPart implements WorkItem {
    String key
}

class CustCaseWI implements WorkItem {
    String key;
    boolean isRisky
}