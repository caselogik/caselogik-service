package workitems

// workItems
class SimpleWork {
    boolean isOK
}

class PaymentBatch {
    String ref
    List<String> paymentRefs = new ArrayList<>()
}

// process
process('simple-1') {

    // variables
    variables {
        var('wiPkg') setTo 'com.itblueprints.workitems'
        var('financeUrl') setFromEnvVar 'FIN_URL'
        var('channelUrl') setFromProperty 'channel.url'
    }


    // swimlanes
    swimlanes {
        swimlane('customer')
        swimlane('channel') {
            department = 'channelServices'
        }
        swimlane('dataProtectionOfficer') {
            role = 'dataProtectionOfficer'
        }
    }


    // nodes
    nodes {

        start {
            workItem = SimpleWork
        }

        orGate('or01') {
            rule {
                _isRandomlySelected(0.05) //TODO
                if(_workItem.isOK) return _go('OK')
                else return _go('NotOK')
            }
        }

        andGate('and01') {
            rule {
                if (_workItem.isOK) return _go('OK', 'somewhatOK')
                else return _go('NotOK')
            }
        }

        task('checkApplication') {
            actorType = worker
            swimlane = 'channel'
            execution {
                url = '$uiUrl/abc/1/da'
            }
        }

        task('rateRisk') {
            actorType = worker
            swimlane = 'channel'
            execution {
                url = '$uiUrl/abc/1/da'
            }
        }

        task('batchPayments') {
            actorType = system
            swimlane = 'finance'
            execution {
                when = onEvent('endOfDay')
                url = '$financeSvcsUrl/abc/1/da.html'
            }
            outputType = PaymentBatch
        }

        task('collectPayments') {
            actorType = system
            swimlane = 'finance'
            execution {
                when = immediately
                url = 'abc/1/da.html'
            }
        }

        sync('sync_1') {
            inputType = SimpleWork
            splitNode = node('and_1')
            timeOut = '1H'
            rule {
                if(_isInputSetComplete) return _go(_preSplitWI)
                else return _wait
            }

        }

        wait('wait_1') {
            inputType = SimpleWork
            timeOut = '1H'
            rule {
                if(_event('commsFailureHandled') && _event('commsFailureIgnored') && ! _event('cancelled')) return _go
                else return _wait
            }
        }

        end('end01')
    }

    // flow
    flow {
        start >> N('or01')
        N('or01') >> ['isOK'] >> N('checkApplication') >> N('end01')
        N('or01') >> ['notOK'] >> N('and01')
        N('and01') >> N('batchPayments') >> end
        N('and01') >> end
    }

}

