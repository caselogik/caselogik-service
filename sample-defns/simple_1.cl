package sample

workDataFrom('simple_wis.cl')

//process
process('simple_1') {

    //variables
    variables {
        var('lrsEndpoint') setTo 'lrs1'
        var('financeUrl') setTo 'https://finance.ros.rov.uk'
        var('caselogikUrl') setTo 'https://caselogik.ros.rov.uk'
        var('channelUrl') setTo property('channel.url')
        var('calendarUrl') setTo property('calendar.url')
        var('codeFolder') setTo envVariable('CASELOGIK_CODE_HOME')
    }

    //swimlanes
    swimlanes {
        swimlane('customer')
        swimlane('channel') {
            groups = ['cusotmerSupport']
            calendarUrl = '${calendarUrl}'
        }
        swimlane('finance') {
            groups = ['registrationFinance']
            calendarUrl = '${calendarUrl}'
        }
    }

    //nodes
    nodes {

        start {
            workItem = SimpleWork
        }

        //all paths must have guards
        orGate('or_1') {
            rule {
                if(_workData.isOK) _goVia('ok')
                else _goVia('notOK')
            }
        }

        //when no rule is present all paths are taken
        andGate('and_1')

        //when a rule is present, all paths must have guards
        andGate('and_2') {
            rule {
                if (_workData.isOK) _goVia('ok', 'somewhatOK')
                else _goVia('notOK')
            }
        }

        //for splitting workitems in a case

        workSplit('wsplit_1') {
            rule {
                List<Payment> payments = _workItem.payments
                _goWithEachItemIn(payments)
            }
        }



        sync('sync_1') {
            splitNode = 'and_1'
        }

        wait('wait_1') {
            timeOut = '5s'
        }

        wait('wait_2') {
            until = onEvent('fileReceived')
        }

        batch('batch_payments'){

            when = onEvent('endOfDay')
            workBatch = PaymentBatch
            rule {
                _batchKey = _workItem.bankId
            }
        }

        task('collectPayments') {
            swimlane = 'finance'
            execution {
                by = system
                when = immediately
                url = 'abc/1/da.html'
            }
        }

        task('checkApplication') {
            swimlane = 'channel'
            execution {
                by = human
                url = 'abc/${codeFolder}/da'
            }
            priority {
                rule {
                    if(_workItem.clientId=='1234' && _taskAge > '12H') {
                        _priority = 2        
                    }
                    else _priority = 0
                }
            }
        }

        end('end_1')
    }

    //flow
    flow {
        start >> N('and_1')-['ok'] >> N('sync_1')
        N('and_1')-['somewhatOK'] >> N('wait_1') >> N('sync_1') >> end
        N('and_1')-['notOK'] >> N('or_1')-['OK'] >> end
        N('or_1')-['notOK'] >> N('wsplit_1') >> N('batch_payments') >> N('end_1')
    }

}

