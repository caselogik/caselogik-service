package sample

workDataFrom('simple_wis.cl')

//process
process('simple_3') {

    swimlanes {
        swimlane('finance')
        swimlane('legal')

    }

    //nodes
    nodes {

        start {
            workItem = PaymentBatch
        }

        andGate('and_1')

        task('collectPayments') {
            swimlane = 'finance'
            execution {
                by = system
                when = immediately
                url = 'http://localhost:8081/collectPayments'
            }
        }

        task('evaluateRisk') {
            swimlane = 'legal'
            execution {
                by = human
                url = 'http://localhost:8081/evaluateRisk'
            }
        }

    }

    //flow
    flow {
        start >> N('collectPayments') >> end
    }

}
