package simple_4

class Payment implements WorkItem {
    String key
    float amount
}

class PaymentBatch implements WorkBatch {
    String key
    String ref
    List<Payment> items

}

//process
process('simple_4') {

    swimlanes {
        swimlane('finance')
    }

    //nodes
    nodes {

        start {
            workItem = PaymentBatch
        }

        task('collectPayments') {
            swimlane = 'finance'
            execution {
                by = system
                when = immediately
                url = 'http://localhost:8080/exec'
            }
        }

    }

    //flow
    flow {
        start >> N('collectPayments') >> end
    }

}

