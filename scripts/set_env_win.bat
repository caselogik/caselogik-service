set CASELOGIK_CODE_HOME=C:\Dev\Code\Caselogik
set CASELOGIK_DB_USER=root
set CASELOGIK_DB_PASSWORD=root
set CASELOGIK_SERVICE_URL=http://localhost:8080
set CASELOGIK_DEFN_FOLDER=C:\Dev\Code\Caselogik\test-client\definition
set CASELOGIK_JPA_URL=jdbc:mariadb://localhost:3306/caselogik
set CASELOGIK_JMS_DATA_FOLDER=C:\Dev\Code\Caselogik\artemis