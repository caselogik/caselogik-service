package org.caselogik.service

class CLRuntimeException(val code: Int, msgTemplate: String, vararg val args: String, cause: Throwable? = null)
    : RuntimeException(String.format(msgTemplate, * args), cause){

    //=============================================
    companion object {

        //*******************************************
        //Property values
        val ENV_VAR_MISSING = 5

        //Loading errors
        val NODE_CREATION_FAILED = 11
        val DUPLICATE_PROCESS_DEFN = 12

        //Enactment errors
        val PROCESS_NOT_FOUND = 30
        val NODE_NOT_FOUND = 31
        val CASE_NOT_FOUND = 32
        val CASE_BATCH_NOT_FOUND = 33
        val GUARDS_NOT_MATCHED = 43
        val TOKEN_NOT_FOUND = 44
        val WD_LOAD_FAILED = 46
        val WD_TYPE_NOT_FOUND = 47
        val INVALID_DEFN_FOLDER = 48
        val NO_DEFN_FILES_FOUND = 49
        val WD_DEPENDENCY_NOT_FOUND = 50
        val INVALID_CASE_STATUS_CHANGE = 52
        val INVALID_TOKEN_STATUS_CHANGE = 53
        val ENV_VARS_IN_CONFIG_NOT_RESOLVED = 54
        val CONFIG_PROP_BLANK = 55
        val BAD_COMMAND = 56

        //Infra exceptions
        val UNSUPPORTED_DB = 60

    }

}

