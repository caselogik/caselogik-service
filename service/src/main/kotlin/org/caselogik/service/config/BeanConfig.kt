package org.caselogik.service.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.caselogik.definition.dsl.DslLoader
import org.caselogik.service.definition.loader.ProcessLoader
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.controllers.ScheduleCtrl
import org.caselogik.service.enactment.events.EventFactory
import org.caselogik.service.enactment.events.EventSender
import org.caselogik.service.enactment.gateways.ExecutionGateway
import org.caselogik.service.enactment.repositories.TaskRepo
import org.caselogik.service.enactment.repositories.TokenRepo
import org.caselogik.service.enactment.util.EnactmentContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.web.client.RestTemplate


@Configuration
class BeanConfig

    @Autowired
    constructor(val cfg: CLConfiguration){

    //-------------------------------------
    @Bean @Primary
    fun dslLoader(): DslLoader {
        return DslLoader()
    }

    //-------------------------------------
    @Bean @Primary
    fun restTemplate(builder: RestTemplateBuilder): RestTemplate {
        return builder.build()
    }

    //-------------------------------------
    @Bean @Primary
    fun objectMapper(builder: Jackson2ObjectMapperBuilder): ObjectMapper {
        val mapper = builder.build<ObjectMapper>()
        mapper.registerModule(JavaTimeModule())
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        mapper.dateFormat = StdDateFormat()
        return mapper
    }


    //-------------------------------------
    @Bean @Primary
    fun processRepo(ectx: EnactmentContext): ProcessRepo {
        return ProcessRepo(ectx)
    }

    //-------------------------------------
    @Bean @Primary
    fun processLoader(processRepo: ProcessRepo): ProcessLoader {
        return ProcessLoader(processRepo, cfg)
    }

    //-------------------------------------
    @Bean @Primary
    fun enactmentContext(eventSender: EventSender,
                         tokenRepo: TokenRepo,
                         taskRepo: TaskRepo,
                         executionGW: ExecutionGateway,
                         config: CLConfiguration,
                         scheduleCtrl: ScheduleCtrl): EnactmentContext {

        return EnactmentContext(eventSender, tokenRepo, taskRepo, executionGW, config, scheduleCtrl)
    }

    //-------------------------------------
    @Bean @Primary
    fun eventFactory(): EventFactory {
        return EventFactory()
    }

    //-------------------------------------
    /**
     *  Note: The Ctrl classes have to be annotated with @Component, else
     *  throws class not open exception when spring tries to create proxies. The
     *  annotation helps spring kick in and make the class open through bytecode
     *  manipulation using javassist (see build.gradle)
     */
}


