package org.caselogik.service.config

import org.caselogik.service.CLRuntimeException
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import java.time.Duration
import javax.annotation.PostConstruct
import javax.validation.constraints.NotBlank


@Configuration
@ConfigurationProperties(prefix = "caselogik")
class CLConfiguration {


    @NotBlank
    var serviceUrl: String = ""
    var definitionFolder = ""
    var clockTickIntervalSecs = 60

    fun getClockTickInterval(): Duration =Duration.ofSeconds(clockTickIntervalSecs.toLong())

    //---------------------------

    val infrastructure = Infrastructure()

    class Infrastructure{

        val jpa = Jpa()
        val jms = Jms()
    }

    //------------------------------
    class Jpa {
        var url: String = ""
        var username: String = ""
        var password: String = ""
        var hbm2ddl: String = "validate"
        var debug: Boolean = false
    }

    //-------------------------------------
    class Jms {
        var serverMode: String = "embedded" // embedded, external
        var embeddedServerDataFolder: String = ""
    }

    //--------------------------------------
    // Make sure that all environment variables have been replaced
    @PostConstruct
    fun validate(){
        serviceUrl.check("service-url", true)

        definitionFolder.check ( "definition.folder-url", true)

        infrastructure.jpa.url.check("infrastructure.jpa.url", true)

        infrastructure.jpa.username.check("infrastructure.jpa.username")
        infrastructure.jpa.password.check("infrastructure.jpa.password")

        infrastructure.jms.serverMode.check("infrastructure.jms.serverMode", true)

        infrastructure.jms.embeddedServerDataFolder.check("infrastructure.jms.embeddedServerDataFolder")

    }

    //-------------------------------------------------------------
    private val regex = "\\\$\\{.*\\}".toRegex()

    private fun String.check(propName: String, mandatory: Boolean = false){

        if(this.contains(regex)) throw CLRuntimeException(CLRuntimeException.ENV_VARS_IN_CONFIG_NOT_RESOLVED,
                "Environment variable expression '%s' not resolved for property '%s' in application.yml.", this, propName)

        if(mandatory && this.isNullOrEmpty()) throw CLRuntimeException(CLRuntimeException.CONFIG_PROP_BLANK,
                "Property '%s' is blank in application.yml.", propName)
    }

}