package org.caselogik.service.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer
import org.springframework.boot.autoconfigure.jms.artemis.ArtemisConfigurationCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jms.annotation.EnableJms
import org.springframework.jms.config.DefaultJmsListenerContainerFactory
import org.springframework.jms.config.JmsListenerContainerFactory
import org.springframework.jms.support.converter.MappingJackson2MessageConverter
import org.springframework.jms.support.converter.MessageConverter
import org.springframework.jms.support.converter.MessageType
import javax.jms.ConnectionFactory
import org.apache.activemq.artemis.core.config.Configuration as ArtemisConfig


@Configuration
@EnableJms
class JmsConfiguration
    @Autowired
    constructor(val props: CLConfiguration){

    //------------------------------------------
    // JMS Artemis
    @Bean // Serialize message content to json using TextMessage
    fun jacksonJmsMessageConverter(mapper: ObjectMapper): MessageConverter {
        val converter = MappingJackson2MessageConverter()
        converter.setObjectMapper(mapper)
        converter.setTargetType(MessageType.TEXT)
        converter.setTypeIdPropertyName("_type")
        return converter
    }

    //--------------------------------------------------
    @Bean
    fun myFactory(connectionFactory: ConnectionFactory,
                  configurer: DefaultJmsListenerContainerFactoryConfigurer): JmsListenerContainerFactory<*> {
        val factory = DefaultJmsListenerContainerFactory()
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory)


        // You could still override some of Boot's default if necessary.
        return factory
    }

}

//================================================================
// Artemis config
@Configuration
class ArtemisConfig
    @Autowired
    constructor(val props: CLConfiguration) : ArtemisConfigurationCustomizer{

    //---------------------------------------------------
    override fun customize(config: ArtemisConfig) {

        config.isPersistenceEnabled = true
        val dataFolder = props.infrastructure.jms.embeddedServerDataFolder
        config.bindingsDirectory = "${dataFolder}/bindings"
        config.pagingDirectory = "${dataFolder}/paging"
        config.largeMessagesDirectory = "${dataFolder}/largemessages"
        config.journalDirectory = "${dataFolder}/journal"
    }

}