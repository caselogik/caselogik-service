package org.caselogik.service.config

import org.caselogik.service.CLRuntimeException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import java.util.*
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource


@Configuration
class JpaConfiguration
    @Autowired
    constructor(val config: CLConfiguration) {

    //-------------------------------------------
    //JPA Hibernate
    @Bean
    fun entityManagerFactory(dataSource: DataSource): EntityManagerFactory {
        val em = LocalContainerEntityManagerFactoryBean()
        em.dataSource = dataSource
        em.setPackagesToScan(
                "org.caselogik.service.enactment.entities",
                "org.caselogik.service.enactment.events.entities",
                "org.caselogik.service.enactment.logging",
                "org.caselogik.service.enactment.util.cluster"
        )

        val vendorAdapter = HibernateJpaVendorAdapter()
        em.jpaVendorAdapter = vendorAdapter

        val properties = Properties()
        properties.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false")
        properties.setProperty("hibernate.physical_naming_strategy","org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy")
        properties.setProperty("hibernate.hbm2ddl.auto", config.infrastructure.jpa.hbm2ddl)
        properties.setProperty("hibernate.dialect", dialects[dbtype()])

        if(config.infrastructure.jpa.debug) properties.setProperty("hibernate.show_sql", "true")

        em.setJpaProperties(properties)
        em.afterPropertiesSet()

        return em.getObject()!!
    }

    //----------------------------------------------------------------------
    private val dialects = arrayOf(
            "org.hibernate.dialect.MySQLDialect",
            "org.hibernate.dialect.PostgreSQLDialect",
            "org.hibernate.dialect.MariaDBDialect",
            "org.hibernate.dialect.H2Dialect")


    // *************** JDBC ***************

    //-------------------------------------
    // Datasource
    @Bean
    @Primary
    fun dataSource(): DataSource {

        return DataSourceBuilder
                .create()
                .url(config.infrastructure.jpa.url)
                .username(config.infrastructure.jpa.username)
                .password(config.infrastructure.jpa.password)
                .driverClassName(drivers[dbtype()])
                .build()
    }

    //------------------------------------------------------------------------
    private val drivers = arrayOf(
            "com.mysql.jdbc.Driver",
            "org.postgresql.Driver",
            "org.mariadb.jdbc.Driver",
            "org.h2.Driver")

    //------------------------------------------------------------------------
    fun dbtype(): Int {
        val url = config.infrastructure.jpa.url

        val type = if(url.contains("jdbc:mysql:")) 0
        else if(url.contains("jdbc:postgresql:")) 1
        else if(url.contains("jdbc:mariadb:")) 2
        else if(url.contains("jdbc:h2:")) 3
        else throw CLRuntimeException(CLRuntimeException.UNSUPPORTED_DB, "Could not figure out database type from id '%s'.", url)

        return type;
    }
}