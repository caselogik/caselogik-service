package org.caselogik.service.definition.entities

import groovy.lang.MissingMethodException
import groovy.lang.MissingPropertyException
import org.caselogik.api.ExternalEvent
import org.caselogik.api.WorkData
import org.caselogik.api.WorkItem
import org.caselogik.definition.dsl.CLDefinitionException
import org.caselogik.definition.dsl.model.RuleDefn
import org.caselogik.service.enactment.events.TimeoutEvent
import org.caselogik.service.enactment.entities.Token

//============================================
interface WfNode {
    val id: String
    val name: String?
    val workDataType: Class<out WorkData>
    val process: BaseProcess
    val inPaths: ArrayList<Path>
    val outPaths: ArrayList<Path>

    fun transitionIn(token: Token)
}


//============================================
interface Activity : WfNode

//============================================
interface TokenRestNode {

    fun onEvent(event: TimeoutEvent)
    fun onEvent(event: ExternalEvent)
    val uid: NodeUid
}

//============================================
interface SplitterNode


//============================================
interface RuleBasedNode {

    fun testRules()

    fun handleRuleError(ex: Exception, id: String, rule: RuleDefn) {
        when(ex) {
            is MissingPropertyException -> {
                if(WorkItem::class.java.isAssignableFrom(ex.type))
                    throw CLDefinitionException(CLDefinitionException.getUNKNOWN_PROPERTY_IN_WORKDATA(), "Property '%s' not present in work item type '%s'. Error in rule of node '%s'. %s", ex.property, ex.type.simpleName, id, rule._scriptLocation)
                else
                    throw CLDefinitionException(CLDefinitionException.getUNKNOWN_PROPERTY_IN_RULE(), "Unknown property '%s' in rule of node '%s'. %s", ex.property, id, rule._scriptLocation)
            }

            is MissingMethodException -> {
                throw CLDefinitionException(CLDefinitionException.getUNKNOWN_METHOD_IN_RULE(), "Unknown method '%s' in rule of node '%s'. %s", ex.method, id, rule._scriptLocation)
            }

            is IllegalArgumentException -> {
                val msg = ex.message!!
                //if this exception is because an argument to a rule delegate method (begins with _) is null ...
                if(msg.startsWith("Parameter specified as non-null is null: method")
                        && msg.contains("${this::class.simpleName}RuleDelegate._")) //... then suppress the exception as it is expected at this stage
                else
                    throw ex

            }
        }
    }
}


