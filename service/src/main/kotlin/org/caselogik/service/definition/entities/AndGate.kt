package org.caselogik.service.definition.entities

import org.caselogik.service.CLRuntimeException
import org.caselogik.api.WorkData
import org.caselogik.definition.dsl.model.AndGateDefn
import org.caselogik.definition.dsl.model.RuleDefn
import org.caselogik.service.enactment.entities.Token
import org.caselogik.service.enactment.entities.TokenStatus
import org.caselogik.service.enactment.events.TokenSplit

//========================================================
class AndGate(defn: AndGateDefn, process: BaseProcess)
    : ProcessNode(defn.id, defn.name, defn.workItemType, process), RuleBasedNode, SplitterNode {

    val rule: RuleDefn? = defn.rule

    //---------------------------------------------
    // Note: Unguarded paths will always be taken
    override fun testRules()  {
        if(rule != null){
            val wi = workDataType.getDeclaredConstructor().newInstance()
            rule.closure?.delegate = AndGateRuleDelegate(wi)
            try {
                rule.closure?.call() as AndGateRuleResult
            }
            catch(ex: Exception){
                handleRuleError(ex, id, rule)
            }
        }
    }

    //--------------------------------------------------------
    override fun transitionIn(token: Token) {

        super.transitionIn(token)

        val selectedOutPaths =
           if(rule==null) outPaths  //default rule is send out via all outpaths
           else {
               val d = AndGateRuleDelegate(token.workData.dataObject)
               rule.closure.delegate = d
               val result = rule.closure?.call() as AndGateRuleResult
               val paths = outPaths.filter {  it.guard.isEmpty() || result.guards.contains(it.guard)} //paths without guards are also included
               if(paths.isEmpty())
                   throw CLRuntimeException(CLRuntimeException.GUARDS_NOT_MATCHED, "Guards computed by rule of '%s' (%s) don't match guards on its out paths (%s).", id, result.guards.joinToString(), outPaths.map { it.guard }.joinToString())
               paths
           }

        token.status = TokenStatus.Inactive

        //first gather all the paths and their tokens
        val transitions = mutableListOf<Transition>()
        for(p in selectedOutPaths){
            val childToken = token.newChildTokenOnSplit()
            _eCtx.tokenRepo.save(childToken)
            transitions.add(Transition(p, childToken))
            _eCtx.eventSender.send(TokenSplit(childToken.id, id, token.id))

        }


        //then transition out to the next nodes
        // i = MapItem<Path, Token>
        for(transition in transitions){
            transitionOut(transition.token, transition.path)
        }

    }

    //---------------------------------
    override fun toString(): String {
        return "AndGate(id='$id', workDataType='${workDataType.name}')"
    }

}

//======================================================
data class Transition(val path: Path, val token: Token)


//======================================================
class AndGateRuleDelegate(val _workData: WorkData) {
    fun _goVia(vararg guards: String) : AndGateRuleResult {
        return AndGateRuleResult(guards.toList())
    }
}
//====================================================
class AndGateRuleResult(val guards: List<String>)

