package org.caselogik.service.definition.entities

import org.caselogik.service.CLRuntimeException
import org.caselogik.api.ExternalEvent
import org.caselogik.api.WorkItem
import org.caselogik.definition.dsl.model.EventDefn
import org.caselogik.service.definition.loader.ContextData
import org.caselogik.service.enactment.util.EnactmentContext
import kotlin.reflect.KClass

abstract class BaseProcess(val id: String, val name: String?) {

    lateinit var workItemsMap: Map<String, Class<out WorkItem>>
    lateinit var variablesMap: Map<String, Variable>
    lateinit var nodesMap: Map<String, ProcessNode>
    lateinit var  paths: List<Path>

    //------------------------------------
    open fun setData(ctxData: ContextData){
        workItemsMap = ctxData.workItemsMap
        variablesMap = ctxData.variablesMap
        nodesMap = ctxData.nodesMap
        paths = ctxData.paths
    }

    //------------------------------------------
    private lateinit var _enactmentCtx: EnactmentContext
    fun setEnactmentContext(ctx: EnactmentContext){
        _enactmentCtx = ctx
        nodesMap.values.forEach{
            it.setEnactmentContext(ctx)
        }
    }
    //--------------------------------------------------------------------
    private val extEventRegistry = HashMap<String, MutableList<TokenRestNode>>()

    fun registerExternalEvent(ed: EventDefn, node: TokenRestNode){
        if(!extEventRegistry.containsKey(ed.name)){
            extEventRegistry.put(ed.name, mutableListOf<TokenRestNode>())
        }
        extEventRegistry[ed.name]?.add(node)
    }

    //----------------------------------------------------
    fun onEvent(evt: ExternalEvent){
        if(extEventRegistry.containsKey(evt.name)){
            extEventRegistry[evt.name]?.forEach { it.onEvent(evt) }
        }
    }
    //-------------------------------
    fun getNode(nodeId: String): ProcessNode {
        if(!nodesMap.containsKey(nodeId)) {
            throw CLRuntimeException(CLRuntimeException.NODE_NOT_FOUND, "Node '%s' of process '%s' not found. Check node id and process id.", nodeId, id)
        }
        return nodesMap[nodeId]!!
    }

    //-------------------------------
    fun getTaskNodes(): List<TaskNode> {
        return  nodesMap.values.filter{ it is TaskNode }.map { it as TaskNode}
    }

    //--------------------------------
    override fun toString(): String {
        return "(id='$id', variables=$variablesMap, nodes=${nodesMap.values}, workItems=$workItemsMap"
    }

}

