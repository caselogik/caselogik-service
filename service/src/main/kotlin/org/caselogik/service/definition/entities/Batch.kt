package org.caselogik.service.definition.entities

import org.caselogik.api.ExternalEvent
import org.caselogik.api.WorkBatch
import org.caselogik.api.WorkItem
import org.caselogik.definition.dsl.model.BatchDefn
import org.caselogik.definition.dsl.model.EventDefn
import org.caselogik.definition.dsl.model.RuleDefn
import org.caselogik.service.enactment.events.TimeoutEvent
import org.caselogik.service.enactment.entities.*
import org.caselogik.service.enactment.events.TokensBatched

class Batch(defn: BatchDefn, process: BaseProcess)
                : ProcessNode(defn.id, defn.name, defn.workItemType, process), RuleBasedNode, TokenRestNode {


    private val workBatchType: Class<out WorkBatch> = defn.workBatchType
    private val batchingEvent : EventDefn = defn.event
                                            .also { process.registerExternalEvent(defn.event, this) }

    //-----------------------------------------
    override fun onEvent(event: TimeoutEvent) {
        TODO("not implemented")
    }

    //-----------------------------------------
    override fun onEvent(event: ExternalEvent) {
        if(this.batchingEvent.matches(event)){

            //for holding batched items
            val batches = HashMap<String, ArrayList<WorkItemToken>>()

            val tokens = _eCtx.tokenRepo.getActiveTokensAtNode(this)
            val wiTokens = tokens.map{ it as WorkItemToken }
            val del = BatchRuleDelegate()
            rule.closure?.delegate = del

            //Run the batching rule to get a set of batches and the token that belong to each
            for(token in wiTokens){
                del._workItem = token.workItem
                rule.closure?.call()
                if(!batches.containsKey(del._batchKey)){
                    batches.put(del._batchKey, ArrayList())
                }
                batches[del._batchKey]!!.add(token)
            }

            //for holding tokens that need to go out
            val outTokens = mutableListOf<WorkBatchToken>()

            for(batchKey in batches.keys){

                //create new batch
                val wb = workBatchType.getDeclaredConstructor().newInstance() as WorkBatch
                wb.key = batchKey
                wb.items = batches[batchKey]?.map { it.workItem }

                //create its envelope
                val wbe = WorkBatchE().apply {
                    @Suppress("UNCHECKED_CAST")
                    items = wb.items.map { WorkItemE().apply { dataObject = it } } as MutableList<WorkItemE>
                    dataObject = wb.apply { items = mutableListOf() }
                }

                //Create new wb token
                val wbToken = WorkBatchToken()
                wbToken.processId = this.process.id
                wbToken.workData = wbe
                wbToken.itemTokens = batches[batchKey] as MutableList<WorkItemToken>

                wbToken.itemTokens.forEach{
                    it.batchToken  = wbToken
                    it.updateStatus(TokenStatus.Inactive)
                }

                _eCtx.tokenRepo.save(wbToken)

                outTokens.add(wbToken)
            }

            for(t in outTokens){
                _eCtx.eventSender.send(TokensBatched(t.id, id, t.itemTokens.map{it.id}, event))
            }

            //transition out
            for(token in outTokens){
                transitionOut(token, outPaths[0])
            }
        }
    }

    //-----------------------------------------
    val rule: RuleDefn = defn.rule

    //-----------------------------------------
    override fun testRules() {

        //TODO
        val del = BatchRuleDelegate()
        del._batchKey = "1111"
        //del._workItem =
        rule.closure?.delegate = del
        try {
            rule.closure?.call()
        }
        catch(ex: Exception){
            handleRuleError(ex, id, rule)
        }
    }

    //-----------------------------------------
    override fun transitionIn(token: Token) {
        super.transitionIn(token)
    }

    //---------------------------------
    override fun toString(): String {
        return "Batch(id='$id', inputType='${workDataType.name}', outputType='${workBatchType.name}')"
    }
}

//======================================================
class BatchRuleDelegate() {
    lateinit var _workItem: WorkItem
    lateinit var _batchKey: String

}
