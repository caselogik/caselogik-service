package org.caselogik.service.definition.entities

import org.caselogik.definition.dsl.model.Constants
import org.caselogik.definition.dsl.model.EndDefn
import org.caselogik.service.enactment.events.CaseBatchCompleted
import org.caselogik.service.enactment.events.CaseCompleted
import org.caselogik.service.enactment.entities.*


class End(defn: EndDefn, process: BaseProcess)
    : ProcessNode(defn.id, defn.name, defn.workItemType, process) {

    val isDefault = defn.id == Constants.NodeIds.getDefaultEndNode()

    //---------------------------------
    override fun toString(): String {
        return "End(id='$id', inputType='${workDataType.name}')"
    }

    //------------------------------
    override fun transitionIn(token: Token) {
        super.transitionIn(token)
        when(token) {
            is WorkItemToken -> processToken(token)
            is WorkBatchToken -> processToken(token)
        }
    }

    //----------------------------------------------
    private fun processToken(token: WorkItemToken){
        token.status = TokenStatus.Dead
        //Case is complete if there are no more active tokens
        val case = token.case
        if (case.tokens.filter { it.status == TokenStatus.Active }.isEmpty()){
            case.status = CaseStatus.Completed
            _eCtx.eventSender.send(CaseCompleted(case.id))

            //CaseBatch is complete if no more active cases
            val caseBatch = case.caseBatch
            if(caseBatch!=null){
                if(caseBatch.cases.filter{ it.status == CaseStatus.Active}.isEmpty()){
                    caseBatch.status = CaseBatchStatus.Completed
                    _eCtx.eventSender.send(CaseBatchCompleted(caseBatch.id))
                }
            }
        }
    }

    //---------------------------------------------
    private fun processToken(token: WorkBatchToken){
        token.status = TokenStatus.Dead
        token.itemTokens.forEach{processToken(it) }
    }
}