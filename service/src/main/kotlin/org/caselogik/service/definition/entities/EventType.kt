package org.caselogik.service.definition.entities

interface EventType

class SystemEventType : EventType

class UserEventType : EventType