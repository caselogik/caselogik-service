package org.caselogik.service.definition.entities

import org.caselogik.definition.dsl.model.TaskDefn
import org.caselogik.service.enactment.events.TaskCreated
import org.caselogik.service.enactment.entities.TaskStatus
import org.caselogik.service.enactment.entities.Token
import org.caselogik.service.enactment.entities.Worker


class ExternalPartyTaskNode(defn: TaskDefn,
                            process: BaseProcess,
                            swimLane: Swimlane) : TaskNode(defn, process, swimLane){

    //--------------------------------------------------------
    override fun transitionIn(token: Token) {
        super.transitionIn(token)

        val task = createTask(token)

        val e1 = TaskCreated(task.id)
        _eCtx.eventSender.send(e1)

        //set the worker as external and activate task
        task.worker = Worker.external(id)
        task.changeStatus(TaskStatus.Active)

        //No need to raise TaskAllocated or TaskActivated event

    }

    //---------------------------------
    override fun toString(): String {
        return "ExternalTaskNode(id='$id', inputType='${workDataType.name}')"
    }
}
