package org.caselogik.service.definition.entities

import org.caselogik.definition.dsl.model.TaskDefn
import org.caselogik.service.enactment.events.TaskCreated
import org.caselogik.service.enactment.entities.Token


class HumanTaskNode(defn: TaskDefn,
                    process: BaseProcess,
                    swimLane: Swimlane) : TaskNode(defn, process, swimLane){

    //--------------------------------------------------------
    override fun transitionIn(token: Token) {
        super.transitionIn(token)
        val task = createTask(token)
        val e1 = TaskCreated(task.id)
        _eCtx.eventSender.send(e1)
    }

    //---------------------------------
    override fun toString(): String {
        return "HumanTaskNode(id='$id', inputType='${workDataType.name}')"
    }
}
