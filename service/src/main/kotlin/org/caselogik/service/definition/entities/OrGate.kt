package org.caselogik.service.definition.entities

import org.caselogik.service.CLRuntimeException
import org.caselogik.api.WorkData
import org.caselogik.definition.dsl.model.OrGateDefn
import org.caselogik.definition.dsl.model.RuleDefn
import org.caselogik.service.enactment.entities.Token

class OrGate(defn: OrGateDefn, process: BaseProcess)
    : ProcessNode(defn.id, defn.name, defn.workItemType, process), RuleBasedNode {

    val rule: RuleDefn = defn.rule

    //--------------------------------------
    override fun testRules() {

        val wi = workDataType.getDeclaredConstructor().newInstance()
        rule.closure?.delegate = OrGateRuleDelegate(wi)
        try {
            rule.closure?.call() as OrGateRuleResult
        }
        catch(ex: Exception){
            handleRuleError(ex, id, rule)
        }
    }

    //------------------------------------------------------
    override fun transitionIn(token: Token) {
        super.transitionIn(token)

        val d = OrGateRuleDelegate(token.workData.dataObject)
        rule.closure?.delegate = d
        val result = rule.closure?.call() as OrGateRuleResult
        val pathsMatched = outPaths.filter { it.guard == result.guard }
        if(pathsMatched.isEmpty())
            throw CLRuntimeException(CLRuntimeException.GUARDS_NOT_MATCHED, "Guard computed by rule of '%s' (%s) doesn't match guards on its out paths (%s).", id, result.guard, outPaths.map { it.guard }.joinToString())

        val selectedOutPath = pathsMatched[0]

        transitionOut(token, selectedOutPath)

    }

    //---------------------------------
    override fun toString(): String {
        return "OrGate(id='$id', workDataType='${workDataType.name}')"
    }
}

//====================================================
class OrGateRuleResult(val guard: String)

//======================================================
class OrGateRuleDelegate(val _workData: WorkData) {
    fun _goVia(guard: String) : OrGateRuleResult {
        return OrGateRuleResult(guard)
    }
}




