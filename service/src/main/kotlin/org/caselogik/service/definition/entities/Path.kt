package org.caselogik.service.definition.entities

class Path(val from: ProcessNode, val to: ProcessNode, val guard: String) {

    //---------------------------------------------
    override fun toString(): String {
        return "Path(from='$from', to='$to', guard='$guard')"
    }
}


