package org.caselogik.service.definition.entities

import org.caselogik.api.WorkData
import org.caselogik.definition.dsl.model.NodeDefn
import org.caselogik.service.definition.loader.ContextData
import org.caselogik.service.enactment.entities.Token
import org.caselogik.service.enactment.events.TokenRouted
import org.caselogik.service.enactment.util.EnactmentContext
import org.caselogik.service.enactment.repositories.WorkItemMapper
import java.time.Duration

abstract class ProcessNode(override val id: String,
                           override val name: String?,
                           override val workDataType: Class<out WorkData>,
                           override val process: BaseProcess) : WfNode {

    override val inPaths: ArrayList<Path> = ArrayList()
    override val outPaths: ArrayList<Path> = ArrayList()

    val uid = NodeUid.new(process.id, id)

    val workDataTypeName: String
        get() { return workDataType.name }

    //------------------------------------------
    fun toWorkData(wiJson: String): WorkData {
        return WorkItemMapper.toWorkData(wiJson, workDataType)
    }

    //----------------------------------------------------------
    fun parseDuration(durationStr: String) : Duration? {
        if(durationStr.isNotBlank()) {
            val s1 = durationStr.toUpperCase()
            val posOfD = s1.indexOf('D')
            val s2 = if (posOfD > 0) "P" + s1.substring(0, posOfD + 1) + "T" + s1.substring(posOfD + 1) else "PT$s1"
            return Duration.parse(s2)
        }
        else return null
    }

    //-------------------------------------------
    open fun link(defn: NodeDefn, ctx: ContextData){} //default method

    //----------------------------------------------
    protected lateinit var _eCtx: EnactmentContext
    open fun setEnactmentContext(eCtx: EnactmentContext){
        _eCtx = eCtx
    }

    //--------------------------------------
    override fun transitionIn(token: Token) {
        token.setCurrentNode(this)
    }

    //--------------------------------------
    protected fun transitionOut(token: Token, path: Path){
        _eCtx.eventSender.send(TokenRouted(token.id, this.id, path.to.id, path.guard))
        path.to.transitionIn(token);

    }
}

//==============================================================
inline class NodeUid(val uid: String) {

    companion object {
        fun new(processId: String, nodeId: String) : NodeUid = NodeUid("$processId:$nodeId")
    }

}