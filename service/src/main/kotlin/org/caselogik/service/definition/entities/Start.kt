package org.caselogik.service.definition.entities

import org.caselogik.definition.dsl.model.StartDefn
import org.caselogik.service.enactment.entities.Token

class Start(defn: StartDefn, process: BaseProcess)
    : ProcessNode(defn.id, defn.name, defn.workItemType, process) {

    //---------------------------------
    override fun toString(): String {
        return "Start(inputType='${workDataType.name}')"
    }

    //------------------------------
    override fun transitionIn(token: Token) {
        transitionOut(token,  outPaths[0])
    }

}