package org.caselogik.service.definition.entities

import org.caselogik.api.WorkItem
import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.service.enactment.entities.Token

class SubProcess(procDefn: ProcessDefn, override val process: BaseProcess)
    : BaseProcess(procDefn.id, procDefn.name), Activity {

    override val inPaths: ArrayList<Path> = ArrayList()
    override val outPaths: ArrayList<Path> = ArrayList()
    override lateinit var workDataType: Class<out WorkItem>

    //--------------------------------
    override fun toString(): String {
        return "SubProcess(${super.toString()})"
    }

    //------------------------------
    override fun transitionIn(token: Token) {

        TODO("not implemented")
    }

}