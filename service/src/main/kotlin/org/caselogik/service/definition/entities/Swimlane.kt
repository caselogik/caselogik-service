package org.caselogik.service.definition.entities

open class Swimlane(val id: String) {


}

//Internal swimlane can have groups and calendars
class InternalSwimlane(id: String, val groups: List<String>, val calendarUrl: String?) : Swimlane(id)  {


}