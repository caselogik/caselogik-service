package org.caselogik.service.definition.entities

import org.caselogik.api.ExternalEvent
import org.caselogik.definition.dsl.model.SyncDefn
import org.caselogik.definition.dsl.model.NodeDefn
import org.caselogik.service.definition.loader.ContextData
import org.caselogik.service.enactment.events.TimeoutEvent
import org.caselogik.service.enactment.entities.Token
import org.caselogik.service.enactment.entities.TokenStatus
import org.caselogik.service.enactment.events.TokenMerged


class Sync (defn: SyncDefn, process: BaseProcess)
    : ProcessNode(defn.id, defn.name, defn.workItemType, process), TokenRestNode {

    lateinit var splitNode: SplitterNode

    //---------------------------------------
    override fun onEvent(event: TimeoutEvent){

    }
    //---------------------------------------
    override fun onEvent(event: ExternalEvent){
        //TODO Handle sync on external event
        // For each token send external event received event
    }
    //---------------------------------------------------------------
    override fun transitionIn(token: Token) {
        super.transitionIn(token)

        val parentToken = token.parentToken
        val expectedTokens = parentToken.childTokens
        val arrivedTokens = expectedTokens.filter { it.currentNodeId == id }

        if(expectedTokens.size == arrivedTokens.size){
            expectedTokens.forEach {
                it.status = TokenStatus.Dead
                _eCtx.eventSender.send(TokenMerged(it.id, id, parentToken.id))
            }
            parentToken.status = TokenStatus.Active

            transitionOut(parentToken, outPaths[0])
        }
    }

    //-----------------------------------------
    override fun link(defn: NodeDefn, ctx: ContextData) {
        val md = defn as SyncDefn
        splitNode = ctx.nodesMap[md.splitNodeId] as SplitterNode
    }

    //---------------------------------
    override fun toString(): String {
        return "Sync(id='$id', inputType='${workDataType.name}')"
    }
}
