package org.caselogik.service.definition.entities

import org.caselogik.definition.dsl.model.EventDefn
import org.caselogik.definition.dsl.model.TaskDefn
import org.caselogik.service.enactment.events.TaskActivated
import org.caselogik.service.enactment.events.TaskCreated
import org.caselogik.service.enactment.entities.ExecutionDetails
import org.caselogik.service.enactment.entities.TaskStatus
import org.caselogik.service.enactment.entities.Token
import org.caselogik.service.enactment.entities.Worker
import java.time.LocalDateTime

class SystemTaskNode(defn: TaskDefn,
                     process: BaseProcess,
                     swimLane: Swimlane) : TaskNode(defn, process, swimLane){

    private val executionEvent : EventDefn = defn.execution.event
                                            .also{ process.registerExternalEvent(defn.execution.event, this)}

    //----------------------------------------------
    override fun transitionIn(token: Token) {
        super.transitionIn(token)

        val task = createTask(token)
        _eCtx.eventSender.send(TaskCreated(task.id))

        //set the worker as system
        task.worker = Worker.system(id)

        //set an execution details object
        val execDetails = ExecutionDetails();
        execDetails.lastAttemptAt = LocalDateTime.now()
        task.execDetails = execDetails

        //No need to raise TaskAllocated event

        if(executionEvent.name == TaskCreated::class.java.simpleName){
            task.changeStatus(TaskStatus.Active)
            _eCtx.eventSender.send(TaskActivated(task.id))
            _eCtx.executionGW.executeTask(task)
        }

    }


    //---------------------------------
    override fun toString(): String {
        return "SystemTaskNode(id='$id', inputType='${workDataType.name}', execEvent='$executionEvent', execUrl='$executionUrl')"
    }

}