package org.caselogik.service.definition.entities

import org.caselogik.api.ExternalEvent
import org.caselogik.api.WorkData
import org.caselogik.definition.dsl.model.RuleDefn
import org.caselogik.definition.dsl.model.TaskDefn
import org.caselogik.service.enactment.events.TaskCompleted
import org.caselogik.service.enactment.events.TaskEvent
import org.caselogik.service.enactment.events.TimeoutEvent
import org.caselogik.service.enactment.entities.Task
import org.caselogik.service.enactment.entities.Token
import java.time.LocalDateTime

abstract class TaskNode (defn: TaskDefn, process: BaseProcess, val swimLane: Swimlane)
    : ProcessNode(defn.id, defn.name, defn.workItemType, process), Activity, TokenRestNode, RuleBasedNode {

    //-----------------------------------------
    val executionUrl : String = defn.execution.url

    //-----------------------------------------
    val priorityRule: RuleDefn? = defn.priority?.rule

    val hasPriorityRule = priorityRule != null

    //---------------------------------------
    protected fun createTask(token: Token): Task {
        val task = Task()
        task.token = token
        task.taskNodeId = id
        task.processId = process.id
        task.createdAt = LocalDateTime.now()
        //task's currentWorker = Queue, by default

        //calculate priority if reqd
        if(priorityRule!=null){
            val wd = token.workData.dataObject
            val del = PriorityRuleDelegate(wd, 0)
            priorityRule.closure?.delegate = del
            priorityRule.closure?.call()
            task.priority = del._priority
        }

        _eCtx.taskRepo.save(task)
        return task
    }

    //-------------------------------------
    fun onEvent(event: TaskEvent) {
        when(event){
            is TaskCompleted -> {
                val token = event.task.token
                transitionOut(token, outPaths[0])
            }
        }
    }

    //------------------------------------
    override fun testRules() {
        if(priorityRule!=null){
            val wi = workDataType.getDeclaredConstructor().newInstance()
            priorityRule.closure?.delegate = PriorityRuleDelegate(wi, 0)
            try {
                priorityRule.closure?.call()
            }
            catch(ex: Exception){
                handleRuleError(ex, id, priorityRule)
            }
        }
    }

    //---------------------------------------
    override fun onEvent(event: TimeoutEvent){}

    //-----------------------------------------
    override fun onEvent(event: ExternalEvent){
        //TODO Handle execution on external event
        // For each token send external event received event
    }

}

//======================================================
class PriorityRuleDelegate(val _workData: WorkData, var _priority: Int)