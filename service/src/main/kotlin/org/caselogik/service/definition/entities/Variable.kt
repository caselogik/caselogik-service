package org.caselogik.service.definition.entities

class Variable(val name: String, val value: String) {

    //-------------------------------
    override fun toString(): String {
        return "Variable(name='$name', value='$value')"
    }

    //------------------------------
    companion object {
        const val variableTokenTemplate: String = "\$NAME"
    }
}