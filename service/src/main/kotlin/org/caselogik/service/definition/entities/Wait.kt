package org.caselogik.service.definition.entities

import org.caselogik.api.ExternalEvent
import org.caselogik.definition.dsl.model.EventDefn
import org.caselogik.definition.dsl.model.WaitDefn
import org.caselogik.service.enactment.events.TimeoutEvent
import org.caselogik.service.enactment.events.WaitTimedOut
import org.caselogik.service.enactment.entities.Token
import org.caselogik.service.enactment.events.ExternalEventMatched
import java.time.Duration

class Wait(defn: WaitDefn, process: BaseProcess)
    : ProcessNode(defn.id, defn.name, defn.workItemType, process), TokenRestNode {


    private val timeOut: Duration? = defn.timeOut?.let{ parseDuration(defn.timeOut) }

    private val untilEvent : EventDefn? = defn.event
                                            ?.also{ process.registerExternalEvent(defn.event, this)}

    //---------------------------------------
    override fun onEvent(event: TimeoutEvent){
        if(event is WaitTimedOut) {
            transitionOut(event.token, outPaths[0])
        }
    }

    //---------------------------------------
    override fun onEvent(event: ExternalEvent){
        if(this.untilEvent !=null && this.untilEvent.matches(event)){
            val tokens = _eCtx.tokenRepo.getActiveTokensAtNode(this)
            for(token in tokens){
                _eCtx.eventSender.send(ExternalEventMatched(token.id, id, event))
            }
            for(token in tokens){
                transitionOut(token, outPaths[0])
            }
        }
    }

    //----------------------------------------------------
    override fun transitionIn(token: Token) {
        super.transitionIn(token)

        if(timeOut!=null) {
            _eCtx.scheduleCtrl.scheduleAfterSecs(WaitTimedOut::class, token.id.toString(), timeOut.seconds)
        }

    }

    //---------------------------------
    override fun toString(): String {
        return "Wait(id='$id', timeOut='$timeOut', inputType='${workDataType.name}')"
    }

}










