package org.caselogik.service.definition.entities

import org.caselogik.definition.dsl.model.Constants
import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.service.definition.loader.ContextData

class WfProcess(procDefn: ProcessDefn) : BaseProcess(procDefn.id, procDefn.name) {

    lateinit var swimlanesMap: Map<String, Swimlane>
    lateinit var start: Start

    //------------------------------------------
    override fun setData(ctxData: ContextData){
        super.setData(ctxData)
        swimlanesMap = ctxData.swimlanesMap
        start = ctxData.nodesMap[Constants.NodeIds.getStartNode()] as Start

    }

    //-----------------------------------------------------
    fun printFlow() {
        println("\nProcess: $id")
        printFlowRecurse(this.start, "   ")
        println()
    }
    private fun printFlowRecurse(from: ProcessNode, indent: String){
        println(from)
        from.outPaths.forEach{
            print("$indent -> ")
            printFlowRecurse(it.to, "$indent   ")
        }
    }

    //--------------------------------
    override fun toString(): String {
        return "WfProcess(${super.toString()})"
    }

}
