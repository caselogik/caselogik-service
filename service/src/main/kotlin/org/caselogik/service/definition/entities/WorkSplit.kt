package org.caselogik.service.definition.entities

import org.caselogik.api.WorkItem
import org.caselogik.definition.dsl.model.RuleDefn
import org.caselogik.definition.dsl.model.WorkSplitDefn
import org.caselogik.service.enactment.entities.Token

//========================================================
class WorkSplit (defn: WorkSplitDefn, process: BaseProcess)
    : ProcessNode(defn.id, defn.name, defn.workItemType, process), RuleBasedNode, SplitterNode {

    val rule: RuleDefn? = defn.rule

    //--------------------------------------------------------
    override fun testRules() {

        if(rule!=null) {
            val wi = workDataType.getDeclaredConstructor().newInstance() as WorkItem
            rule.closure?.delegate = WorkSplitRuleDelegate(wi)
            try {
                rule.closure?.call() as WorkSplitResult
            } catch (ex: Exception) {
                handleRuleError(ex, id, rule)
            }
        }
    }

    //-------------------------------------------------------
    override fun transitionIn(token: Token) {
        super.transitionIn(token)
        TODO("not implemented")
    }

    //---------------------------------
    override fun toString(): String {
        return "WorkSplit(id='$id', workDataType='${workDataType.name}')"
    }

}

//========================================================
class WorkSplitRuleDelegate(val _workItem: WorkItem) {
    fun _goWithEachItemIn(outputs: List<WorkItem>) : WorkSplitResult {
        return WorkSplitResult(outputs)
    }
}

class WorkSplitResult(val outputs: List<WorkItem>)
