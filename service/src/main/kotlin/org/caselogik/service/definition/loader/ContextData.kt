package org.caselogik.service.definition.loader

import org.caselogik.api.WorkItem
import org.caselogik.service.definition.entities.*
import java.util.HashMap

class ContextData(
        val variablesMap: Map<String, Variable>,
        val workItemsMap: Map<String, Class<out WorkItem>>,
        val swimlanesMap: Map<String, Swimlane>,
        val process: BaseProcess
) {
    val nodesMap  = HashMap<String, ProcessNode>()
    val paths =  mutableListOf<Path>()
}