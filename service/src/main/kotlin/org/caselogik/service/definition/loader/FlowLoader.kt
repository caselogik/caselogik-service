package org.caselogik.service.definition.loader

import org.caselogik.definition.dsl.model.FlowNode
import org.caselogik.definition.dsl.model.FlowPath
import org.caselogik.definition.dsl.model.ProcessDefn
import org.caselogik.service.definition.entities.Path
import java.util.*

class FlowLoader {

    private val nodeLoader = NodeLoader()

    //---------------------------------------------------------------------
    fun load(procDefn: ProcessDefn, ctx: ContextData) {

        resolveWorkItemTypesRecurse(procDefn.startFlowNode, procDefn)
        loadFlowRecurse(procDefn.startFlowNode, procDefn, HashSet<FlowPath>(), ctx)
    }

    //-------------------------------------------------------------------------------
    private fun loadFlowRecurse(node: FlowNode,
                                procDefn: ProcessDefn,
                                pathSet: HashSet<FlowPath>,
                                ctx: ContextData){

        val fromNodeDefn = procDefn.nodesMap[node.id]!!
        val from = nodeLoader.load(fromNodeDefn, ctx)

        for(flowPath in node.outPaths){
            if(!pathSet.contains(flowPath)) {
                val toNode = flowPath.to
                val toNodeDefn = procDefn.nodesMap[toNode.id]!!
                val to = nodeLoader.load(toNodeDefn, ctx)

                //set paths
                val path = Path(from, to, flowPath.guard)
                from.outPaths.add(path)
                to.inPaths.add(path)
                pathSet.add(flowPath)
                ctx.paths.add(path)

                //link
                to.link(toNodeDefn, ctx)

                loadFlowRecurse(toNode, procDefn, pathSet, ctx)
            }
        }
    }

    //------------------------------------------------------------------------------
    private fun resolveWorkItemTypesRecurse(node: FlowNode, procDefn: ProcessDefn) {

        val pathSet= HashSet<FlowPath>()

        val fromNodeDefn = procDefn.nodesMap[node.id]!!
        for (flowPath in node.outPaths) {
            if(!pathSet.contains(flowPath)){
                val toNode = flowPath.to
                val toNodeDefn = procDefn.nodesMap[toNode.id]!!
                //update missing input workitem types based on flow
                if (toNodeDefn.workItemType == null) {
                    toNodeDefn.workItemType = fromNodeDefn.workItemType
                }
                pathSet.add(flowPath)
                resolveWorkItemTypesRecurse(toNode, procDefn)
            }
        }
    }

}