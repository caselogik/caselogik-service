package org.caselogik.service.definition.loader

import org.caselogik.service.CLRuntimeException
import org.caselogik.definition.dsl.model.*
import org.caselogik.service.definition.entities.*

class NodeLoader {
    //--------------------------------------------
    fun load(defn: NodeDefn, ctx: ContextData): ProcessNode {

        val id: String = defn.id
        var node: ProcessNode? = ctx.nodesMap[id]

        if(node == null) {
            when (defn) {
                is StartDefn -> node = Start(defn, ctx.process)
                is EndDefn -> node = End(defn, ctx.process)
                is OrGateDefn -> node = OrGate(defn, ctx.process)
                is AndGateDefn -> node = AndGate(defn, ctx.process)
                is SyncDefn -> node = Sync(defn, ctx.process)
                is WaitDefn -> node = Wait(defn, ctx.process)
                is WorkSplitDefn -> node = WorkSplit(defn, ctx.process)
                is BatchDefn -> node = Batch(defn, ctx.process)
                is TaskDefn -> {
                    val swimlane = ctx.swimlanesMap[defn.swimlane.id]!!
                    when (defn.execution.actorType) {
                        ExecutionDefn.getSYSTEM() -> {
                            node = SystemTaskNode(defn, ctx.process, swimlane)
                        }
                        ExecutionDefn.getHUMAN() -> {
                            node = HumanTaskNode(defn, ctx.process, swimlane)
                        }
                        ExecutionDefn.getEXTERNAL_PARTY() -> {
                            node = ExternalPartyTaskNode(defn, ctx.process, swimlane)
                        }
                        else -> throw CLRuntimeException(CLRuntimeException.NODE_CREATION_FAILED, "Invalid actor type in TaskDefn")
                    }
                }
            }

            //If node couldn't be created
            if (node == null) {
                val nodeType = defn::class.java.simpleName.replace("Defn", "").toLowerCase()
                throw CLRuntimeException(CLRuntimeException.NODE_CREATION_FAILED, "Failed to create %s node from defintion '%s'.", nodeType, defn.id)
            }
            else { //All OK
                if( node is RuleBasedNode){
                    (node as RuleBasedNode).testRules()
                }
            }

            ctx.nodesMap[node.id] = node
        }
        return node
    }

}