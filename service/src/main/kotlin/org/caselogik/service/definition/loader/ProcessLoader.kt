package org.caselogik.service.definition.loader


import org.caselogik.service.CLRuntimeException
import org.caselogik.service.config.CLConfiguration
import org.caselogik.definition.dsl.DslLoader
import org.caselogik.definition.dsl.FileScriptSource
import org.caselogik.definition.dsl.ScriptSource
import org.caselogik.service.definition.entities.*
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.mappers.TaskTOMapper
import org.caselogik.service.loggerFor
import java.io.File
import java.util.*

class ProcessLoader

    constructor (private val processRepo: ProcessRepo,
                 private val config: CLConfiguration){

    private val dslLoader = DslLoader()
    private val variableLoader = VariableLoader()
    private val swimlaneLoader = SwimlaneLoader()
    private val flowLoader = FlowLoader()
    private val log = loggerFor<ProcessLoader>()

    //-----------------------------------------------
    fun loadAllDefinitions() {

        val filesToLoad = getDefnFilesInFolder(config.definitionFolder)

        //a list to hold files
        val scripts = ArrayList<ScriptSource>()

        for(f in filesToLoad) {
            log.info("Loading definition file '$f'")
            scripts.add(FileScriptSource(f))
        }

        val scriptsMap = getProcScriptsWithDependencies(scripts);

        //Load processes
        for(procScript in scriptsMap.keys){
            val proc = loadProcess(procScript, scriptsMap[procScript]!!)
            processRepo.addProcess(proc)
        }

        postloadAllProcesses()
    }

    //------------------------------------------------------
    private fun getDefnFilesInFolder(folder: String): List<String> {
        if(File(folder).isDirectory) {
            val clFiles = File(folder).listFiles()
                                        .filter { f -> !f.isDirectory && f.name.endsWith(".cl") }
                                        .map { it.absolutePath }
            if(clFiles.size == 0)throw CLRuntimeException(CLRuntimeException.NO_DEFN_FILES_FOUND, "Definition folder '$folder' is either empty or does not contain any files with '.cl' suffix.")
            return clFiles
        }
        else throw CLRuntimeException(CLRuntimeException.INVALID_DEFN_FOLDER, "'$folder' in application.yml is not a valid folder.")
    }

    //-------------------------------------
    private fun postloadAllProcesses(){
        //populate node cache
        val allNodes = HashMap<NodeUid, ProcessNode>()

        for(proc in processRepo.getProcesses()){
            allNodes.putAll(proc.nodesMap.map { it.value.uid to it.value })
        }
        processRepo.allProcessNodesInUniverse = allNodes

        //set values on TaskTOMapper
        TaskTOMapper.serviceUrl = config.serviceUrl
        TaskTOMapper.taskNodesInUniverse = processRepo.getProcessNodesMapOfType(TaskNode::class)

    }

    //------------------------------------------------------
    private fun getProcScriptsWithDependencies(scripts: List<ScriptSource>):
            Map<ScriptSource, List<ScriptSource>>{

        val scriptsMap = HashMap<ScriptSource, List<ScriptSource>>()
        for(ss in scripts){
            val wdScriptNames = ArrayList<String>()
            dslLoader.inspectScript(ss, wdScriptNames)

            if(ss.containsProcDefn) {
                val wdScripts = HashSet<ScriptSource>()
                for(sn in wdScriptNames) {
                    val matchingScripts = scripts.filter { it.id == sn }
                    if (matchingScripts.isEmpty()) throw CLRuntimeException(CLRuntimeException.WD_DEPENDENCY_NOT_FOUND,
                            "Script file '%s' uses work data from file '%s', but '%s' has not been loaded. Check the definition folder.", ss.id, sn, sn)

                    wdScripts.add(matchingScripts[0])
                }

                scriptsMap.put(ss, ArrayList(wdScripts))
            }
        }
        return scriptsMap;
    }


    //----------------------------------------------
    //Only used for scenario testing
    internal fun loadProcess(src: ScriptSource){
        val proc = loadProcess(src, listOf())
        processRepo.addProcess(proc)
        postloadAllProcesses()
    }
    internal fun unloadAllProcesses() {
        processRepo.deleteAllProcesses()
    }

    //------------------------------------------------------------
    fun loadProcess(src: ScriptSource, wdScripts: List<ScriptSource>) : WfProcess {

        log.info("Processing '${src.id}'")
        wdScripts.forEach{log.info("Including work data from '${it.id}'")}
        val procDefn = dslLoader.loadProcessDefinition(src, wdScripts, this.javaClass.classLoader)
        val proc = WfProcess(procDefn)

        //load workitemFiles, variables and swimlanes
        val ctx = ContextData(
                procDefn.variablesMap.values.associateBy({it.name },{variableLoader.load(it)}),
                procDefn.workItems,
                procDefn.swimlanesMap.values.associateBy({it.id }, {swimlaneLoader.load(it)}),
                proc
                )

        //load flow and nodes (only ones referred to in the flow)
        flowLoader.load(procDefn, ctx)

        proc.setData(ctx)

        return proc
    }

}





