package org.caselogik.service.definition.loader

import org.caselogik.definition.dsl.model.SwimlaneDefn
import org.caselogik.service.definition.entities.InternalSwimlane
import org.caselogik.service.definition.entities.Swimlane

class SwimlaneLoader {

    //---------------------------------------
    fun load(defn: SwimlaneDefn): Swimlane {

        var sl: Swimlane
        if(defn.type == SwimlaneDefn.getTYPE_EXTERNAL()){
            sl = Swimlane(defn.id)
        }
        else if(defn.type == SwimlaneDefn.getTYPE_INTERNAL()) {
            sl = InternalSwimlane(defn.id, defn.groups, defn.calendarUrl)
        }
        else throw RuntimeException("Unknown swimlane type "+defn.type+" in definition "+defn)
        return sl
    }

}