package org.caselogik.service.definition.loader

import org.caselogik.service.CLRuntimeException
import org.caselogik.definition.dsl.model.ConstValueSource
import org.caselogik.definition.dsl.model.EnvVariableSource
import org.caselogik.definition.dsl.model.VariableDefn
import org.caselogik.service.definition.entities.Variable

class VariableLoader {

    //------------------------------------------------
    fun load(defn: VariableDefn): Variable {

        var value: String? = ""
        val source = defn.source
        when (source) {
            is ConstValueSource -> value = source.value
            is EnvVariableSource -> value = getValue(source, defn._scriptLocation)
        }
        return Variable(defn.name, value!!)
    }

    //-------------------------------------------------------------------------
    private fun getValue(source: EnvVariableSource, srcLocation: String): String {
            val env = System.getenv()
        val name = source.envVarName.toUpperCase()
        val value = env[name]
        if (value == null) throw CLRuntimeException(CLRuntimeException.ENV_VAR_MISSING, "Environment variable '%s' is not set. %s", name, srcLocation)
        return value
    }
}