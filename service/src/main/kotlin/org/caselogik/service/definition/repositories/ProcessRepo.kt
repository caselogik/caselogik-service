package org.caselogik.service.definition.repositories

import org.caselogik.service.CLRuntimeException
import org.caselogik.service.definition.entities.*
import org.caselogik.service.enactment.util.EnactmentContext
import org.caselogik.service.enactment.repositories.WorkItemMapper
import kotlin.reflect.KClass

class ProcessRepo

    constructor(private val eCtx: EnactmentContext) {

    private val processCache = HashMap<String, WfProcess>()

    //-------------------------------
    fun addProcess(proc: WfProcess) {

        if(processCache.containsKey(proc.id)) {
            throw CLRuntimeException(CLRuntimeException.DUPLICATE_PROCESS_DEFN, "Another process with id '%s' already exists. Check for duplicate process ids in definition scripts.", proc.id)
        }
        proc.setEnactmentContext(eCtx)
        proc.workItemsMap.values.forEach{ WorkItemMapper.putWorkItemType(it)}

        processCache.put(proc.id, proc)
    }

    //------------------------------------------------------------
    lateinit var allProcessNodesInUniverse: Map<NodeUid, ProcessNode>

    fun getProcessNodeByUid(pNodeUid: NodeUid): ProcessNode {
        return getNodeFromUid(allProcessNodesInUniverse, pNodeUid)
    }

    //-----------------------------------------------------------------------
    inline fun <reified T : Any> getProcessNodesMapOfType(clazz: KClass<T>): Map<NodeUid, T> {
        return allProcessNodesInUniverse.values.filter { it is T }.associateBy({ it.uid }, { it as T })
    }

    //-----------------------------------------------------------------
    private fun <T> getNodeFromUid(map: Map<NodeUid, T>, uid: NodeUid): T {
        return map.getOrElse(uid) {
            throw CLRuntimeException(CLRuntimeException.NODE_NOT_FOUND, "Node with uid '%s' not found.", uid.toString())
        }
    }

    //-----------------------------------------------
    fun getProcess(id: String): WfProcess {
        if(!processCache.containsKey(id)) {
            throw CLRuntimeException(CLRuntimeException.PROCESS_NOT_FOUND, "Process '%s' not found in repository.", id)
        }
        return processCache[id]!!
    }

    //---------------------------------------
    fun getProcesses(): Collection<WfProcess>{
        return processCache.values;
    }
    //---------------------------------------
    fun deleteAllProcesses(){
        processCache.clear()
        allProcessNodesInUniverse = mapOf()

    }

}