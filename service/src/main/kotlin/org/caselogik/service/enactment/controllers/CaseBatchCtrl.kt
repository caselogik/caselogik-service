package org.caselogik.service.enactment.controllers

import org.caselogik.api.WorkBatch
import org.caselogik.api.commands.wfcase.*
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.controllers.helpers.CaseCtrlHelper
import org.caselogik.service.enactment.events.*
import org.caselogik.service.enactment.entities.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class CaseBatchCtrl

    @Autowired
    constructor(private val processRepo: ProcessRepo,
                private val helper: CaseCtrlHelper,
                private val eventSender: EventSender) {



    //************ COMMANDS *************

    fun onCommand(cmd: CreateCaseBatch): CaseBatchCmdResponse {

        val proc = processRepo.getProcess(cmd.processId)
        val wb = proc.start.toWorkData(cmd.workBatchJson) as WorkBatch

        //Create the wi tokens and cases for each item in the batch
        val wiTokens = mutableListOf<WorkItemToken>()
        val cases = mutableListOf<WfCase>()
        for (wi in wb.items) {
            val wiToken = helper.createToken(wi)
            val case = helper.createCase(proc.id, wiToken)
            wiTokens.add(wiToken)
            cases.add(case)
        }
        val wbToken = helper.createToken(wb, wiTokens)
        wb.items = mutableListOf() //clear items in work batch as they are now in the WorkBatchToken.items.dataObject

        val caseBatch = helper.createCaseBatch(proc.id, wbToken, cases)
        eventSender.send(CaseBatchCreated(caseBatch.id))
        cases.forEach{
            eventSender.send(CaseCreated(it.id))
        }
        return okCaseBatchResponse(caseBatch.id)
    }

    //**************** EVENTS *******************


    fun onEvent(event: CaseBatchEvent) {

        when(event){
            is CaseBatchCreated -> {
                val proc = processRepo.getProcess(event.caseBatch.processId)
                val token = event.caseBatch.tokens[0]
                proc.start.transitionIn(token)
            }
        }

    }

    //----------------------------------
    fun loadEvent(event: CaseBatchEvent){
        val caseBatch = helper.fetchCaseBatch(event.caseBatchId)
        event.caseBatch = caseBatch
    }

    //----------- HELPERS -------------------------------
    private fun okCaseBatchResponse(caseBatchId: Long, message: String = ""): CaseBatchCmdResponse {
        return CaseBatchCmdResponse(caseBatchId, "Command successful. $message")
    }

}