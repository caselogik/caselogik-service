package org.caselogik.service.enactment.controllers

import org.caselogik.service.CLRuntimeException
import org.caselogik.api.WorkItem
import org.caselogik.api.commands.wfcase.*
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.controllers.helpers.CaseCtrlHelper
import org.caselogik.service.enactment.events.*
import org.caselogik.service.enactment.entities.*
import org.caselogik.service.enactment.repositories.NoteRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class CaseCtrl

    @Autowired
    constructor(private val processRepo: ProcessRepo,
                private val helper: CaseCtrlHelper,
                private val noteRepo: NoteRepo,
                private val eventSender: EventSender) {



    //************ COMMANDS *************

    fun onCommand(cmd: CreateCase): CaseCmdResponse {

        val proc = processRepo.getProcess(cmd.processId)
        val wi = proc.start.toWorkData(cmd.workItemJson) as WorkItem
        val token = helper.createToken(wi)
        val case = helper.createCase(proc.id, token)
        val event = CaseCreated(case.id)
        eventSender.send(event)
        return okCaseResponse(case.id)
    }


    //---------------------------------------------------
    fun onCommand(cmd: CreateCaseNote): CaseCmdResponse {
        val case = helper.fetchCase(cmd.caseId)
        val note = newNote(cmd.noteContent, cmd.noteAuthorId)
        case.caseNotes.add(note)
        eventSender.send(CaseNoteCreated(case.id, note.id))
        return okCaseResponse(case.id, ": noteId = ${note.id}")
    }

    //---------------------------------------------------
    fun onCommand(cmd: AppendToCaseNote): CaseCmdResponse {
        val case = helper.fetchCase(cmd.caseId)
        if(case.caseNotes.filter{ it.id == cmd.parentNoteId}.isEmpty()){
            return errorCaseResponse(case.id, "Parent case note with id ${cmd.parentNoteId} not found")
        }

        val note = newNote(cmd.noteContent, cmd.noteAuthorId, cmd.parentNoteId)
        case.caseNotes.add(note)
        eventSender.send(CaseNoteAppendedTo(case.id, note.id, note.parentNoteId!!))
        return okCaseResponse(case.id, ": noteId = ${note.id}")
    }

    //-----------------------------------------
    private fun newNote(content: String,
                        authorId: String,
                        parentNoteId: Long? = null): CaseNote{
        val note = CaseNote()
        note.content = content
        note.authorId = authorId
        note.timestamp = LocalDateTime.now()
        note.parentNoteId = parentNoteId
        noteRepo.save(note)
        return note
    }

    //-----------------------------------------------
    fun onCommand(cmd: CancelCase): CaseCmdResponse {
        val case = helper.fetchCase(cmd.caseId)

        //Cancels tokens of the case
        case.updateStatus(CaseStatus.Cancelled)
            case.tokens.forEach{
            it.updateStatus(TokenStatus.Dead)
            it.cancelWork()
        }
        return okCaseResponse(case.id)
    }

    //**************** EVENTS *******************

    fun onEvent(event: CaseEvent) {

        when(event){
            is CaseCreated -> {
                val token = event.case.tokens[0]
                if(token.status == TokenStatus.Active) { //Don't start process if token is inactive i.e. part of BatchToken
                    val proc = processRepo.getProcess(event.case.processId)
                    proc.start.transitionIn(token)
                }
            }
        }
    }

    //----------------------------------
    fun loadEvent(event: CaseEvent){
        val case = helper.fetchCase(event.caseId)
        event.case = case
    }

    //----------- HELPERS -------------------------------
    private fun okCaseResponse(caseId: Long, message: String = ""): CaseCmdResponse {
        return CaseCmdResponse(caseId, "OK" + message)
    }
    private fun errorCaseResponse(caseId: Long, msg: String): CaseCmdResponse {
        return CaseCmdResponse(caseId, "Error: $msg")
    }

}