package org.caselogik.service.enactment.controllers

import org.caselogik.api.ExternalEvent
import org.caselogik.api.commands.FireExternalEvent
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.events.EventSender
import org.caselogik.service.enactment.events.entities.ExternalEventRecord
import org.caselogik.service.enactment.events.ExternalEventLog
import org.caselogik.service.loggerFor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ProcessCtrl

    @Autowired
    constructor(private val processRepo: ProcessRepo,
                private val eeLog: ExternalEventLog,
                private val eventSender: EventSender){

    val log = loggerFor<ProcessCtrl>()


    //---------------------------------------------
    fun onCommand(cmd: FireExternalEvent): Boolean {
        val id = "${cmd.eventName}-${cmd.eventKey}"
        if (!eeLog.findById(id).isPresent){
            val er = ExternalEventRecord()
            er.id = id
            er.name = cmd.eventName
            er.key = cmd.eventKey
            eeLog.save(er)

            val evt = ExternalEvent(cmd.eventName, cmd.eventKey)
            eventSender.send(evt)
            return true
        }
        else return false
    }

    //----------------------------------------------
    fun onEvent(event: ExternalEvent) {

        processRepo.getProcesses().forEach{
            it.onEvent(event)
        }
    }

}