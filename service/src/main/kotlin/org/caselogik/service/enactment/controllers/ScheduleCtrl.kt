package org.caselogik.service.enactment.controllers

import org.caselogik.service.config.CLConfiguration
import org.caselogik.service.enactment.util.cluster.ClusterStateEvaluator
import org.caselogik.service.enactment.events.EventFactory
import org.caselogik.service.enactment.events.EventSender
import org.caselogik.service.enactment.events.ScheduledEvent
import org.caselogik.service.enactment.events.entities.PersistedScheduledEvent
import org.caselogik.service.enactment.events.ScheduledEventRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit
import kotlin.reflect.KClass

@Component
class ScheduleCtrl

    @Autowired
    constructor(private val seRepo: ScheduledEventRepo,
                private val eventFactory: EventFactory,
                private val eventSender: EventSender,
                private val clusterStateEvaluator: ClusterStateEvaluator,
                private val config: CLConfiguration){

    private val tasks = ConcurrentHashMap<Long, ScheduledEventTask>()
    private val scheduler = Executors.newSingleThreadScheduledExecutor()

    private var heartbeatInterval = config.getClockTickInterval()


    //-----------------------------------
    init {
         //Clock ticks every n seconds
        scheduler.scheduleAtFixedRate({
           onClockTick()
        }, heartbeatInterval.toMillis(), heartbeatInterval.seconds, TimeUnit.SECONDS)
    }

    //--------------------------------------
    fun onClockTick(){
        clusterStateEvaluator.evaluateClusterState()
        if(clusterStateEvaluator.isSingleton) loadScheduledEvents()
    }

    //--------------------------------------------
    fun loadScheduledEvents() {
        val events = seRepo.getScheduledEventsWithin(heartbeatInterval)
        for(event in events){
            if(!tasks.containsKey(event.id)){ //if not already scheduled
                //if event is in the past execute immediately
                if(event.isDue()){
                    runAfter(event, 0)
                }
                //else if within the next 15 mins then schedule for execution
                else  runAfter(event, event.getSecsToFiring())
            }
        }
    }

    //--------------------------------------------------------------------------------------------
    fun <T: ScheduledEvent> scheduleAt(eventType: KClass<T>, targetKey: String, fireAt: ZonedDateTime){

        val psEvent = PersistedScheduledEvent()
        psEvent.eventType = eventType.simpleName!!
        psEvent.targetKey = targetKey
        psEvent.setFiringTime(fireAt)
        runAfter(psEvent, psEvent.getSecsToFiring())
    }


    //--------------------------------------------------------------------------------------------------
    fun <T: ScheduledEvent> scheduleAfterSecs(eventType: KClass<T>, targetKey: String, fireAfterSecs: Long){

        val psEvent = PersistedScheduledEvent()
        psEvent.eventType = eventType.simpleName!!
        psEvent.targetKey = targetKey
        psEvent.setFiringTime(ZonedDateTime.now().plusSeconds(fireAfterSecs))

        runAfter(psEvent, fireAfterSecs)
    }

    //------------------------------------
    fun deleteScheduledEvent(persistedId: Long){
        if(tasks.containsKey(persistedId)){
            val task = tasks[persistedId]!!
            task.future.cancel(true)
            tasks.remove(persistedId)
        }
        seRepo.delete(persistedId)
    }

    //--------------------------------------------------------------------------
    private fun runAfter(psEvent: PersistedScheduledEvent, afterSecs: Long){
        seRepo.save(psEvent)
        val task = ScheduledEventTask(psEvent, eventFactory, eventSender, clusterStateEvaluator)
        task.future = scheduler.schedule(task, afterSecs, TimeUnit.SECONDS)
        tasks.put(psEvent.id, task)
    }



}

//=================================================================
class ScheduledEventTask (val psEvent: PersistedScheduledEvent,
                          val eventFactory: EventFactory,
                          val eventSender: EventSender,
                          val clusterStateEvaluator: ClusterStateEvaluator): Runnable {

    lateinit var future: ScheduledFuture<*>

    @Transactional
    override fun run() {
        if(clusterStateEvaluator.isSingleton) { //Only if this instance has the baton
            val event = eventFactory.createScheduledEvent(psEvent)
            eventSender.send(event)
        }
    }

}