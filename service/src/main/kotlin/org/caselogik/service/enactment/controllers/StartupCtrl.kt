package org.caselogik.service.enactment.controllers

import org.caselogik.service.definition.loader.ProcessLoader
import org.caselogik.service.loggerFor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

@Component
class StartupCtrl

    @Autowired
    constructor(private val processLoader: ProcessLoader): CommandLineRunner{

    val log = loggerFor<StartupCtrl>()

    override fun run(vararg args: String?) {
        log.info("Starting Caselogik Service ...")
        processLoader.loadAllDefinitions()
        log.info("Caselogik Service started")
    }
}