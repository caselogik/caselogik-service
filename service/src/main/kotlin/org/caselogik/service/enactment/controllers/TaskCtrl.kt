package org.caselogik.service.enactment.controllers

import org.caselogik.api.WorkData
import org.caselogik.api.commands.task.*
import org.caselogik.service.definition.entities.NodeUid
import org.caselogik.service.definition.entities.TaskNode
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.controllers.helpers.TaskCmdValidator
import org.caselogik.service.enactment.events.*
import org.caselogik.service.enactment.gateways.ExecutionGateway
import org.caselogik.service.enactment.gateways.ExecutionStatusUpdateListener
import org.caselogik.service.enactment.mappers.WorkerTOMapper
import org.caselogik.service.enactment.entities.Task
import org.caselogik.service.enactment.entities.TaskStatus
import org.caselogik.service.enactment.entities.Worker
import org.caselogik.service.enactment.entities.WorkerType
import org.caselogik.service.enactment.repositories.TaskRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime
import org.caselogik.api.Worker as WorkerTO

@Component
class TaskCtrl

    @Autowired
    constructor(private val processRepo: ProcessRepo,
                private val taskRepo: TaskRepo,
                private val executionGateway: ExecutionGateway,
                private val eventSender: EventSender,
                private val cmdValidator: TaskCmdValidator) : ExecutionStatusUpdateListener {

    //-----------------------------------
    init{
        executionGateway.setExecStatusUpdateListener(this)
    }

    //*********** ALLOCATION *************

    // AllocateTask -> TaskAllocated
    @Transactional
    fun onCommand(cmd: AllocateTask): TaskCmdResponse {
        val task = loadTask(cmd)
        return cmdValidator.applyCmd(task, cmd) {
            val (fromTO, toTO) = changeAllocation(task, cmd.toWorker)
            val event = TaskAllocated(task.id, fromTO, toTO)
            eventSender.send(event)
        }
    }

    //------------------------------------------
    // DeallocateTask -> TaskDeallocated
    @Transactional
    fun onCommand(cmd: DeallocateTask): TaskCmdResponse {
        val task = loadTask(cmd)
        return cmdValidator.applyCmd(task, cmd) {
            if(task.status==TaskStatus.Active) task.changeStatus(TaskStatus.Inactive)
            val (fromTO, _) = changeAllocation(task, Worker.none())
            val event = TaskDeallocated(task.id, fromTO)
            eventSender.send(event)
        }
    }

    //----------------------------------------------
    // ClaimTaskInQueue -> TaskClaimed
    @Transactional
    fun onCommand(cmd: ClaimTaskInQueue): TaskCmdResponse {

        val tasks = taskRepo.getTasksInQueue(cmd.processId, cmd.taskNodeId)
        if(tasks.size > 0){
            val task = tasks[0]
            val (_, toWorkerTO) = changeAllocation(task, cmd.byWorker)
            val event = TaskClaimed(task.id, toWorkerTO)
            eventSender.send(event)
            return TaskCmdResponse(event.taskId, "OK")
        }
        else {
            return TaskCmdResponse(-1, "No tasks in queue")
        }
    }

    //------------------------------------------
    // ReferTask -> TaskReferred
    @Transactional
    fun onCommand(cmd: ReferTask): TaskCmdResponse {
        val task = loadTask(cmd)

        return cmdValidator.applyCmd(task, cmd) {
            task.referrer = task.worker
            val (fromTO, toTO) = changeAllocation(task, cmd.toWorker)
            val event = TaskReferred(task.id, fromTO, toTO)
            eventSender.send(event)
        }
    }

    //-----------------------------------------------------
    // RecordTaskReferralCompleted -> TaskReferralCompleted
    @Transactional
    fun onCommand(cmd: RecordTaskReferralCompleted): TaskCmdResponse {
        val task = loadTask(cmd)
        return cmdValidator.applyCmd(task, cmd) {
            val (fromTO, toTO) = changeAllocation(task, task.referrer)
            task.referrer = Worker.none()
            val event = TaskReferred(task.id, fromTO, toTO)
            eventSender.send(event)
        }
    }

    //*********** EXECUTION STATUS UPDATE *************

    //Called when a synchronous response is received by the ExecutionGateway
    override fun processExecutionStatusUpdate(update: ExecutionStatusUpdate) {
        when(update) {
            is RecordTaskCompleted -> onCommand(update)
            is RecordTaskFailed -> onCommand(update)
            is RecordTaskProgress -> onCommand(update)
            else -> throw RuntimeException("Error - Unexpected ExecutionStatusUpdate '${update.javaClass.simpleName}'")
        }
    }

    //---------------------------------------------
    // RecordTaskFailed -> TaskFailed
    @Transactional
    fun onCommand(cmd: RecordTaskFailed): TaskCmdResponse {
        val task = loadTask(cmd)
        return cmdValidator.applyCmd(task, cmd) {
            task.changeStatus(TaskStatus.Failed)
            if(cmd.workDataJson!=null){
                task.token.workData.json = cmd.workDataJson
                task.execDetails?.isWorkDataUpdated = true
            }
            task.execDetails?.lastFailureReason = cmd.reason;
            val event = TaskFailed(task.id, cmd.reason)
            eventSender.send(event)
        }
    }

    //---------------------------------------------
    // RecordTaskProgress -> TaskInProgress
    @Transactional
    fun onCommand(cmd: RecordTaskProgress): TaskCmdResponse {
        val task = loadTask(cmd)
        return cmdValidator.applyCmd(task, cmd) {
            if(cmd.workDataJson!=null){
                task.token.workData.json = cmd.workDataJson
                task.execDetails?.isWorkDataUpdated = true
            }
            task.execDetails?.lastProgressUpdateAt = LocalDateTime.now()
            val event = TaskInProgress(task.id)
            eventSender.send(event)
        }

    }

    //---------------------------------------------
    // RecordTaskCompleted -> TaskCompleted
    @Transactional
    fun onCommand(cmd: RecordTaskCompleted): TaskCmdResponse {
        //only command valid for all worker types

        val task = loadTask(cmd)
        return cmdValidator.applyCmd(task, cmd) {
            task.changeStatus(TaskStatus.Completed)
            if (cmd.workDataJson != null) {
                //make sure the json is valid
                val taskNode = processRepo.getProcessNodeByUid(NodeUid.new(task.processId, task.taskNodeId))
                taskNode.toWorkData(cmd.workDataJson) //throws exception is json is invalid

                task.token.workData.json = cmd.workDataJson
                task.execDetails?.isWorkDataUpdated = true
            }

            val event = TaskCompleted(task.id, task.workDataJson(), task.workDataType())
            eventSender.send(event)
        }

    }


    //************* OTHER ***************

    //ActivateTask -> TaskActivated
    @Transactional
    fun onCommand(cmd: ActivateTask): TaskCmdResponse {
        val task = loadTask(cmd)
        return cmdValidator.applyCmd(task, cmd) {
            task.changeStatus(TaskStatus.Active)
            val event = TaskActivated(task.id)
            eventSender.send(event)
        }
    }

    //---------------------------------------------
    //InactivateTask -> TaskInactivated
    @Transactional
    fun onCommand(cmd: InactivateTask): TaskCmdResponse {
        val task = loadTask(cmd)
        return cmdValidator.applyCmd(task, cmd) {
            task.changeStatus(TaskStatus.Inactive)
            val event = TaskInactivated(task.id)
            eventSender.send(event)
        }
    }

    //---------------------------------------------
    //ChangeTaskPriority -> TaskPriorityChanged
    @Transactional
    fun onCommand(cmd: ChangeTaskPriority): TaskCmdResponse {
        val task = loadTask(cmd)
        return cmdValidator.applyCmd(task, cmd) {
            task.priority = cmd.newPriority
            val event = TaskPriorityChanged(task.id)
            eventSender.send(event)
        }
    }

    //---------------------------------------------
    //RetryFailedTask -> FailedTaskRetried
    @Transactional
    fun onCommand(cmd: RetryFailedTask): TaskCmdResponse {
        val task = loadTask(cmd)
        return cmdValidator.applyCmd(task, cmd) {
            task.changeStatus(TaskStatus.Active)
            eventSender.send(TaskActivated(task.id))

            task.execDetails!!.lastAttemptAt = LocalDateTime.now()
            executionGateway.retryFailedTask(task)
            val event = FailedTaskRetried(task.id)
            eventSender.send(event)
        }
    }


    //************* EVENTS ***************

    fun onEvent(event: TaskEvent) {
        val taskNode = processRepo.allProcessNodesInUniverse[event.task.taskNodeUid()] as TaskNode
        taskNode.onEvent(event)
    }

    //----------------------------------
    fun loadEvent(event: TaskEvent){
        val task = loadTask(event)
        event.task = task
    }

    //************* HELPERS ***************

    private fun changeAllocation(task: Task, newWorker: WorkerTO): Pair<WorkerTO, WorkerTO>{
        val oldWorker = task.worker
        task.worker = WorkerTOMapper.toWorker(newWorker)
        return Pair(WorkerTOMapper.fromWorker(oldWorker), newWorker)
    }
    private fun changeAllocation(task: Task, newWorker: Worker): Pair<WorkerTO, WorkerTO>{
        val oldWorker = task.worker
        task.worker = newWorker
        return Pair(WorkerTOMapper.fromWorker(oldWorker), WorkerTOMapper.fromWorker(newWorker))
    }

    //-----------------------------------------------
    private fun loadTask(msg: TaskEvent): Task = taskRepo.getById(msg.taskId)
    private fun loadTask(msg: TaskCmd): Task = taskRepo.getById(msg.taskId)


}