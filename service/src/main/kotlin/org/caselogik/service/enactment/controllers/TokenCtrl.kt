package org.caselogik.service.enactment.controllers

import org.caselogik.service.definition.entities.TokenRestNode
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.events.TimeoutEvent
import org.caselogik.service.enactment.events.TokenRoutingEvent
import org.caselogik.service.enactment.repositories.TokenRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class TokenCtrl

    @Autowired
    constructor(private val processRepo: ProcessRepo,
                private val tokenRepo: TokenRepo){

    //----------------------------------
    fun onEvent(event: TimeoutEvent) {
        val node = processRepo.allProcessNodesInUniverse[event.token.currentNodeUid()] as TokenRestNode
        node.onEvent(event)
     }

    //----------------------------------
    fun loadEvent(event: TimeoutEvent){
        val token = tokenRepo.getById(event.tokenId)
        event.token = token
    }
    //----------------------------------
    fun loadEvent(event: TokenRoutingEvent) {
        val token = tokenRepo.getById(event.tokenId)
        event.token = token
    }

}