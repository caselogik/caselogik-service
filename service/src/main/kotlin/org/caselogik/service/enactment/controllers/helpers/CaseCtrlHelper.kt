package org.caselogik.service.enactment.controllers.helpers

import org.caselogik.api.WorkBatch
import org.caselogik.api.WorkItem
import org.caselogik.service.enactment.entities.*
import org.caselogik.service.enactment.repositories.CaseBatchRepo
import org.caselogik.service.enactment.repositories.CaseRepo
import org.caselogik.service.enactment.repositories.TokenRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class CaseCtrlHelper

    @Autowired
    constructor(private val tokenRepo: TokenRepo,
            private val caseRepo: CaseRepo,
            private val caseBatchRepo: CaseBatchRepo){


    //--------------------------------------
    fun fetchCase(caseId: Long): WfCase {
        return caseRepo.getById(caseId)
    }

    //--------------------------------------
    fun fetchCaseBatch(caseBatchId: Long): CaseBatch {
        return caseBatchRepo.getById(caseBatchId)
    }


    //--------------------------------------
    fun createToken(wi: WorkItem): WorkItemToken {
        val wie = WorkItemE().apply { dataObject = wi }
        val wiToken = WorkItemToken()
        wiToken.workData = wie
        tokenRepo.save(wiToken)
        return wiToken;
    }

    //--------------------------------------
    fun createCase(processId: String,
                            wiToken: WorkItemToken): WfCase {
        wiToken.processId = processId
        val case = WfCase()
        case.processId = processId
        case.originalWorkItem = wiToken.workData as WorkItemE
        case.key = case.originalWorkItem.dataObject.key
        case.createdAt = LocalDateTime.now()
        wiToken.case = case
        case.tokens.add(wiToken)
        caseRepo.save(case)

        return case
    }

    //-------------------------------------------------
    fun createToken(wb: WorkBatch, wiTokens:List<WorkItemToken>): WorkBatchToken {

        val wbe = WorkBatchE()
        wbe.items = wiTokens.map{ it.workData as WorkItemE }.toMutableList()
        wbe.dataObject = wb

        val wbToken = WorkBatchToken()
        wbToken.workData = wbe
        for (wiToken in wiTokens) {
            wiToken.updateStatus(TokenStatus.Inactive)
            wiToken.batchToken = wbToken
            wbToken.itemTokens.add(wiToken)
        }
        tokenRepo.save(wbToken)
        return wbToken
    }

    //-------------------------------------------------------------------
    fun createCaseBatch(processId: String,
                                 wbToken: WorkBatchToken,
                                 cases: List<WfCase>): CaseBatch {
        wbToken.processId = processId
        val caseBatch = CaseBatch()
        caseBatch.processId = processId
        caseBatch.originalWorkBatch = wbToken.workData as WorkBatchE
        caseBatch.key = caseBatch.originalWorkBatch.dataObject.key
        caseBatch.createdAt = LocalDateTime.now()
        caseBatch.tokens.add(wbToken)
        wbToken.caseBatch = caseBatch

        for (case in cases) {
            caseBatch.cases.add(case)
            case.caseBatch = caseBatch
        }

        caseBatchRepo.save(caseBatch)
        return caseBatch
    }

}