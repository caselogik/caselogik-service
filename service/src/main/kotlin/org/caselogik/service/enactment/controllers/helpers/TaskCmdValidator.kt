package org.caselogik.service.enactment.controllers.helpers

import org.caselogik.api.commands.task.*
import org.caselogik.service.enactment.entities.Task
import org.caselogik.service.enactment.entities.TaskStatus
import org.caselogik.service.enactment.entities.WorkerType
import org.springframework.stereotype.Component
import java.lang.Exception

@Component
class TaskCmdValidator {

    private val validStatusChanges: HashMap<TaskStatus, Set<TaskStatus>> = HashMap()

    init {
        //Valid state transitions
        validStatusChanges.put(TaskStatus.Inactive, setOf(TaskStatus.Cancelled, TaskStatus.Active))
        validStatusChanges.put(TaskStatus.Active, setOf(TaskStatus.Inactive, TaskStatus.Failed, TaskStatus.Suspended, TaskStatus.Completed))
        validStatusChanges.put(TaskStatus.Failed, setOf(TaskStatus.Active))
        validStatusChanges.put(TaskStatus.Suspended, setOf(TaskStatus.Active))

    }

    //----------------------------------------------------
    fun applyCmd(task: Task, cmd: TaskCmd, action: () -> Unit ):TaskCmdResponse{

        val errorMessage = checkCmd(task, cmd)
        if(errorMessage == "OK") {
            try {
                action()
            }
            catch(ex: Exception){
                return TaskCmdResponse(task.id, "Error: ${ex.message}")
            }
            return TaskCmdResponse(task.id, "OK")
        }
        else return TaskCmdResponse(task.id, "Error: $errorMessage")

    }

    //----------------------------------------------------
    private fun checkCmd(task: Task, cmd: TaskCmd): String {

        if(cmd is AllocateTask && task.worker.type != WorkerType.None){
            return "Task is not in queue"
        }
        if(cmd is ReferTask && task.isReferred()){
            return "Task has already been referred"
        }
        if(cmd is RecordTaskReferralCompleted && !task.isReferred()){
            return "Task has not been referred"
        }
        if(cmd is ChangeTaskPriority && cmd.newPriority > 99){
            return "Task priority value must be less than 100"
        }
        if(cmd is RetryFailedTask && task.status != TaskStatus.Failed){
            return "Task is not in 'failed' state"
        }
        if(task.worker.type != WorkerType.Human){
            when(cmd){
                is DeallocateTask,
                is ReferTask,
                is RecordTaskReferralCompleted -> return "Worker type is not 'human'"
            }
        }
        if(task.worker.type != WorkerType.System){
            when(cmd){
                is RecordTaskFailed,
                is RecordTaskProgress,
                is RetryFailedTask -> return "Worker type is not 'system'"
            }
        }
        if(task.worker.type == WorkerType.ExternalParty){
            when(cmd){
                is ActivateTask,
                is ChangeTaskPriority -> return "Worker type is 'externalParty'"
            }
        }

        when(cmd) {
            is RecordTaskFailed -> return checkStatusChange(task.status, TaskStatus.Failed)
            is RecordTaskCompleted -> return checkStatusChange(task.status, TaskStatus.Completed)
            is ActivateTask -> return checkStatusChange(task.status, TaskStatus.Active)
            is InactivateTask -> return checkStatusChange(task.status, TaskStatus.Inactive)
        }

        return "OK";

    }

    //-----------------------------------------------------------------------
    private fun checkStatusChange(currentStatus: TaskStatus, newStatus: TaskStatus): String {
        if (currentStatus isIn validStatusChanges.keys && newStatus isIn validStatusChanges[currentStatus]!!) return "OK"
        else return "Cannot change task status from $currentStatus to $newStatus"
    }

}