package org.caselogik.service.enactment.entities

import org.caselogik.api.WorkBatch
import java.time.LocalDateTime
import javax.persistence.*

@Entity
class CaseBatch() {

    //--------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "caseBatchGen")
    @SequenceGenerator(name="caseBatchGen", sequenceName = "seq_casebatch")
    var id: Long = 0

    //--------------------------------
    lateinit var processId: String

    //--------------------------------
    @Column(name="cb_key")
    lateinit var key: String

    //-------------------------------
    lateinit var createdAt: LocalDateTime

    //--------------------------------
    @OneToMany(mappedBy = "caseBatch")
    var tokens: MutableList<WorkBatchToken> = ArrayList()

    //--------------------------------
    @Enumerated(EnumType.STRING)
    var status = CaseBatchStatus.Active

    //--------------------------------
    @OneToMany(mappedBy = "caseBatch",
            fetch = FetchType.LAZY)
    var cases: MutableList<WfCase> = ArrayList()

    //-----------------------------------
    @OneToOne(cascade = arrayOf(CascadeType.ALL),
            fetch = FetchType.LAZY)
    lateinit var originalWorkBatch: WorkBatchE

    //------------------------------------
    fun getOriginalWorkData(): WorkBatch {
        return originalWorkBatch.dataObject as WorkBatch
    }

}

//=============================
enum class CaseBatchStatus {
    Active,     //After start, unless all child cases are Completed or Cancelled
    Completed,  //When all child cases are Completed
    Suspended,  //When case batch is Suspended, resulting it all child cases getting Suspended
    Cancelled  //When case batch is Cancelled, resulting it all child cases getting Cancelled
}