package org.caselogik.service.enactment.entities

import java.time.LocalDateTime
import javax.persistence.*

@Entity
class CaseNote {

    //--------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "caseNoteGen")
    @SequenceGenerator(name="caseNoteGen", sequenceName = "seq_casenote")
    var id: Long = 0

    lateinit var content: String

    lateinit var timestamp: LocalDateTime

    lateinit var authorId: String

    var parentNoteId: Long? = null

    @ManyToOne
    lateinit var case: WfCase

}