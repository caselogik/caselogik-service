package org.caselogik.service.enactment.entities

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class ExecutionDetails{

    @Column(length = 2000)
    var lastFailureReason: String = ""

    var isWorkDataUpdated: Boolean? = null  //Boolean? because col value may be null is embedded object is absent

    var lastAttemptAt: LocalDateTime? = null

    var lastProgressUpdateAt: LocalDateTime? = null

}