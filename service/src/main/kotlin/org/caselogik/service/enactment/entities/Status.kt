package org.caselogik.service.enactment.entities

interface Status {

    infix fun <T: Status> isIn(statuses: Collection<T>) = statuses.contains(this)
}