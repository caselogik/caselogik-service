package org.caselogik.service.enactment.entities

import org.caselogik.service.definition.entities.NodeUid
import java.time.LocalDateTime
import javax.persistence.*

@Entity
class Task() {

    //-------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "taskGen")
    @SequenceGenerator(name = "taskGen", sequenceName = "seq_task")
    var id: Long = 0

    //-------------------------------
    @Embedded
    @AttributeOverrides(
            AttributeOverride(name="id",column=Column(name="worker_id")),
            AttributeOverride(name="group",column=Column(name="worker_group")),
            AttributeOverride(name="type",column=Column(name="worker_type"))
    )
    var worker : Worker = Worker.none()

    //-------------------------------
    @Embedded
    @AttributeOverrides(
        AttributeOverride(name="id",column=Column(name="referrer_id")),
        AttributeOverride(name="group",column=Column(name="referrer_group")),
        AttributeOverride(name="type",column=Column(name="referrer_type"))
    )
    var referrer : Worker = Worker.none()

    //-------------------------------
    var taskNodeId: String = ""

    //-------------------------------
    var processId: String = ""

    //-------------------------------
    @Enumerated(EnumType.STRING)
    var status: TaskStatus = TaskStatus.Inactive

    //---------------------------------
    // Lower number = higher priority
    var priority: Int = 100

    //-------------------------------

    //-------------------------------
    @Embedded
    var execDetails: ExecutionDetails? = null

    //-------------------------------
    lateinit var createdAt: LocalDateTime

    //-------------------------------
    @ManyToOne(fetch = FetchType.EAGER)
    lateinit var token: Token

    //----------- Derived Properties ---------------------
    fun isSystemTask(): Boolean = worker.type == WorkerType.System
    fun isBatchTask() = token is WorkBatchToken
    fun isReferred() = referrer.type != WorkerType.None
    fun taskNodeUid() = NodeUid.new(processId, taskNodeId)
    fun workData() = token.workData.dataObject
    fun workDataType() = workData().javaClass.canonicalName
    fun workDataJson() = token.workData.json

    //----------------------------------------
    fun changeStatus(newStatus: TaskStatus){
        status = newStatus
        if(status == TaskStatus.Completed || status == TaskStatus.Cancelled) worker = Worker.none()
    }

    //-------------------------------------------------------
    fun getAvailableActionNames(): List<String> {
        val names = getAvailableActions(status) //empty if status is not live

        //Further actions possible on 'live' tasks
        if(!names.isEmpty()) {
            when (worker.type) {

                //Tasks in queue can be allocated to a worker (Human or System)
                WorkerType.None -> names.add("allocate")

                //Deallocation applies only to Human tasks. Tasks can be deallocated only when it is not active
                WorkerType.Human -> if(status == TaskStatus.Inactive) names.add("deallocate")
                else -> {}
            }

            names.add("change-priority")
        }

        return names
    }


    //=========================================================================
    companion object {


        val availableActions : HashMap<TaskStatus, MutableList<String>> = HashMap()

        //-----------------------------------------------------------------------
        init {

            //Available actions at a state
            availableActions.put(TaskStatus.Inactive, mutableListOf("activate", "cancel"))
            availableActions.put(TaskStatus.Active, mutableListOf("complete", "suspend", "deactivate"))
            availableActions.put(TaskStatus.Suspended, mutableListOf("activate"))
            availableActions.put(TaskStatus.Failed, mutableListOf("retry-execution"))
         }

        //-----------------------------------------------------------------------
        fun getAvailableActions(currentStatus: TaskStatus): MutableList<String> =
            if(currentStatus isIn availableActions.keys) availableActions[currentStatus]!!
            else mutableListOf()
    }
}

//===============================================================
enum class TaskStatus : Status {
    Inactive,   //Initial state. Also when no being worked on actively.
    Active,     //When being worked on actively
    Failed,     //when execution fails for system tasks
    Suspended,  //When paused for a length of time
    Completed,  //When complete. Token moves on.
    Cancelled;  //When cancelled. Token moves on.

}

