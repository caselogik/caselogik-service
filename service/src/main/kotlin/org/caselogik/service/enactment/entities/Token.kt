package org.caselogik.service.enactment.entities

import org.caselogik.service.CLRuntimeException
import org.caselogik.service.definition.entities.NodeUid
import org.caselogik.service.definition.entities.WfNode
import javax.persistence.*

@Entity
@DiscriminatorColumn(name = "type")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
abstract class Token() {

    //----------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tokenGen")
    @SequenceGenerator(name="tokenGen", sequenceName = "seq_token")
    var id: Long = 0

    //-------------------------------
    lateinit var processId: String

    //-----------------------------------
    @OneToOne(cascade = arrayOf(CascadeType.ALL), fetch = FetchType.EAGER)
    lateinit var workData: WorkDataE

    //----------------------------------
    @Enumerated(EnumType.STRING)
    var status = TokenStatus.Active //initial state is Active

    //--------------------------------
    @OneToMany(mappedBy = "parentToken",
            cascade = arrayOf(CascadeType.ALL),
            fetch = FetchType.LAZY) 
    var childTokens: MutableList<Token> = ArrayList()

    //--------------------------------
    @ManyToOne(fetch = FetchType.EAGER) //needs to be eager
    lateinit var parentToken: Token

    //----------------------------------
    var currentNodeId: String = ""

    //----------------------------------
    @OneToMany(mappedBy = "token", fetch = FetchType.LAZY)
    var tasks: MutableList<Task> = ArrayList()

    //-----------------------------------
    abstract fun newChildTokenOnSplit(): Token
    abstract fun setCurrentNode(node: WfNode)

    //------------------------------
    fun currentNodeUid(): NodeUid {
        return NodeUid.new(processId, currentNodeId)
    }

    //----------------------------------------
    fun updateStatus(newStatus: TokenStatus){
        //state transition checks
        var invalid = when(newStatus) {
            TokenStatus.Active -> status != TokenStatus.Inactive //can reach Active only from Inactive
            TokenStatus.Inactive -> status != TokenStatus.Active
            TokenStatus.Dead -> false
        }
        if(invalid) throw CLRuntimeException(CLRuntimeException.INVALID_TOKEN_STATUS_CHANGE,
                "Invalid status change from '%s' to '%s' in Token with id=%s.",
                status.toString(), newStatus.toString(), id.toString())

        status = newStatus

    }

}

//=====================
enum class TokenStatus {
    Active,
    Inactive,
    Dead
}

