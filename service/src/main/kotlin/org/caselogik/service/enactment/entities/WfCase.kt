package org.caselogik.service.enactment.entities

import org.caselogik.service.CLRuntimeException
import org.caselogik.api.WorkItem
import java.time.LocalDateTime
import javax.persistence.*


@Entity
class WfCase() {

    //--------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "caseGen")
    @SequenceGenerator(name="caseGen", sequenceName = "seq_case")
    var id: Long = 0

    //--------------------------------
    lateinit var processId: String

    //--------------------------------
    @Column(name="c_key")
    lateinit var key: String

    //--------------------------------
    @Enumerated(EnumType.STRING)
    var status = CaseStatus.Active

    //-------------------------------
    lateinit var createdAt: LocalDateTime

    //--------------------------------
    @OneToMany(mappedBy = "case")
    var tokens: MutableList<WorkItemToken> = ArrayList()

    //--------------------------------
    @OneToMany(mappedBy = "hostCase")
    var attachedCases: MutableList<WfCase> = ArrayList()

    //--------------------------------
    @ManyToOne
    lateinit var hostCase: WfCase

    //-----------------------------------
    @OneToOne
    lateinit var originalWorkItem: WorkItemE

    //--------------------------------
    @ManyToOne(fetch = FetchType.LAZY)
    var caseBatch: CaseBatch? = null

    //--------------------------------
    @OneToMany(mappedBy = "case")
    var caseNotes: MutableList<CaseNote> = ArrayList()

    //----------------------------------------
    fun updateStatus(newStatus: CaseStatus){
        //state transition checks
        var invalid = when(newStatus) {
            CaseStatus.Active -> status != CaseStatus.Suspended //can reach Active only from Suspended
            CaseStatus.Attached,
            CaseStatus.Suspended -> status != CaseStatus.Active
            CaseStatus.Cancelled,
            CaseStatus.Completed -> status != CaseStatus.Active && status != CaseStatus.Attached
        }
        if(invalid) throw CLRuntimeException(CLRuntimeException.INVALID_CASE_STATUS_CHANGE,
                "Invalid status change from '%s' to '%s' in Case with id=%s.",
                status.toString(), newStatus.toString(), id.toString())

        status = newStatus

    }
}

//=============================
enum class CaseStatus {
    Active,     //When case is live i.e. after start
    Suspended,  //When case is paused for a length of time
    Completed,  //When complete
    Cancelled,  //When cancelled in the middle
    Attached    //When attached to another live case
}
