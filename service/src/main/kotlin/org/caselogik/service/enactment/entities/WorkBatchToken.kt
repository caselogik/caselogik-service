package org.caselogik.service.enactment.entities

import org.caselogik.api.WorkBatch
import org.caselogik.service.definition.entities.WfNode
import javax.persistence.*

@Entity
@DiscriminatorValue(value = "B")
class WorkBatchToken() : Token() {

    //--------------------------------------------------------
    @OneToMany(mappedBy = "batchToken", fetch = FetchType.LAZY)
    var itemTokens: MutableList<WorkItemToken> = ArrayList()

    //---------------------------------
    override fun toString(): String {
        return "WorkBatchToken(id='$id', currentNodeId='$currentNodeId', status='$status')"
    }

    //--------------------------------
    @ManyToOne(fetch = FetchType.LAZY)
    lateinit var caseBatch: CaseBatch

    //----------------------------------
    val workBatch: WorkBatch
        get(){ return workData.dataObject as WorkBatch }


    //----------------------------------------
    override fun setCurrentNode(node: WfNode) {
        this.currentNodeId = node.id
        itemTokens.forEach{ it.setCurrentNode(node) }
    }

    //-----------------------------------
    override fun newChildTokenOnSplit(): Token {
        val child = WorkBatchToken()
        child.processId = this.processId
        child.workData = this.workData
        child.parentToken = this
        child.itemTokens.addAll(this.itemTokens)
        this.childTokens.add(child)
        child.status = TokenStatus.Active
        return child
    }

}