package org.caselogik.service.enactment.entities

import org.caselogik.api.WorkData
import org.caselogik.service.enactment.repositories.WorkDataSerialiser
import javax.persistence.*

@Entity
@DiscriminatorColumn(name = "type")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@EntityListeners(WorkDataSerialiser::class)
abstract class WorkDataE() {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wdeGen")
    @SequenceGenerator(name="wdeGen", sequenceName = "seq_wde")
    var id: Long = -1;

    //For optimistic locking
    @Version
    var version: Int = -1

    @Column( length = 2000 )
    lateinit var json: String

    lateinit var typeName: String

    @Transient
    lateinit var dataObject: WorkData

}

//==================================
@Entity
@DiscriminatorValue(value = "I")
class WorkItemE() : WorkDataE() {
    //--------------------------------
    @ManyToOne(fetch = FetchType.LAZY)
    var batch: WorkBatchE? = null

    //Set to true when task or case gets cancelled
    var isCancelled: Boolean = false

    //-----------------------------------
    override fun toString(): String {
        return "WorkItem{ type='$typeName', json='$json', isCancelled=$isCancelled }"
    }
}


//==================================
@Entity
@DiscriminatorValue(value = "B")
class WorkBatchE() : WorkDataE() {

    //--------------------------------------------------------
    @OneToMany(mappedBy = "batch", fetch = FetchType.LAZY)
    var items: MutableList<WorkItemE> = ArrayList<WorkItemE>()

    //-----------------------------------
    override fun toString(): String {
        return "WorkBatch{ type='$typeName', json='$json', items=[${items.map{it.toString()}.joinToString(",")}] }"
    }
}