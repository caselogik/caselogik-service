package org.caselogik.service.enactment.entities

import org.caselogik.api.WorkItem
import org.caselogik.service.definition.entities.WfNode
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.ManyToOne

@Entity
@DiscriminatorValue(value = "I")
class WorkItemToken() : Token() {

    //--------------------------------
    @ManyToOne(fetch = FetchType.LAZY)
    lateinit var case: WfCase

    //--------------------------------
    @ManyToOne(fetch = FetchType.LAZY)
    var batchToken: WorkBatchToken? = null

    //----------------------------------
    val workItem: WorkItem
        get(){ return workData.dataObject as WorkItem }

    //----------------------------------------
    override fun setCurrentNode(node: WfNode) {
        this.currentNodeId = node.id
    }

    //---------------------------------
    override fun toString(): String {
        return "WorkItemToken(id='$id', currentNodeId='$currentNodeId', status='$status')"
    }

    //------------------------------
    fun cancelWork(){
        (workData as WorkItemE).isCancelled = true
    }
    //------------------------------
    override fun newChildTokenOnSplit(): Token {
        val child = WorkItemToken()
        this.case.tokens.add(child)
        child.case = this.case
        child.processId = this.processId
        child.workData = this.workData
        child.parentToken = this
        this.childTokens.add(child)
        child.status = TokenStatus.Active
        return child
    }
}