package org.caselogik.service.enactment.entities
import javax.persistence.Embeddable
import javax.persistence.EnumType
import javax.persistence.Enumerated

@Embeddable
class Worker() {

    var id: String? = null

    var group: String? = null

    @Enumerated(EnumType.STRING)
    lateinit var type: WorkerType

    //==========================
    companion object {

        //-----------------------------------
        fun human(id: String): Worker{
            val w = Worker()
            w.id = id
            w.type = WorkerType.Human
            return w
        }

        //-----------------------------------
        fun group(group: String): Worker{
            val w = Worker()
            w.group = group
            w.type = WorkerType.Group
            return w
        }

        //-----------------------------------
        fun none(): Worker{
            val w = Worker()
            w.type = WorkerType.None
            return w
        }

        //-----------------------------------
        fun system(id: String): Worker{
            val w = Worker()
            w.id = id
            w.type = WorkerType.System
            return w
        }

        //-----------------------------------
        fun external(id: String): Worker{
            val w = Worker()
            w.id = id
            w.type = WorkerType.ExternalParty
            return w
        }
    }
}

//=========================
enum class WorkerType {
    None,
    System,
    Human,
    Group,
    ExternalParty
}




