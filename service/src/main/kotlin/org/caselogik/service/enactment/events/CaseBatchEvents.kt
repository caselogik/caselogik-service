package org.caselogik.service.enactment.events

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.caselogik.api.Event
import org.caselogik.service.enactment.entities.CaseBatch
import java.time.LocalDateTime
import java.util.*

abstract class CaseBatchEvent : Event {
    @JsonIgnore
    lateinit var caseBatch: CaseBatch

    abstract val caseBatchId: Long

    private val _timeStamp = LocalDateTime.now()
    override fun getTimestamp(): LocalDateTime = _timeStamp
}

data class CaseBatchCreated (
        @JsonProperty("caseBatchId") override val caseBatchId: Long,
        @JsonProperty("txnSequencerTokenId") override val txnSequencerTokenId: String = UUID.randomUUID().toString())
    : CaseBatchEvent(), TxnSequencer

data class CaseBatchCompleted (@JsonProperty("caseBatchId") override val caseBatchId: Long) : CaseBatchEvent()

