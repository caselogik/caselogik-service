package org.caselogik.service.enactment.events

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.caselogik.api.Event
import org.caselogik.service.enactment.entities.WfCase
import java.time.LocalDateTime
import java.util.*

abstract class CaseEvent : Event {
    @JsonIgnore
    lateinit var case: WfCase
    abstract val caseId: Long

    private val _timeStamp = LocalDateTime.now()
    override fun getTimestamp(): LocalDateTime = _timeStamp
}
data class CaseCreated (@JsonProperty("caseId") override val caseId: Long,
                        @JsonProperty("txnSequencerTokenId") override val txnSequencerTokenId: String = UUID.randomUUID().toString())
    : CaseEvent(), TxnSequencer

data class CaseCompleted (@JsonProperty("caseId") override val caseId: Long) : CaseEvent()

data class CaseNoteCreated (@JsonProperty("caseId") override val caseId: Long,
                            @JsonProperty("noteId") val noteId: Long) : CaseEvent()

data class CaseNoteAppendedTo (@JsonProperty("caseId") override val caseId: Long,
                               @JsonProperty("noteId") val noteId: Long,
                               @JsonProperty("parentNoteId") val parentNoteId: Long) : CaseEvent()
