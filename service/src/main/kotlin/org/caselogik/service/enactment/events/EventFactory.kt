package org.caselogik.service.enactment.events

import org.caselogik.service.enactment.events.entities.PersistedScheduledEvent

class EventFactory {

    fun createScheduledEvent(psEvent: PersistedScheduledEvent) : ScheduledEvent {
        val event = when(psEvent.eventType) {
            WaitTimedOut::class.simpleName -> WaitTimedOut(psEvent.targetKey.toLong(), psEvent.id)
            else -> throw RuntimeException("Unknown event type '${psEvent.eventType}'")
        }
        return event
    }
}