package org.caselogik.service.enactment.events

import org.caselogik.api.Event

interface EventSender { fun send(event: Event) }

interface EventReceiver { fun receive(event: Event) }

const val eventQueueName = "events"
