package org.caselogik.service.enactment.events

import org.caselogik.service.enactment.events.entities.ExternalEventRecord
import org.springframework.data.repository.CrudRepository

interface ExternalEventLog : CrudRepository<ExternalEventRecord, String>