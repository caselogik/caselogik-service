package org.caselogik.service.enactment.events

import org.caselogik.api.Event
import org.caselogik.api.ExternalEvent
import org.caselogik.service.enactment.controllers.*
import org.caselogik.service.enactment.events.entities.TxnSequencerToken
import org.caselogik.service.enactment.logging.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jms.annotation.JmsListener
import org.springframework.jms.core.JmsTemplate
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

//=====================================================
@Component
class JmsEventSender
@Autowired
constructor(
        private val jmsTemplate: JmsTemplate,
        private val txnSequencerTokenJpaRepo: TxnSequencerTokenJpaRepo): EventSender {

    //------------------------------------
    override fun send(event: Event){

        if(event is TxnSequencer) {
            val tst = TxnSequencerToken()
            tst.uuid = event.txnSequencerTokenId
            txnSequencerTokenJpaRepo.save(tst)
        }

        jmsTemplate.convertAndSend(eventQueueName, event)
    }

}

//=====================================================
@Component
class JmsEventReceiver
@Autowired
constructor(private val caseCtrl: CaseCtrl,
            private val caseBatchCtrl: CaseBatchCtrl,
            private val taskCtrl: TaskCtrl,
            private val tokenCtrl: TokenCtrl,
            private val processCtrl: ProcessCtrl,
            private val logger: Logger,
            private val txnSequencerTokenJpaRepo: TxnSequencerTokenJpaRepo) : EventReceiver {


    //------------------------------------
    @JmsListener(destination = eventQueueName)
    @Transactional
    override fun receive(event: Event) {

        //wait for earlier txn to complete
        if(event is TxnSequencer) waitTillPrevTxnCompletes(event)

        //load the main object in the event
        when(event) {
            is CaseEvent -> caseCtrl.loadEvent(event)
            is CaseBatchEvent -> caseBatchCtrl.loadEvent(event)
            is TaskEvent -> taskCtrl.loadEvent(event)
            is TimeoutEvent -> tokenCtrl.loadEvent(event)
            is TokenRoutingEvent -> tokenCtrl.loadEvent(event)
            else -> println("Not loading event $event")
        }

        //add log entry
        logger.logEvent(event)

        //take actions based on event
        when(event) {
            is CaseEvent -> caseCtrl.onEvent(event)
            is CaseBatchEvent -> caseBatchCtrl.onEvent(event)
            is TaskEvent -> taskCtrl.onEvent(event)
            is TimeoutEvent -> tokenCtrl.onEvent(event)
            is ExternalEvent -> processCtrl.onEvent(event)
            else -> println("Not handling event $event")
        }


    }

    //-------------------------------------
    private fun waitTillPrevTxnCompletes(event: TxnSequencer){
        var tstOpt = txnSequencerTokenJpaRepo.findById(event.txnSequencerTokenId)
        var retryCount = 1 //retry for 3 secs

        while (!tstOpt.isPresent && retryCount <= 10) {
            Thread.sleep(100L * retryCount++)
            tstOpt = txnSequencerTokenJpaRepo.findById(event.txnSequencerTokenId)
        }

        tstOpt.ifPresent{ tst -> txnSequencerTokenJpaRepo.delete(tst)}

    }

}