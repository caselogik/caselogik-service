package org.caselogik.service.enactment.events

import org.caselogik.api.Event

interface ScheduledEvent : Event {
    val persistedId: Long
    val triggeredAt: Long

}
