package org.caselogik.service.enactment.events

import org.caselogik.service.enactment.events.entities.PersistedScheduledEvent
import org.caselogik.service.enactment.events.entities.EventSchedule
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Component
import java.time.Duration
import java.time.ZonedDateTime

@Component
class ScheduledEventRepo

    @Autowired
    constructor(private val eventJpaRepo: ScheduledEventJpaRepo,
                private val scheduleJpaRepo: EventScheduleJpaRepo) {

    //------------------------------------------------
    fun getScheduledEventsWithin(duration: Duration): Collection<PersistedScheduledEvent>{
        val timeHorizon = ZonedDateTime.now().plus(duration)
        val epochSecs = timeHorizon.toEpochSecond()
        return eventJpaRepo.findFiringBefore(epochSecs)
    }

    //-----------------------------------
    fun save(psEvent: PersistedScheduledEvent) {
        eventJpaRepo.save(psEvent)
    }

    //-----------------------------------
    fun save(schedule: EventSchedule) {
        scheduleJpaRepo.save(schedule)
    }

    //-----------------------------------
    fun delete(schedId: Long){
        scheduleJpaRepo.deleteById(schedId)
    }
}

//===================================================================
interface ScheduledEventJpaRepo : CrudRepository<PersistedScheduledEvent, Long>{
    @Query("SELECT e FROM PersistedScheduledEvent e WHERE e.fireAt <= horizon")
    fun findFiringBefore(horizon: Long): Collection<PersistedScheduledEvent>
}

//===================================================================
interface EventScheduleJpaRepo : CrudRepository<EventSchedule, Long>