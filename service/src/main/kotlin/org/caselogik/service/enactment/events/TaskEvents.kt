package org.caselogik.service.enactment.events

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.caselogik.api.Event
import org.caselogik.api.Worker
import org.caselogik.service.enactment.entities.Task
import java.time.LocalDateTime
import java.util.*

abstract class TaskEvent : Event {
    @JsonIgnore
    lateinit var task: Task
    abstract val taskId: Long

    private val _timeStamp = LocalDateTime.now()
    override fun getTimestamp(): LocalDateTime = _timeStamp
}

data class TaskCreated (@JsonProperty("taskId") override val taskId: Long,
                        @JsonProperty("txnSequencerTokenId") override val txnSequencerTokenId: String = UUID.randomUUID().toString())
    : TaskEvent(), TxnSequencer

data class TaskCompleted (@JsonProperty("taskId") override val taskId: Long,
                          @JsonProperty("workDataJson") val workDataJson: String,
                          @JsonProperty("workDataType") val workDataType: String,
                          @JsonProperty("txnSequencerTokenId") override val txnSequencerTokenId: String = UUID.randomUUID().toString())
    : TaskEvent(), TxnSequencer

data class TaskActivated (@JsonProperty("taskId") override val taskId: Long) : TaskEvent()
data class TaskInactivated (@JsonProperty("taskId") override val taskId: Long) : TaskEvent()

data class TaskPriorityChanged (@JsonProperty("taskId") override val taskId: Long) : TaskEvent()

data class TaskSuspended (@JsonProperty("taskId") override val taskId: Long) : TaskEvent()

//Allocation events
data class TaskAllocated (@JsonProperty("taskId") override val taskId: Long,
                          @JsonProperty("from") val from: Worker,
                          @JsonProperty("to") val to: Worker) : TaskEvent()

data class TaskDeallocated (@JsonProperty("taskId") override val taskId: Long,
                            @JsonProperty("from") val from: Worker) : TaskEvent()

data class TaskClaimed (@JsonProperty("taskId") override val taskId: Long,
                        @JsonProperty("by") val by: Worker) : TaskEvent()

data class TaskReferred (@JsonProperty("taskId") override val taskId: Long,
                         @JsonProperty("by") val from: Worker,
                         @JsonProperty("to") val to: Worker) : TaskEvent()

//Execution events
data class TaskFailed (@JsonProperty("taskId") override val taskId: Long,
                       @JsonProperty("reason") val reason: String) : TaskEvent()

data class TaskInProgress (@JsonProperty("taskId") override val taskId: Long) : TaskEvent()

data class FailedTaskRetried (@JsonProperty("taskId") override val taskId: Long) : TaskEvent()
