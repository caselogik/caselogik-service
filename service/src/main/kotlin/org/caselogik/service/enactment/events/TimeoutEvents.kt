package org.caselogik.service.enactment.events

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.caselogik.api.Event
import org.caselogik.service.enactment.entities.Token
import java.time.Instant
import java.time.LocalDateTime

//-----------------------------------------------
abstract class TimeoutEvent : ScheduledEvent {
    abstract val tokenId: Long

    @JsonIgnore
    lateinit var token: Token

    private val _timeStamp = LocalDateTime.now()
    override fun getTimestamp(): LocalDateTime = _timeStamp
}
//------------------------------------------------------------------------------------
data class WaitTimedOut(@JsonProperty("tokenId") override val tokenId: Long,
                        @JsonProperty("scheduledEventId") override val persistedId: Long,
                        @JsonProperty("triggeredAt") override val triggeredAt: Long = Instant.now().toEpochMilli())
    : TimeoutEvent()

//------------------------------------------------------------------------------------
data class MergeTimedOut(@JsonProperty("tokenId") override val tokenId: Long,
                         @JsonProperty("scheduledEventId") override val persistedId: Long,
                         @JsonProperty("triggeredAt") override val triggeredAt: Long = Instant.now().toEpochMilli())
    : TimeoutEvent()




