package org.caselogik.service.enactment.events

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.caselogik.api.Event
import org.caselogik.api.ExternalEvent
import org.caselogik.service.definition.entities.NodeUid
import org.caselogik.service.definition.entities.Transition
import org.caselogik.service.enactment.entities.Token
import java.time.LocalDateTime

//-----------------------------------------------
abstract class TokenRoutingEvent : Event {


    abstract val tokenId: Long
    abstract val nodeId: String

    @JsonIgnore
    lateinit var token: Token

    private val _timeStamp = LocalDateTime.now()
    override fun getTimestamp(): LocalDateTime = _timeStamp
}

//------------------------------------------------------------------------------------
data class TokenSplit(@JsonProperty("tokenId") override val tokenId: Long,
                      @JsonProperty("nodeId") override val nodeId: String,
                      @JsonProperty("parentTokenId") val parentTokenId: Long ) : TokenRoutingEvent()

//------------------------------------------------------------------------------------
data class TokenRouted(@JsonProperty("tokenId") override val tokenId: Long,
                       @JsonProperty("nodeId") override val nodeId: String,
                       @JsonProperty("toNodeId") val toNodeId: String,
                       @JsonProperty("guard") val guard: String) : TokenRoutingEvent()


//------------------------------------------------------------------------------------
data class TokenMerged(@JsonProperty("tokenId") override val tokenId: Long,
                       @JsonProperty("nodeId") override val nodeId: String,
                       @JsonProperty("parentTokenId") val parentTokenId: Long) : TokenRoutingEvent()

//------------------------------------------------------------------------------------
data class TokensBatched(@JsonProperty("tokenId") override val tokenId: Long,
                         @JsonProperty("nodeId") override val nodeId: String,
                         @JsonProperty("childTokenIds") val childTokenIds: List<Long>,
                         @JsonProperty("trigger") val trigger: ExternalEvent) : TokenRoutingEvent()

//------------------------------------------------------------------------------------
data class ExternalEventMatched(@JsonProperty("tokenId") override val tokenId: Long,
                                @JsonProperty("nodeId") override val nodeId: String,
                                @JsonProperty("externalEvent") val externalEvent: ExternalEvent) : TokenRoutingEvent()