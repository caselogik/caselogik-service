package org.caselogik.service.enactment.events


interface TxnSequencer {

    val txnSequencerTokenId: String
}