package org.caselogik.service.enactment.events

import org.caselogik.service.enactment.events.entities.TxnSequencerToken
import org.springframework.data.repository.CrudRepository

interface TxnSequencerTokenJpaRepo : CrudRepository<TxnSequencerToken, String>
