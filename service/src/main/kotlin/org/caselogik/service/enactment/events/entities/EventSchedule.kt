package org.caselogik.service.enactment.events.entities

import javax.persistence.*

@Entity
class EventSchedule {

    //-------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eventSched")
    @SequenceGenerator(name = "eventSched", sequenceName = "seq_eventsched")
    var id: Long = 0

    //-------------------------------
    var eventType: String = ""
    var cron: String = ""

}