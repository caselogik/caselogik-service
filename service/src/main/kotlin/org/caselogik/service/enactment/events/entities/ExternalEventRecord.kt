package org.caselogik.service.enactment.events.entities

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class ExternalEventRecord {

    @Id
    lateinit var id: String

    @Column(name="e_key")
    lateinit var key: String

    lateinit var name: String

}