package org.caselogik.service.enactment.events.entities

import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import javax.persistence.*

@Entity
class PersistedScheduledEvent {

    //-------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "schedEvent")
    @SequenceGenerator(name = "schedEvent", sequenceName = "seq_schedevent")
    var id: Long = 0

    //-------------------------------
    var eventType: String = ""
    var targetKey: String = ""
    var fireAt: Long = -1
    var hasFired: Boolean = false

    //-------------------------------
    fun setFiringTime(t: ZonedDateTime){
        fireAt = t.toEpochSecond()
    }

    //------------------------------------------
    @Transient
    private lateinit var dateTime: ZonedDateTime
    private fun initDateTime(){
        val i = Instant.ofEpochSecond(fireAt);
        dateTime = ZonedDateTime.ofInstant(i, ZoneId.systemDefault());
    }

    //-------------------------------------
    fun isDue(): Boolean {
        if(!::dateTime.isInitialized)initDateTime()
        val now = ZonedDateTime.now()
        return (dateTime.isEqual(now) || dateTime.isBefore(now))
    }

    //-------------------------------------
    fun getSecsToFiring(): Long {
        if(!::dateTime.isInitialized)initDateTime()
        return ZonedDateTime.now().until(dateTime, ChronoUnit.SECONDS)
    }

}