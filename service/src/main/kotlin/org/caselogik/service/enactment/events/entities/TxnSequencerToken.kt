package org.caselogik.service.enactment.events.entities

import javax.persistence.Entity
import javax.persistence.Id

@Entity
class TxnSequencerToken {

    @Id
    var uuid: String? = null
}