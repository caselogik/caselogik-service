package org.caselogik.service.enactment.gateways

import org.caselogik.api.commands.task.ExecutionStatusUpdate
import org.caselogik.service.enactment.entities.Task


interface ExecutionGateway {
    fun executeTask(task: Task)
    fun retryFailedTask(task: Task)
    fun setExecStatusUpdateListener(listener: ExecutionStatusUpdateListener)
}

interface ExecutionStatusUpdateListener {
    fun processExecutionStatusUpdate(update: ExecutionStatusUpdate)
}

const val executionQueueName = "execution-commands"

