package org.caselogik.service.enactment.gateways

import org.caselogik.service.CLRuntimeException
import org.caselogik.api.commands._internal.ExecutionCmd
import org.caselogik.api.commands.task.ExecutionStatusUpdate
import org.caselogik.api.commands.task.RecordTaskFailed
import org.caselogik.service.enactment.entities.Task
import org.caselogik.service.enactment.mappers.TaskTOMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.RequestEntity
import org.springframework.jms.annotation.JmsListener
import org.springframework.jms.core.JmsTemplate
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.ResourceAccessException
import org.springframework.web.client.RestTemplate
import java.net.URI

@Component
class JmsExecutionGateway

    @Autowired
    constructor(private val restTemplate: RestTemplate,
            private val jmsTemplate: JmsTemplate) : ExecutionGateway{

    //--------------------------------------------
    private lateinit var execStatusUpdateListener: ExecutionStatusUpdateListener
    override fun setExecStatusUpdateListener(listener: ExecutionStatusUpdateListener) {
        this.execStatusUpdateListener = listener
    }


    //---------------------------------------------
    override fun executeTask(task: Task) {
        val taskTO = TaskTOMapper.makeTO(task)
        val links = HashMap<String, String>() //TODO
        jmsTemplate.convertAndSend(executionQueueName, ExecutionCmd(taskTO, links))
    }

    //---------------------------------------------
    override fun retryFailedTask(task: Task) {
        val taskTO = TaskTOMapper.makeTO(task)
        taskTO.executionDetails!!.isRetry = true
        val links = HashMap<String, String>() //TODO
        jmsTemplate.convertAndSend(executionQueueName, ExecutionCmd(taskTO, links))
    }

    //----------------------------------------------
    @Transactional
    @JmsListener(destination = executionQueueName)
    fun receive(cmd: ExecutionCmd) {

        try {
            val response = restTemplate.getForEntity(cmd.task.executionUrl, ExecutionStatusUpdate::class.java)

            if(response.hasBody() && response.body?.taskId == null){
                val type = response.body?.javaClass?.simpleName ?: "Unknown"
                throw CLRuntimeException(CLRuntimeException.BAD_COMMAND, "taskId is missing in %s", type)
            }

            response.body?.apply { execStatusUpdateListener.processExecutionStatusUpdate(this) }

        }
        catch(e : Exception){
            e.printStackTrace()
            var reason: String
            when(e){
                is ResourceAccessException -> reason = "Failed to connect to ${cmd.task.executionUrl}"
                is HttpClientErrorException -> reason = "${cmd.task.executionUrl} returned Http Status Code ${e.statusCode}"
                else -> reason = "${e.javaClass.simpleName} thrown. ${e.message}".take(2000)
            }

            val update = RecordTaskFailed(reason, null)
            update.taskId = cmd.task.id
            execStatusUpdateListener.processExecutionStatusUpdate(update)
        }


    }

}