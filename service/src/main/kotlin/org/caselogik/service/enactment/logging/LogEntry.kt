package org.caselogik.service.enactment.logging

import java.time.LocalDateTime
import javax.persistence.*

@Entity
class LogEntry {

    //--------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "caseLogEntryGen")
    @SequenceGenerator(name="caseLogEntryGen", sequenceName = "seq_caselogentry")
    var id: Long = 0

    var caseId: Long = -1

    var caseKey: String? = null

    lateinit var eventName: String

    var tokenId: Long = -1

    var batchTokenId: Long = -1

    var parentTokenId: Long = -1

    var taskId: Long = -1

    var processId: String? = null

    var nodeId: String? = null

    var nodeType: String? = null

    var toNodeId: String? = null

    var toNodeType: String? = null

    var workerId: String? = null

    var workerType: String? = null;

    var guard: String? = null

    @Column( length = 2000 )
    var note: String? = null

    lateinit var timestamp: LocalDateTime

    lateinit var workDataType: String

    @Column( length = 2000 )
    var workDataJson: String? = null

}




