package org.caselogik.service.enactment.logging

import org.springframework.data.repository.CrudRepository

interface LogRepo : CrudRepository<LogEntry, Long> {

    fun findByCaseId(id: Long): List<LogEntry>;
}