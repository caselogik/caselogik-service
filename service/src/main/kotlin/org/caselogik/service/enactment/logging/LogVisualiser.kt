package org.caselogik.service.enactment.logging

import org.caselogik.api.GraphViz
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@Component
class CaseLogVisualiser

    @Autowired
    constructor(private val logRepo: LogRepo){

    fun render(caseId: Long): GraphViz {
        val entries = logRepo.findByCaseId(caseId)
        if(entries.size > 0) {
            return renderAsDot(entries);
        }
        return GraphViz(null, null);
    }

    //-------------------------------------------------------------------
    private fun renderAsDot(entries: List<LogEntry>): GraphViz {

        val repo = Repo()

        var num = 1
        for(e in entries) {
            when (e.eventName) {

                "CaseCreated" -> {
                    val n = CaseCreatedN(num++, e.processId!!, e.caseId, e.caseKey!!, e.workDataType, e.timestamp)
                    repo.addNode(n, e)
                }
                "TokenRouted" -> {
                    if (e.nodeId == "start") {
                        val n = StartN(num++, e.timestamp)
                        repo.addNode(n, e)
                    }

                    if (e.toNodeId == "end") {
                        val n = EndN(num++, e.timestamp)
                        repo.addNode(n, e)
                    }

                    if (e.nodeType ==  "OrGate") {
                        val n = OrN(num++, e.nodeId!!, e.guard!!, e.timestamp)
                        repo.addNode(n, e)
                    }

                    if (e.toNodeType ==  "AndGate") {
                        val n = AndN(num++, e.toNodeId!!, e.timestamp)
                        repo.addNode(n, e)
                    }

                }
                "TaskCreated" -> {
                    val n = TaskN(num++, e.taskId, e.nodeId!!, e.workerType!!, e.timestamp)
                    repo.addNode(n, e)
                    val a = TaskActionN("Created", e.timestamp)
                    n.actions.add(a)
                }

                "TaskClaimed" -> {
                    val t = repo.tasks[e.taskId]
                    val a = OwnershipChangeTaskActionN(e.workerId!!, "Claimed", e.timestamp)
                    t!!.actions.add(a)
                }

                "TaskActivated", "TaskCompleted" -> {
                    val action = when(e.eventName) {
                        "TaskActivated" -> "Activated"
                        "TaskCompleted" -> "Completed"
                        else -> ""
                    }

                    val t = repo.tasks[e.taskId]
                    val a = TaskActionN(action, e.timestamp)
                    t!!.actions.add(a)
                }
                "TokenSplit" -> {
                    val n = repo.tokenSplits[e.nodeId]!!
                    repo.currents.put(e.tokenId, n)
                }
                "CaseCompleted" -> {
                    val n = CaseCompletedN(num++, e.timestamp)
                    repo.addNode(n, e)
                }
            }
        }

        val sb = StringBuilder()
        sb.append("digraph G { rankdir=TB; ranksep=0.5; nodesep=0.5; node [shape=record, fontname=courier, fontsize=10]; edge [fontname=courier, fontsize=10];")

        for(n in repo.nodes){
            sb.append(n.renderNode()).append(" ")
        }

        for(n in repo.nodes){
            sb.append(n.renderEdge()).append(" ")
        }

        sb.append(" }")
        return GraphViz(sb.toString(), "dot")
    }

    //------------------------------
    private class Repo {

        val nodes = mutableListOf<N>()
        val currents = mutableMapOf<Long, N>();
        val tasks = mutableMapOf<Long, TaskN>()
        val ors = mutableMapOf<String, OrN>()
        val tokenSplits = mutableMapOf<String, TokenSplitN>()
        var lastEnd: N = None()

        fun addNode(n: N, e: LogEntry){

            nodes.add(n)

            when(n) {
                is OrN -> ors.put(n.id, n)
                is AndN -> tokenSplits.put(n.id, n)
                is TaskN -> tasks.put(e.taskId, n)
                is EndN -> lastEnd = n
            }

            if(n is CaseCompletedN){
                lastEnd.nexts.add(n)
            }
            else {

                if (currents.containsKey(e.tokenId)) {
                    currents[e.tokenId]!!.nexts.add(n)
                    currents[e.tokenId] = n
                } else {
                    currents.put(e.tokenId, n)
                }
            }
        }

    }

}

//====================================
private abstract class N(val num: Int, timeLDT: LocalDateTime) {
    val time = timeLDT.format(Util.formatter)
    val nexts = mutableListOf<N>()
    abstract fun renderNode(): String
    val sb = StringBuilder()

    //------------------------
    fun renderEdge(): String {
        for(nxt in nexts) {
            sb.append("n_$num -> n_${nxt.num} ;").append("\n")
        }
        return sb.toString()
    }

    //--------------
    fun dt2str(dt: LocalDateTime): String{
        return dt.format(Util.formatter)
    }

}

//====================================
private object Util{
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
}

//===================================
private class None: N(-1, LocalDateTime.now()){
    override fun renderNode(): String { return ""}
}


//===================================
private class CaseCreatedN(num: Int,
                           val type: String,
                           val id: Long,
                           val key: String,
                           val workDataType: String,
                           time: LocalDateTime) : N(num, time) {

    override fun renderNode(): String {
        return "n_$num [style=\"rounded\", label = \"Case Created\\n'$type' | caseId=$id\\nkey=$key\\nworkDataType=$workDataType \\ntime=$time\"];"
    }
}

private class CaseCompletedN(num: Int, time: LocalDateTime) : N(num, time) {
    override fun renderNode(): String {
        return "n_$num [style=\"rounded\", label = \"Case Completed | time=$time\"];"
    }
}

//===================================
private class StartN(num: Int, time: LocalDateTime) : N(num, time) {
    override fun renderNode(): String {
        return "n_$num [shape=\"ellipse\", label = \"Start\"];"
    }
}

private class EndN(num: Int, time: LocalDateTime) : N(num, time) {
    override fun renderNode(): String {
        return "n_$num [shape=\"ellipse\", label = \"End\"];"
    }
}

//===================================
private class TaskN(num: Int, val taskId: Long, val type: String, val workerType: String, time: LocalDateTime) : N(num, time){

    val actions = mutableListOf<TaskActionN>()

    override fun renderNode(): String {
        val sb = StringBuilder()
        var firstItem = true
        for(ta in actions){
            if(!firstItem) sb.append("|")
            else firstItem = false
            sb.append(ta.render())
        }

        return "n_$num [label = \"Task\\n'$type' | { {taskId=$taskId\\nwokerType=$workerType} | ${sb.toString()} }\"];"
    }
}

//===================================
private open class TaskActionN(val name: String,
                               timeLDT: LocalDateTime) {
    val time = timeLDT.format(Util.formatter)
    open fun render(): String {
        return "{$name | time=$time}"
    }

}

//===================================
private open class OwnershipChangeTaskActionN(
        val workerId: String,
        name: String,
        time: LocalDateTime) : TaskActionN(name, time){

    override fun render(): String {
        return "{$name | worker='$workerId' \\ntime=$time}"
    }
}

//===================================
private class OrN(num: Int, val id: String, var guard: String, time: LocalDateTime) : N(num, time) {

    override fun renderNode(): String {
        return "n_$num [label = \"OrGate\\n'$id' | {guard='$guard'\\ntime=$time}\"];"
    }
}

//===================================
private abstract class TokenSplitN(num: Int, val id: String, time: LocalDateTime) : N(num, time) {}

//===================================
private class AndN(num: Int, id: String, time: LocalDateTime) : TokenSplitN(num, id, time) {
    override fun renderNode(): String {
        return "\tn_$num [label = \"AndGate\\n'$id' | time=$time\"];"
    }
}