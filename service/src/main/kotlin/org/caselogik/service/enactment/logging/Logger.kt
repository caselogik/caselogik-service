package org.caselogik.service.enactment.logging

import org.caselogik.api.Event
import org.caselogik.api.ExternalEvent
import org.caselogik.service.definition.entities.*
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.entities.WorkBatchToken
import org.caselogik.service.enactment.entities.WorkItemToken
import org.caselogik.service.enactment.events.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.lang.RuntimeException

/***
 * This component receives all Events that are related to WfCases and stores
 * them in the CaseLogRepo as a CaseLogEntry. It forms a complete history
 * of what has happened to a Case. When the log for a case is requested by
 * the client, this data is read and converted to a graph in the dot format
 */


@Component
class Logger

    @Autowired
    constructor(private val logRepo: LogRepo,
                private val processRepo: ProcessRepo){

    //------------------------------------------
    fun logEvent(evt: Event){

        val cle = newEntry(evt)

        when(evt){
            is CaseEvent -> processEvent(evt, cle)
            is TaskEvent -> processEvent(evt, cle)
            is TimeoutEvent -> processEvent(evt, cle)
            is TokenRoutingEvent -> processEvent(evt, cle)
            // CaseBatchEvent and ScheduledEvent not processed as not related to a Case
        }

    }

    //-----------------------------------------------------------
    private fun processEvent(ce: CaseEvent, cle: LogEntry){
        val case = ce.case
        cle.caseId = ce.caseId
        cle.processId = ce.case.processId

        when(ce) {
            is CaseCreated -> {
                cle.tokenId = case.tokens[0].id
                cle.workDataType = case.originalWorkItem.typeName
                cle.workDataJson = case.originalWorkItem.json
                cle.caseKey = case.key
            }
        }
    }

    //----------------------------------------------------------
    private fun processEvent(te: TaskEvent, cle: LogEntry){
        val task = te.task
        cle.taskId = task.id
        cle.processId = task.processId
        cle.nodeId = task.taskNodeId
        val token = task.token
        when(token){
            is WorkItemToken -> {
                cle.tokenId = token.id
                cle.caseId = token.case.id
            }
            is WorkBatchToken -> cle.batchTokenId = task.token.id
        }

        when(te){
            is TaskCreated -> {
                val tn = processRepo.getProcessNodeByUid(task.taskNodeUid()) as TaskNode

                cle.workerType = when(tn){
                    is HumanTaskNode -> "Human"
                    is SystemTaskNode -> "System"
                    is ExternalPartyTaskNode -> "ExternalParty"
                    else -> throw RuntimeException("Unrecognised TaskNode type")
                }
            }

            is TaskCompleted -> {
                cle.workDataType = te.workDataType
                cle.workDataJson = te.workDataJson
            }
            is TaskClaimed -> {
                cle.workerId = te.by.id
                cle.workerType = te.by.type
            }
            is TaskFailed -> {
                cle.note = te.reason
            }
        }
    }


    //------------------------------------------------------------
    private fun processEvent(te: TimeoutEvent, cle: LogEntry){

    }

    //------------------------------------------------------------
    private fun processEvent(te: ExternalEvent, cle: LogEntry){

    }
    //----------------------------------------------------------------
    private fun processEvent(te: TokenRoutingEvent, cle: LogEntry){

        val token = te.token
        cle.processId = token.processId
        cle.nodeId = te.nodeId
        cle.nodeType = processRepo.getProcessNodeByUid(NodeUid.new(token.processId, te.nodeId)).javaClass.simpleName
        when(token){
            is WorkItemToken -> {
                cle.tokenId = token.id
                cle.caseId = token.case.id
            }
            is WorkBatchToken -> cle.batchTokenId = token.id
        }

        when(te){
            is TokenRouted -> {
                cle.toNodeId = te.toNodeId
                cle.toNodeType = processRepo.getProcessNodeByUid(NodeUid.new(token.processId, te.toNodeId)).javaClass.simpleName
                cle.guard = te.guard
            }
            is TokenSplit -> cle.parentTokenId = te.parentTokenId
            is TokenMerged -> cle.parentTokenId = te.parentTokenId
        }
    }

    //------------------------------------------
    private fun newEntry(evt: Event): LogEntry {
        val cle = LogEntry()
        cle.eventName = evt.javaClass.simpleName
        cle.timestamp = evt.timestamp
        logRepo.save(cle)
        return cle
    }


}