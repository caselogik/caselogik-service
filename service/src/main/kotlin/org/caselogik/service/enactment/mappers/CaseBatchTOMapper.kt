package org.caselogik.service.enactment.mappers

import org.caselogik.service.enactment.entities.CaseBatch
import org.caselogik.service.enactment.entities.CaseBatchStatus
import org.caselogik.api.CaseBatch as CaseBatchTO


object CaseBatchTOMapper {
    fun makeTO(caseBatch: CaseBatch): CaseBatchTO {
        val caseTOs = caseBatch.cases.map { CaseTOMapper.makeTO(it) }

        val to = CaseBatchTO(caseBatch.id,
                caseBatch.status.name,
                caseBatch.processId,
                caseBatch.key,
                caseTOs)

        return to
    }

    //-------------------------------------------
    fun makeStatusEnum(status: String): CaseBatchStatus {
        val s =  when (status) {
            "Active" -> CaseBatchStatus.Active
            "Suspended" -> CaseBatchStatus.Suspended
            "Completed" -> CaseBatchStatus.Completed
            "Cancelled" -> CaseBatchStatus.Cancelled
            else -> throw RuntimeException("Unknown status '$status'")
        }
        return s
    }

}