package org.caselogik.service.enactment.mappers

import org.caselogik.service.enactment.entities.CaseNote

object CaseNoteTOMapper{
    fun makeTO(caseNote: CaseNote): org.caselogik.api.CaseNote {
        val to = org.caselogik.api.CaseNote(
                caseNote.id,
                caseNote.content,
                caseNote.timestamp,
                caseNote.authorId,
                caseNote.parentNoteId
        )

        return to
    }

}