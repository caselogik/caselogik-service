package org.caselogik.service.enactment.mappers

import org.caselogik.service.enactment.entities.CaseNote
import org.caselogik.service.enactment.entities.CaseStatus
import org.caselogik.service.enactment.entities.WfCase
import java.time.format.DateTimeFormatter
import org.caselogik.api.Case as CaseTO
import org.caselogik.api.CaseNote as CaseNoteTO

object CaseTOMapper {
    fun makeTO(case: WfCase): CaseTO {
        val to = CaseTO(case.id,
                case.status.name,
                case.processId,
                case.key,
                case.createdAt.format(DateTimeFormatter.ISO_DATE_TIME),
                case.caseNotes.map {
                    CaseNoteTOMapper.makeTO(it)
                })

        return to
    }

    //-------------------------------------------
    fun makeStatusEnum(status: String): CaseStatus {
        val s =  when (status) {
            "Active" -> CaseStatus.Active
            "Suspended" -> CaseStatus.Suspended
            "Completed" -> CaseStatus.Completed
            "Cancelled" -> CaseStatus.Cancelled
            "Attached" -> CaseStatus.Attached
            else -> throw RuntimeException("Unknown status '$status'")
        }
        return s
    }

}

