package org.caselogik.service.enactment.mappers

import org.caselogik.api.GraphViz
import org.caselogik.api.FlowPath
import org.caselogik.service.definition.entities.*
import org.caselogik.api.Node as NodeTO
import org.caselogik.api.WfProcess as ProcessTO

object ProcessTOMapper {

    //--------------------------------------
    fun makeTO(proc: WfProcess): ProcessTO {
        val to = ProcessTO(proc.id, proc.name)

        to.nodes = proc.nodesMap.values.map {n ->
            NodeTO(
                n.id,
                n.name,
                n.javaClass.simpleName
            )
        }

        to.paths = proc.paths.map { p ->
            val guard = if(p.guard.isBlank()) null else p.guard
            FlowPath(p.from.id, p.to.id, guard)
        }
        return to
    }


    //--------------------------------------
    fun renderFlow(proc: ProcessTO): GraphViz {

        val sb = StringBuilder()
        sb.append("digraph G { rankdir=TB; ranksep=0.5; nodesep=0.5; node [shape=record, fontname=courier, fontsize=10]; edge [fontname=courier, fontsize=10];")

        var endNum = 1
        val nodeSet = HashSet<String>()
        val nodes = mutableListOf<String>()
        val edges = mutableListOf<String>()



        for(path in proc.paths) {

            var from = ""
            var to = ""
            if(path.fromNodeId == "start"){
                nodes.add("start [shape=\"circle\"];")
                from = "start"
            }

            if(path.toNodeId == "end"){
                nodes.add("end_${endNum} [label = \"end\", shape=\"circle\"];")
                to = "end_${endNum}"
                endNum++
            }

            if(path.fromNodeId != "start" && !nodeSet.contains(path.fromNodeId)){
                nodes.add("${path.fromNodeId};")
                nodeSet.add(path.fromNodeId)
            }

            if(path.toNodeId!="end" && !nodeSet.contains(path.toNodeId)){
                nodes.add("${path.toNodeId};")
                nodeSet.add(path.toNodeId)
            }

            from = if(from.isEmpty()) path.fromNodeId else from
            to = if(to.isEmpty()) path.toNodeId else to

            val label = if(path.guard!=null) " [ label = \"[${path.guard}]\" ]" else ""

            edges.add("$from -> $to $label; ")
        }

        nodes.forEach{sb.append(it)}
        edges.forEach{sb.append(it)}

        sb.append(" }")
        return GraphViz(sb.toString(), "dot")

    }



}