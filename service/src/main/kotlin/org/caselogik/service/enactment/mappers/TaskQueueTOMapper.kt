package org.caselogik.service.enactment.mappers

import org.caselogik.api.TaskQueue
import org.caselogik.service.definition.entities.ExternalPartyTaskNode
import org.caselogik.service.definition.entities.HumanTaskNode
import org.caselogik.service.definition.entities.SystemTaskNode
import org.caselogik.service.definition.entities.TaskNode

object TaskQueueTOMapper {

    fun makeTO(taskNode: TaskNode, taskCount: Long): TaskQueue {

        val actorType = when(taskNode) {
            is SystemTaskNode -> "System"
            is HumanTaskNode -> "Human"
            is ExternalPartyTaskNode -> "ExternalParty"
            else -> throw RuntimeException("Unknown task node type")
        }
        return TaskQueue(taskNode.id, taskNode.name, taskNode.process.id, actorType, taskCount)

    }

}