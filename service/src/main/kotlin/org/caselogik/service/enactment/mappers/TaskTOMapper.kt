package org.caselogik.service.enactment.mappers

import org.caselogik.service.definition.entities.NodeUid
import org.caselogik.service.definition.entities.TaskNode
import org.caselogik.service.enactment.entities.Task
import org.caselogik.service.enactment.entities.TaskStatus
import org.caselogik.service.enactment.repositories.WorkItemMapper
import java.time.format.DateTimeFormatter
import org.caselogik.api.ExecutionDetails as ExecutionDetailsTO
import org.caselogik.api.Task as TaskTO


object TaskTOMapper {

    lateinit var serviceUrl: String
    lateinit var taskNodesInUniverse: Map<NodeUid, TaskNode>

    //-------------------------------------------
    fun makeTO(task: Task): TaskTO {

        val taskNode = taskNodesInUniverse[task.taskNodeUid()]
        if(taskNode == null) throw Exception("TaskNode with uid='${task.taskNodeUid()}' not found")


        //Add HATEOS links
        val links = HashMap<String, String>()
        val selfLink = "$serviceUrl/tasks/${task.id}"
        links.put("self",selfLink)
        task.getAvailableActionNames().forEach{ links.put("self.$it","$selfLink/$it")}

        val execDetails = task.execDetails?.run {
            ExecutionDetailsTO(
                    false,
                    isWorkDataUpdated,
                    lastFailureReason,
                    lastAttemptAt?.format(DateTimeFormatter.ISO_DATE_TIME),
                    lastProgressUpdateAt?.format(DateTimeFormatter.ISO_DATE_TIME))
        }

        //put task id in execution url
        val workData = task.workData()

        val execUrl = "${taskNode.executionUrl}?taskId=${task.id}&workDataKey=${workData.key}"

        val to = TaskTO(
                task.id,
                task.processId,
                task.taskNodeId,
                task.workDataType(),
                WorkItemMapper.toJson(task.workData()),
                WorkerTOMapper.fromWorker(task.worker),
                WorkerTOMapper.fromWorker(task.referrer),
                task.status.name,
                task.isSystemTask(),
                task.isBatchTask(),
                task.isReferred(),
                task.priority,
                task.createdAt.format(DateTimeFormatter.ISO_DATE_TIME),
                execUrl,
                execDetails,
                links
        )
        return to
    }

    //-------------------------------------------
    fun makeStatusEnum(status: String): TaskStatus{
        val s =  when (status) {
            "Active" -> TaskStatus.Active
            "Inactive" -> TaskStatus.Inactive
            "Suspended" -> TaskStatus.Suspended
            "Completed" -> TaskStatus.Completed
            "Cancelled" -> TaskStatus.Cancelled
            else -> throw RuntimeException("Unknown status '$status'")
        }
        return s
    }


}
