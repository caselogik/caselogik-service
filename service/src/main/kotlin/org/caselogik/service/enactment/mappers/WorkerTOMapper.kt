package org.caselogik.service.enactment.mappers

import org.caselogik.service.enactment.entities.Worker
import org.caselogik.service.enactment.entities.WorkerType
import org.caselogik.api.Worker as WorkerTO

object WorkerTOMapper{

    fun fromWorker(worker: Worker): WorkerTO {
        return WorkerTO( worker.id, worker.type.name,null)
    }

    //---------------------------------------
    fun toWorker(to: WorkerTO): Worker {

        val typ = when(to.type) {
            "None" -> WorkerType.None
            "System" -> WorkerType.System
            "Human" -> WorkerType.Human
            "Group" -> WorkerType.Group
            "ExternalParty" -> WorkerType.ExternalParty
            else -> throw RuntimeException("Worker type '${to.type}' is not valid")
        }

        val worker = Worker()
        worker.id = to.id
        worker.type = typ
        return worker
    }
}