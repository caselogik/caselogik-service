package org.caselogik.service.enactment.repositories

import org.caselogik.service.CLRuntimeException
import org.caselogik.service.enactment.entities.CaseBatch
import org.springframework.data.repository.CrudRepository

interface CaseBatchRepo : CrudRepository<CaseBatch, Long> {


    //---------------------------------------
    @JvmDefault
    fun getById(id: Long): CaseBatch {

        var caseBatchOpt = findById(id)
        if(!caseBatchOpt.isPresent) throw CLRuntimeException(CLRuntimeException.CASE_BATCH_NOT_FOUND, "Couldn't find CaseBatch with id=%s in repository", id.toString())
        return caseBatchOpt.get()
    }

}
