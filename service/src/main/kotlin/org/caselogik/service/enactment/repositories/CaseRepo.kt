package org.caselogik.service.enactment.repositories

import org.caselogik.service.CLRuntimeException
import org.caselogik.service.enactment.entities.WfCase
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface CaseRepo : CrudRepository<WfCase, Long> {

    //-------------------------------------------------------------------
    @Query("SELECT c FROM WfCase c WHERE c.processId = :processId")
    fun getCasesForProcess(@Param("processId") processId: String): Collection<WfCase>

    //-------------------------------------------------------------------
    @Query("""
        SELECT c FROM WfCase c
        WHERE c.status != org.caselogik.service.enactment.entities.CaseStatus.Completed
        AND   c.status != org.caselogik.service.enactment.entities.CaseStatus.Cancelled
        AND   c.status != org.caselogik.service.enactment.entities.CaseStatus.Attached
        """)
    fun getAllLiveCases(): Collection<WfCase>

    //--------------------------------------------------------------------
    @Query("""
        SELECT c FROM WfCase c
        WHERE c.processId = :processId
        AND c.status != org.caselogik.service.enactment.entities.CaseStatus.Completed
        AND   c.status != org.caselogik.service.enactment.entities.CaseStatus.Cancelled
        AND   c.status != org.caselogik.service.enactment.entities.CaseStatus.Attached
        """)
    fun getLiveCasesForProcess(processId: String): Collection<WfCase>

    //----------------------------------------------------
    @JvmDefault
    fun getById(id: Long): WfCase {

        var caseOpt = findById(id)
        if(!caseOpt.isPresent) throw CLRuntimeException(CLRuntimeException.CASE_NOT_FOUND, "Couldn't find Case with id=%s in repository", id.toString())
        return caseOpt.get()
    }

}