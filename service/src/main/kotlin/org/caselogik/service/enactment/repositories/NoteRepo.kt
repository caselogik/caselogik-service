package org.caselogik.service.enactment.repositories

import org.caselogik.service.enactment.entities.CaseNote
import org.springframework.data.repository.CrudRepository


interface NoteRepo : CrudRepository<CaseNote, Long>