package org.caselogik.service.enactment.repositories

import org.caselogik.service.enactment.entities.Task
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository


interface TaskRepo : CrudRepository<Task, Long> {

    //------------------------------
    @JvmDefault
    fun getById(id: Long): Task {
        val taskOpt = findById(id)
        if(!taskOpt.isPresent) throw RuntimeException("Couldn't find Task with id "+id)
        return taskOpt.get()
    }


    //-----------------------------------------------
    @Query("""
        SELECT t FROM Task t
        WHERE t.status!=org.caselogik.service.enactment.entities.TaskStatus.Completed
        AND t.status!=org.caselogik.service.enactment.entities.TaskStatus.Cancelled
        """)
    fun getAllLiveTasks(): List<Task>

    //---------------------------------------------------------------------
    // Active tasks for given task node ordered by priority, then by create time
    @Query("""
        SELECT t FROM Task t
        WHERE t.token.processId = :processId
        AND t.token.currentNodeId = :taskNodeId
        AND t.worker.type = org.caselogik.service.enactment.entities.WorkerType.None
        AND t.status!=org.caselogik.service.enactment.entities.TaskStatus.Completed
        AND t.status!=org.caselogik.service.enactment.entities.TaskStatus.Cancelled
        ORDER BY t.priority ASC, t.createdAt ASC
        """)
    fun getTasksInQueue(processId: String, taskNodeId: String): List<Task>

    //---------------------------------------------------------------------
    // Active tasks for given task node ordered by priority, then by create time
    @Query("""
        SELECT COUNT(t) FROM Task t
        WHERE t.token.processId = :processId
        AND t.token.currentNodeId = :taskNodeId
        AND t.worker.type = org.caselogik.service.enactment.entities.WorkerType.None
        AND t.status!=org.caselogik.service.enactment.entities.TaskStatus.Completed
        AND t.status!=org.caselogik.service.enactment.entities.TaskStatus.Cancelled
        """)
    fun countTasksInQueue(processId: String, taskNodeId: String): Long

    //-----------------------------------------------
    // Active tasks for given worker id
    @Query("""
        SELECT t FROM Task t
        WHERE t.worker.id = :workerId
        AND t.status!=org.caselogik.service.enactment.entities.TaskStatus.Completed
        AND t.status!=org.caselogik.service.enactment.entities.TaskStatus.Cancelled
        """)
    fun getTasksInWorklist(workerId: String): List<Task>

}
