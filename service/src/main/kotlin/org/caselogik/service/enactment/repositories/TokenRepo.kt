package org.caselogik.service.enactment.repositories

import org.caselogik.service.CLRuntimeException
import org.caselogik.service.definition.entities.WfNode
import org.caselogik.service.enactment.entities.*
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface TokenRepo : CrudRepository<Token, Long> {

    //-------------------------------------------------
    @JvmDefault
    fun getById(id: Long): Token {
        val tokenOpt = findById(id)
        if(!tokenOpt.isPresent) throw CLRuntimeException(CLRuntimeException.TOKEN_NOT_FOUND, "Couldn't find Token with id=%s in repository", id.toString())
        return tokenOpt.get();
    }

    //----------------------------------------------------
    @JvmDefault
    fun getActiveTokensAtNode(node: WfNode) : Collection<Token>{
        return getActiveTokensAtNode(node.process.id, node.id)
    }

    //------------------------------------------------------
    @Query("SELECT t FROM Token t WHERE t.processId = :processId AND t.currentNodeId = :nodeId AND t.status = org.caselogik.service.enactment.entities.TokenStatus.Active")
    fun getActiveTokensAtNode(processId: String, nodeId: String): Collection<Token>

}
