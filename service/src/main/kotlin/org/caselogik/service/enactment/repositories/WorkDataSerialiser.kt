package org.caselogik.service.enactment.repositories

import org.caselogik.service.enactment.entities.WorkDataE
import javax.persistence.PostLoad
import javax.persistence.PrePersist

class WorkDataSerialiser {

    //-------------------------------------------
    @PrePersist
    fun serialiseWorkItem(workData: WorkDataE){
        workData.json = WorkItemMapper.toJson(workData.dataObject)
        workData.typeName = workData.dataObject.javaClass.canonicalName
    }

    //-------------------------------------------
    @PostLoad
    fun deserialiseWorkItem(workData: WorkDataE){
        workData.dataObject = WorkItemMapper.toWorkData(workData.json, workData.typeName)
    }

}
