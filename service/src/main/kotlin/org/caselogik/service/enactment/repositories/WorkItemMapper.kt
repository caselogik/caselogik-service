package org.caselogik.service.enactment.repositories

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.caselogik.service.CLRuntimeException
import org.caselogik.api.WorkData

object WorkItemMapper{


    private val workDataTypeMap = HashMap<String, Class<out WorkData>>()

    //--------------------------------------
    fun putWorkItemType(clazz: Class<out WorkData>) {
        workDataTypeMap.put(clazz.canonicalName, clazz)
    }

    //-------------------------------------------------
    val mapper = jacksonObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

    //--------------------------------------------
    fun toJson(workData: Any): String {
        return mapper.writeValueAsString(workData)
    }

    //------------------------------------------------------------
    fun toWorkData(json: String, wdClassName: String): WorkData {
        val clazz = workDataTypeMap[wdClassName]
        if(clazz == null) throw CLRuntimeException(CLRuntimeException.WD_TYPE_NOT_FOUND, "Work data class '%s' not found.", wdClassName)
        return mapper.readValue(json, clazz) as WorkData
    }

    //------------------------------------------------------------
    fun toWorkData(json: String, clazz: Class<out WorkData>): WorkData {
        try {
            return mapper.readValue(json, clazz) as WorkData
        }
        catch(e: Exception){
            throw CLRuntimeException(CLRuntimeException.WD_LOAD_FAILED,
                    "Failed to load work data of type '%s' from from JSON -\n%s",
                    clazz.simpleName, json)
        }
    }

}