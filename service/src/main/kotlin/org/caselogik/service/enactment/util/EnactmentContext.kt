package org.caselogik.service.enactment.util

import org.caselogik.service.config.CLConfiguration
import org.caselogik.service.enactment.controllers.ScheduleCtrl
import org.caselogik.service.enactment.events.EventSender
import org.caselogik.service.enactment.gateways.ExecutionGateway
import org.caselogik.service.enactment.repositories.TaskRepo
import org.caselogik.service.enactment.repositories.TokenRepo

class EnactmentContext

    constructor(
            val eventSender: EventSender,
            val tokenRepo: TokenRepo,
            val taskRepo: TaskRepo,
            val executionGW: ExecutionGateway,
            val config: CLConfiguration,
            val scheduleCtrl: ScheduleCtrl
    ){

}