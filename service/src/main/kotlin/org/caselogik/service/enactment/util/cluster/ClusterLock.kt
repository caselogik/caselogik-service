package org.caselogik.service.enactment.util.cluster

import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Transient

@Entity
class ClusterLock {

    @Id
    var id: Long = 1L

    var lastClaim: Long  = -1L

    //-------------------------------------------------
    fun setLastClaimTimestamp() {
        lastClaim = ZonedDateTime.now().toEpochSecond()
    }

    //------------------------------------------
    @Transient
    private lateinit var lastClaimedAt: ZonedDateTime
    private fun initLastClaimedAt(){
        val i = Instant.ofEpochSecond(lastClaim);
        lastClaimedAt = ZonedDateTime.ofInstant(i, ZoneId.systemDefault());
    }

    //---------------------------------------------------------
    fun hasLastClaimEexpired(expiryDuration: Duration): Boolean{
        if(!::lastClaimedAt.isInitialized)initLastClaimedAt()
        val now = ZonedDateTime.now()
        return now.isAfter(lastClaimedAt.plus(expiryDuration))
    }
}

