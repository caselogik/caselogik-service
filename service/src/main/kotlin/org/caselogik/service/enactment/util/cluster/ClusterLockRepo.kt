package org.caselogik.service.enactment.util.cluster

import org.caselogik.service.enactment.util.cluster.ClusterLock
import org.springframework.data.jpa.repository.Lock
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import javax.persistence.LockModeType

interface ClusterLockRepo  : CrudRepository<ClusterLock, Long> {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("from ClusterLock cl where cl.id = 1")
    fun getLock(): ClusterLock?
}