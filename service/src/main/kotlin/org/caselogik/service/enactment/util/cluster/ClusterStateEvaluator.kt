package org.caselogik.service.enactment.util.cluster

interface ClusterStateEvaluator {

    val isSingleton: Boolean
    fun evaluateClusterState()
}