package org.caselogik.service.enactment.util.cluster

import org.caselogik.service.config.CLConfiguration
import org.caselogik.service.enactment.util.cluster.ClusterLock
import org.caselogik.service.enactment.util.cluster.ClusterLockRepo
import org.caselogik.service.enactment.util.cluster.ClusterStateEvaluator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import javax.persistence.LockTimeoutException

@Component
class DbBasedClusterStateEvaluator

    @Autowired
    constructor(private val clusterLockRepo: ClusterLockRepo,
                private val config: CLConfiguration) : ClusterStateEvaluator {

    private val claimExpiryTime = config.getClockTickInterval().multipliedBy(2)

    override var isSingleton = false

    //-----------------------------------
    @Transactional
    override fun evaluateClusterState(){
        try {
            var cl = clusterLockRepo.getLock()
            if(cl == null){
                cl = ClusterLock()
                cl.setLastClaimTimestamp()
                clusterLockRepo.save(cl)
                isSingleton = true
            }
            else {
                isSingleton = cl.hasLastClaimEexpired(claimExpiryTime)
            }
        }
        catch(ltex: LockTimeoutException){
            isSingleton = false
        }
        catch(ex: Exception){
            ex.printStackTrace()
        }
    }

}