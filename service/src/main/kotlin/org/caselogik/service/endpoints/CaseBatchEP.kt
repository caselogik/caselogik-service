package org.caselogik.service.endpoints

import org.caselogik.api.commands.wfcase.CaseBatchCmdResponse
import org.caselogik.api.commands.wfcase.CreateCaseBatch
import org.caselogik.service.enactment.controllers.CaseBatchCtrl
import org.caselogik.service.enactment.repositories.CaseBatchRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1")
class CaseBatchEP
    @Autowired
    constructor(private val caseBatchRepo: CaseBatchRepo,
                private val caseCtrl: CaseBatchCtrl) {

    //-----------------------------------
    @PutMapping("/case-batches")
    @ResponseBody
    @Transactional
    fun createCaseBatch(@RequestBody cmd: CreateCaseBatch): ResponseEntity<CaseBatchCmdResponse>{
        val resp = caseCtrl.onCommand(cmd)
        return ResponseEntity.status(HttpStatus.OK).body(resp)
    }

}