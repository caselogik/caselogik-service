package org.caselogik.service.endpoints

import org.caselogik.api.CaseLog
import org.caselogik.api.commands.wfcase.CaseCmdResponse
import org.caselogik.api.commands.wfcase.CreateCase
import org.caselogik.service.enactment.controllers.CaseCtrl
import org.caselogik.service.enactment.entities.WfCase
import org.caselogik.service.enactment.logging.CaseLogVisualiser
import org.caselogik.service.enactment.mappers.CaseTOMapper
import org.caselogik.service.enactment.repositories.CaseRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.util.*
import org.caselogik.api.Case as CaseTO

@RestController
@RequestMapping("/api/v1")
class CaseEP
    @Autowired
    constructor(private val caseRepo: CaseRepo,
                private val caseCtrl: CaseCtrl,
                private val caseLogVisualiser: CaseLogVisualiser) {

    //---------------------------------------------------
    @GetMapping("/cases")
    @ResponseBody
    fun getAllCases(@RequestParam("live") live: Optional<Boolean>,
                    @RequestParam("processId") processId: Optional<String>): ResponseEntity<Collection<CaseTO>> {

        val cases = if(processId.isPresent){
            if(live.isPresent && live.get()) caseRepo.getLiveCasesForProcess(processId.get())
            else caseRepo.getCasesForProcess(processId.get())
        }
        else {
            if(live.isPresent && live.get()) caseRepo.getAllLiveCases()
            else caseRepo.findAll()
        }
        return makeResponse(cases)
    }

    //---------------------------------------------------
    @GetMapping("/cases/{id}/log")
    @ResponseBody
    fun getCaseLog(@PathVariable("id") caseId: Long,
                   @RequestParam("visual") visual: Optional<Boolean>): ResponseEntity<CaseLog> {

        val cl = CaseLog(caseId)
        if(visual.orElse(false)){
            cl.graphViz = caseLogVisualiser.render(caseId)
        }
        return ResponseEntity.status(HttpStatus.OK).body(cl)
    }

    //-----------------------------------
    @PutMapping("/cases")
    @ResponseBody
    @Transactional
    fun createCase(@RequestBody cmd: CreateCase): ResponseEntity<CaseCmdResponse>{
        val resp = caseCtrl.onCommand(cmd)
        return ResponseEntity.status(HttpStatus.OK).body(resp)
    }

    //------------------------------------------------------------------------
    private fun makeResponse(cases: Iterable<WfCase>): ResponseEntity<Collection<CaseTO>>{
        val caseTOs = cases.map { CaseTOMapper.makeTO(it) }
        return ResponseEntity.status(HttpStatus.OK).body(caseTOs)
    }

}