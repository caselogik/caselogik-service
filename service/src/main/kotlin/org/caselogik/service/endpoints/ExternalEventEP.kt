package org.caselogik.service.endpoints

import org.caselogik.api.commands.ExternalEventResponse
import org.caselogik.api.commands.FireExternalEvent
import org.caselogik.service.enactment.controllers.ProcessCtrl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api/v1")
class ExternalEventEP
    @Autowired
    constructor(private val processCtrl: ProcessCtrl) {


    //---------------------------------------------------
    @PostMapping("/events")
    fun fireExternalEvent(@RequestBody cmd: FireExternalEvent): ResponseEntity<ExternalEventResponse> {

        val ok = processCtrl.onCommand(cmd)
        val response = ExternalEventResponse(
                if (ok) ExternalEventResponse.Code.OK
                else ExternalEventResponse.Code.DuplicateEvent
        )
        return ResponseEntity.status(HttpStatus.OK).body(response)
    }

}