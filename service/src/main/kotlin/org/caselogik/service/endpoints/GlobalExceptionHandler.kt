package org.caselogik.service.endpoints

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.servlet.http.HttpServletRequest

@ControllerAdvice
class GlobalExceptionHandler {

    @ExceptionHandler(value = [(Exception::class)])
    fun defaultErrorHandler(req: HttpServletRequest, e: Exception): ResponseEntity<out Any> {

        e.printStackTrace()
        return ResponseEntity.status(500).body(e.message)
    }

}