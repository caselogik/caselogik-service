package org.caselogik.service.endpoints

import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.controllers.ProcessCtrl
import org.caselogik.service.enactment.mappers.ProcessTOMapper
import org.caselogik.service.enactment.repositories.TaskRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import org.caselogik.api.WfProcess as ProcessTO


@RestController
@RequestMapping("/api/v1")
class ProcessEP
    @Autowired
    constructor(private val processRepo: ProcessRepo) {

    //---------------------------------------------------
    @GetMapping("/processes")
    @ResponseBody
    fun getProcesses(): ResponseEntity<List<ProcessTO>> {
        val procs = processRepo.getProcesses()
        val procTOs = procs.map { ProcessTO(it.id, it.name) }.toList()
        return ResponseEntity.status(HttpStatus.OK).body(procTOs)
    }

    //---------------------------------------------------
    @GetMapping("/processes/{processId}")
    @ResponseBody
    fun getProcess(@PathVariable processId: String,
                   @RequestParam("visual") visual: Optional<Boolean>): ResponseEntity<ProcessTO> {
        val proc = processRepo.getProcess(processId)
        val procTO = ProcessTOMapper.makeTO(proc)
        if(visual.orElse(false)){
            procTO.graphViz = ProcessTOMapper.renderFlow(procTO)
        }
        return ResponseEntity.status(HttpStatus.OK).body(procTO)
    }

}