package org.caselogik.service.endpoints

import org.caselogik.api.TaskQueue
import org.caselogik.api.commands.task.ClaimTaskInQueue
import org.caselogik.api.commands.task.TaskCmdResponse
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.controllers.TaskCtrl
import org.caselogik.service.enactment.entities.Task
import org.caselogik.service.enactment.mappers.TaskQueueTOMapper
import org.caselogik.service.enactment.mappers.TaskTOMapper
import org.caselogik.service.enactment.repositories.TaskRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.caselogik.api.Task as TaskTO

@RestController
@RequestMapping("/api/v1")
class TaskDistributionEP
    @Autowired
    constructor(private val taskRepo: TaskRepo,
                private val processRepo: ProcessRepo,
                private val taskCtrl: TaskCtrl) {


    //---------------------------------------------------
    @GetMapping("/task-queues/{processId}/{taskNodeId}/tasks")
    @ResponseBody
    fun getTasksInQueue(@PathVariable processId: String,
                        @PathVariable taskNodeId: String): ResponseEntity<List<TaskTO>>{

        val tasks = taskRepo.getTasksInQueue(processId, taskNodeId)
        return makeResponse(tasks)
    }


    //---------------------------------------------------
    @GetMapping("/task-queues/{processId}")
    @ResponseBody
    fun getTaskQueues(@PathVariable processId: String): ResponseEntity<List<TaskQueue>> {

        val result = mutableListOf<TaskQueue>()
        val taskNodes = processRepo.getProcess(processId).getTaskNodes()
        for(taskNode in taskNodes){
            val taskCount = taskRepo.countTasksInQueue(processId, taskNode.id)
            result.add(TaskQueueTOMapper.makeTO(taskNode, taskCount))
        }
        return ResponseEntity.status(HttpStatus.OK).body(result)
    }

    //--------------------------------------------------
    @PutMapping("/task-queues/{processId}/{taskNodeId}/claim")
    @ResponseBody
    fun claimTaskInQueue(@PathVariable processId: String,
                             @PathVariable taskNodeId: String,
                             @RequestBody cmd: ClaimTaskInQueue): TaskCmdResponse {
        
        cmd.processId = processId
        cmd.taskNodeId = taskNodeId
        return taskCtrl.onCommand(cmd)
    }

    //---------------------------------------------------
    @GetMapping("/worklists/{workerId}/tasks")
    @ResponseBody
    fun getTasksInWorklist(@PathVariable workerId: String): ResponseEntity<List<TaskTO>>{

        val tasks = taskRepo.getTasksInWorklist(workerId)
        return makeResponse(tasks)
    }

    //------------------------------------------------------------------------
    private fun makeResponse(tasks: Iterable<Task>): ResponseEntity<List<TaskTO>>{
        val taskTOs = tasks.map{ TaskTOMapper.makeTO(it) }
        return ResponseEntity.status(HttpStatus.OK).body(taskTOs)
    }

}