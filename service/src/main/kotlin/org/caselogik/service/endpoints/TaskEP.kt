package org.caselogik.service.endpoints

import org.caselogik.api.commands.task.*
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.controllers.TaskCtrl
import org.caselogik.service.enactment.entities.Task
import org.caselogik.service.enactment.mappers.TaskTOMapper
import org.caselogik.service.enactment.repositories.TaskRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import org.caselogik.api.Task as TaskTO

@RestController
@RequestMapping("/api/v1")
class TaskEP
    @Autowired
    constructor(private val taskRepo: TaskRepo,
                private val taskCtrl: TaskCtrl) {


    //---------------------------------------------------
    @GetMapping("/tasks/{taskId}")
    @ResponseBody
    fun getTask(@PathVariable taskId: Long): ResponseEntity<TaskTO>{
        val task = taskRepo.getById(taskId)
        return makeResponse(task)
    }

    //---------------------------------------------------
    @GetMapping("/tasks")
    @ResponseBody
    fun getAllTasks(@RequestParam live: Optional<Boolean>): ResponseEntity<List<TaskTO>>{
        val tasks = if(live.isPresent && live.get())taskRepo.getAllLiveTasks()
                    else taskRepo.findAll()
        return makeResponse(tasks)
    }

    //--------------------------------------------------
    @PutMapping("/tasks/{taskId}/allocate")
    @ResponseBody
    fun allocateTask(@PathVariable taskId: Long, @RequestBody cmd: AllocateTask): TaskCmdResponse {
        cmd.taskId = taskId
        return taskCtrl.onCommand(cmd)
    }

    //--------------------------------------------------
    @PutMapping("/tasks/{taskId}/change-priority")
    @ResponseBody
    fun changeTaskPriority(@PathVariable taskId: Long, @RequestBody cmd: ChangeTaskPriority): TaskCmdResponse {
        cmd.taskId = taskId
        return taskCtrl.onCommand(cmd)
    }

    //--------------------------------------------------
    @PutMapping("/tasks/{taskId}/deallocate")
    @ResponseBody
    fun deallocateTask(@PathVariable taskId: Long): TaskCmdResponse {
        val cmd = DeallocateTask()
        cmd.taskId = taskId
        return taskCtrl.onCommand(cmd)
    }

    //--------------------------------------------------
    @PutMapping("/tasks/{taskId}/activate")
    @ResponseBody
    fun activateTask(@PathVariable taskId: Long): TaskCmdResponse {
        val cmd = ActivateTask()
        cmd.taskId = taskId
        return taskCtrl.onCommand(cmd)
    }

    //--------------------------------------------------
    @PutMapping("/tasks/{taskId}/retry")
    @ResponseBody
    fun retryFailedTask(@PathVariable taskId: Long): TaskCmdResponse {
        val cmd = RetryFailedTask()
        cmd.taskId = taskId
        return taskCtrl.onCommand(cmd)
    }

    //--------------------------------------------------
    @PutMapping("/tasks/{taskId}/record-completed")
    @ResponseBody
    fun recordTaskCompleted(@PathVariable taskId: Long, @RequestBody cmd: RecordTaskCompleted): TaskCmdResponse {
        cmd.taskId = taskId
        return taskCtrl.onCommand(cmd)
    }

    //--------------------------------------------------
    @PutMapping("/tasks/{taskId}/record-referral-completed")
    @ResponseBody
    fun recordTaskReferralCompleted(@PathVariable taskId: Long, @RequestBody cmd: RecordTaskReferralCompleted): TaskCmdResponse {
        cmd.taskId = taskId
        return taskCtrl.onCommand(cmd)
    }

    //--------------------------------------------------
    @PutMapping("/tasks/{taskId}/record-failed")
    @ResponseBody
    fun recordTaskFailed(@PathVariable taskId: Long, @RequestBody cmd: RecordTaskFailed): TaskCmdResponse {
        cmd.taskId = taskId
        return taskCtrl.onCommand(cmd)
    }

    //--------------------------------------------------
    @PutMapping("/tasks/{taskId}/record-progress")
    @ResponseBody
    fun recordTaskProgress(@PathVariable taskId: Long, @RequestBody cmd: RecordTaskProgress): TaskCmdResponse {
        cmd.taskId = taskId
        return taskCtrl.onCommand(cmd)
    }

    //------------------------------------------------------------------------
    private fun makeResponse(tasks: Iterable<Task>): ResponseEntity<List<TaskTO>>{
        val taskTOs = tasks.map{ TaskTOMapper.makeTO(it) }
        return ResponseEntity.status(HttpStatus.OK).body(taskTOs)
    }

    //------------------------------------------------------------------------
    private fun makeResponse(task: Task): ResponseEntity<TaskTO>{
        val taskTO = TaskTOMapper.makeTO(task)
        return ResponseEntity.status(HttpStatus.OK).body(taskTO)
    }
}