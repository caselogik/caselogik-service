package integration

import groovy.util.GroovyTestCase.assertEquals
import org.caselogik.service.definition.loader.ProcessLoader
import org.caselogik.service.definition.repositories.ProcessRepo
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import utils.BaseTest


class LoadAllDefinitions : BaseTest(){

    @Autowired
    lateinit var procRepo: ProcessRepo

    @Autowired
    lateinit var processLoader: ProcessLoader

    @Test
    fun loadAllDefinitions() {

        processLoader.unloadAllProcesses()
        processLoader.loadAllDefinitions()


        val proc = procRepo.getProcess("simple_2")

        assertEquals(8, proc.nodesMap.values.size)
        assertEquals(9, proc.paths.size)
    }
}