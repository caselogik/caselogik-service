package integration

import org.caselogik.service.enactment.controllers.ScheduleCtrl
import org.caselogik.service.enactment.events.WaitTimedOut
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import utils.BaseTest
import java.time.ZonedDateTime


class ScheduleEvent: BaseTest(){

    @Autowired
    lateinit var scheduleCtrl: ScheduleCtrl

    @Test
    fun schedule1() {

        val secs = 2L
        scheduleCtrl.scheduleAfterSecs(WaitTimedOut::class, "123", secs)
        pause(3)
    }

    @Test
    fun schedule2() {

        scheduleCtrl.scheduleAt(WaitTimedOut::class, "123", ZonedDateTime.now().plusSeconds(2))
        pause(3)
    }

    //@Test
    fun deSchedule() {
    }
}