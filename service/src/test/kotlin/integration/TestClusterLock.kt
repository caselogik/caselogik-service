package integration


import junit.framework.TestCase.assertEquals
import org.caselogik.service.enactment.util.cluster.DbBasedClusterStateEvaluator
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import utils.BaseTest

class TestClusterLock : BaseTest(){

    @Autowired
    private lateinit var clusterStateEvaluator: DbBasedClusterStateEvaluator

    @Test
    fun test(){

        val HEARTBEAT_SECS:Long = 3
        clusterStateEvaluator.isSingleton = false //init

        //Trying to get the lock the first time should succeed
        clusterStateEvaluator.evaluateClusterState()
        assertEquals(true, clusterStateEvaluator.isSingleton)

        //Trying to acquire lock immediately should fail
        clusterStateEvaluator.isSingleton = false //reset
        clusterStateEvaluator.evaluateClusterState()
        assertEquals(false, clusterStateEvaluator.isSingleton)

        //Trying to acquire lock within heartbeat*2 secs should succeed
        clusterStateEvaluator.isSingleton = false //reset
        Thread.sleep(HEARTBEAT_SECS * 2 * 1000)
        clusterStateEvaluator.evaluateClusterState()
        assertEquals(true, clusterStateEvaluator.isSingleton)


    }

}