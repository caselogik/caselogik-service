package integration


import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jms.annotation.JmsListener
import org.springframework.jms.core.JmsTemplate
import org.springframework.transaction.annotation.Transactional
import utils.BaseTest
import javax.jms.DeliveryMode


//====================================================
class BrokerTest : BaseTest(){

    @Autowired
    lateinit var jmsTemplate: JmsTemplate

    var message: String? = null

    //------------------------------------
    @JmsListener(destination = "test")
    @Transactional
    fun receive(msg: String) {
        println("MESSAGE - $msg")
        message = msg
    }

    //--------------------------------------
    @Test
    fun test(){

        jmsTemplate.deliveryMode = DeliveryMode.NON_PERSISTENT
        jmsTemplate.convertAndSend("test", "hello")

        Thread.sleep(2000)
        Assert.assertTrue(message == "hello")
    }



}
