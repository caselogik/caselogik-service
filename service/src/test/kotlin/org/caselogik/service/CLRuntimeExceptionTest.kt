package org.caselogik.service


import junit.framework.TestCase.assertEquals
import org.junit.Test

class CLRuntimeExceptionTest {

    @Test
    fun testMessage(){

        val e = CLRuntimeException(CLRuntimeException.TOKEN_NOT_FOUND, "Token has id %s", "1")
        assertEquals("Token has id 1", e.message)
    }

}