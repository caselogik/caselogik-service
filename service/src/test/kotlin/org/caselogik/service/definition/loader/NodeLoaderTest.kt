package org.caselogik.service.definition.loader

import groovy.lang.Closure
import org.caselogik.api.WorkBatch
import org.caselogik.api.WorkItem
import org.caselogik.definition.dsl.model.*
import org.caselogik.service.definition.entities.*
import org.junit.Assert.assertEquals
import org.junit.Test

class NodeLoaderTest {

    private val nodeLoader = NodeLoader()



    @Test
    fun load() {

        val wi = TestWI()

        //Start
        val start = StartDefn()
        start.workItemType = wi.javaClass
        val ctx = createContext()
        val node1 = nodeLoader.load(start, ctx)
        assertEquals(Start::class.java, node1.javaClass)

        //End
        val end = EndDefn()
        end.workItemType = wi.javaClass
        val node2 = nodeLoader.load(end, ctx)
        assertEquals(End::class.java, node2.javaClass)

        //OrGate
        val or = OrGateDefn()
        or.id = "or"
        or.rule = RuleDefn(Closure.IDENTITY)
        or.workItemType = wi.javaClass
        val node3 = nodeLoader.load(or, ctx)
        assertEquals(OrGate::class.java, node3.javaClass)

        //AndGate
        val and = AndGateDefn()
        and.id = "and"
        and.rule = RuleDefn(Closure.IDENTITY)
        and.workItemType = wi.javaClass
        val node4 = nodeLoader.load(and, ctx)
        assertEquals(AndGate::class.java, node4.javaClass)

        //Sync
        val sync = SyncDefn()
        sync.id = "sync"
        sync.workItemType = wi.javaClass
        val node5 = nodeLoader.load(sync, ctx)
        assertEquals(Sync::class.java, node5.javaClass)

        //Wait
        val wait = WaitDefn()
        wait.id = "wait"
        wait.workItemType = wi.javaClass
        val node6 = nodeLoader.load(wait, ctx)
        assertEquals(Wait::class.java, node6.javaClass)

        //WorkSplit
        val workSplit = WorkSplitDefn()
        workSplit.id = "workSplit"
        workSplit.workItemType = wi.javaClass
        val node7 = nodeLoader.load(workSplit, ctx)
        assertEquals(WorkSplit::class.java, node7.javaClass)

        //Batch
        val batch = BatchDefn()
        batch.id = "batch"
        batch.workItemType = wi.javaClass
        batch.workBatchType = wi.javaClass
        batch.event = EventDefn("eod")
        batch.rule = RuleDefn(Closure.IDENTITY)
        val node8 = nodeLoader.load(batch, ctx)
        assertEquals(Batch::class.java, node8.javaClass)

        //TaskNodes
        val swimlane = SwimlaneDefn()
        swimlane.id = "finance"


        //SystemTask
        val sysTask = TaskDefn()
        sysTask.id = "sysTask"
        sysTask.swimlane = swimlane
        sysTask.workItemType = wi.javaClass
        sysTask.execution = ExecutionDefn()
        sysTask.execution.actorType = ExecutionDefn.getSYSTEM()
        sysTask.execution.url = "/"
        sysTask.execution.event = EventDefn("taskCreated")
        val node9 = nodeLoader.load(sysTask, ctx)
        assertEquals(SystemTaskNode::class.java, node9.javaClass)


        //HumanTask
        val humanTask = TaskDefn()
        humanTask.id = "humanTask"
        humanTask.swimlane = swimlane
        humanTask.workItemType = wi.javaClass
        humanTask.execution = ExecutionDefn()
        humanTask.execution.actorType = ExecutionDefn.getHUMAN()
        humanTask.execution.url = "/"
        val node10 = nodeLoader.load(humanTask, ctx)
        assertEquals(HumanTaskNode::class.java, node10.javaClass)

        //HumanTask
        val extTask = TaskDefn()
        extTask.id = "extTask"
        extTask.swimlane = swimlane
        extTask.workItemType = wi.javaClass
        extTask.execution = ExecutionDefn()
        extTask.execution.actorType = ExecutionDefn.getEXTERNAL_PARTY()
        val node11 = nodeLoader.load(extTask, ctx)
        assertEquals(ExternalPartyTaskNode::class.java, node11.javaClass)

    }


    //------------------------------------------
    private fun createContext(): ContextData {
        val variablesMap = HashMap<String, Variable>()
        val workItemsMap = HashMap<String, Class<out WorkItem>>()
        val swimlanesMap = HashMap<String, Swimlane>()
        swimlanesMap.put("finance", Swimlane("finance"))
        val procDefn = ProcessDefn()
        procDefn.id = "test"
        val process = WfProcess(procDefn)
        return ContextData(variablesMap, workItemsMap, swimlanesMap, process)
    }
}

//====================================
class TestWI : WorkItem, WorkBatch {
    override fun setItems(workItems: MutableList<out WorkItem>?) {}

    override fun getItems(): MutableList<out WorkItem> {
        return ArrayList<WorkItem>()
    }

    override fun setKey(id: String?) {}

    override fun getKey(): String {
        return "123"
    }

}