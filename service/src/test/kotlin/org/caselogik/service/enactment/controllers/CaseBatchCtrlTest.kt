package org.caselogik.service.enactment.controllers

import junit.framework.TestCase.assertEquals
import org.caselogik.api.WorkBatch
import org.caselogik.api.commands.wfcase.AppendToCaseNote
import org.caselogik.api.commands.wfcase.CreateCase
import org.caselogik.api.commands.wfcase.CreateCaseBatch
import org.caselogik.api.commands.wfcase.CreateCaseNote
import org.caselogik.service.enactment.events.CaseBatchCreated
import org.caselogik.service.enactment.events.CaseCreated
import org.caselogik.service.enactment.entities.*
import org.caselogik.service.utils.EnactmentTest
import org.junit.Test
import org.mockito.Mockito.*


internal class CaseBatchCtrlTest : EnactmentTest() {


    //--------------------------------
    @Test
    fun onCommand_CreateCaseBatch() {
        //Given
        val cb = CaseBatch()
        cb.id = 1
        val wbToken = WorkBatchToken().apply{ id = 1L }
        `when`(caseCtrlHelper.createToken(any(), any())).thenReturn(wbToken)
        `when`(caseCtrlHelper.createCaseBatch(any(), any(), any())).thenReturn(cb)
        `when`(start.workDataTypeName).thenReturn("org.caselogik.utils.TestWB")
        `when`(start.toWorkData(anyString())).thenReturn(workBatch)
        val ctrl = CaseBatchCtrl(processRepo, caseCtrlHelper, eventSender)

        //When
        val cmd = CreateCaseBatch(TEST_PROCESS_ID, "json", -1)
        ctrl.onCommand(cmd)

        //Then
        val wb = arg<WorkBatch>()
        verify(processRepo).getProcess(TEST_PROCESS_ID)
        verify(caseCtrlHelper).createToken(wb.cap(), any())
        assertEquals(workBatch, wb.value)
        val cases = arg<List<WfCase>>()
        val token = arg<WorkBatchToken>()
        val procId = arg<String>()
        verify(caseCtrlHelper).createCaseBatch(procId.cap(), token.cap(), cases.cap())
        assertEquals(TEST_PROCESS_ID, procId.value)
        assertEquals(1L, token.value.id)
        assertEquals(listOf<WfCase>(), cases.value)

        val event = arg<CaseBatchCreated>()
        verify(eventSender).send(event.cap())
        assertEquals(1L, event.value.caseBatchId)

    }

}

