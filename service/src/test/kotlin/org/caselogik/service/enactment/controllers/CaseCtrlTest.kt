package org.caselogik.service.enactment.controllers

import junit.framework.TestCase.assertEquals
import org.caselogik.api.commands.wfcase.AppendToCaseNote
import org.caselogik.api.commands.wfcase.CreateCase
import org.caselogik.api.commands.wfcase.CreateCaseNote
import org.caselogik.service.enactment.entities.CaseNote
import org.caselogik.service.enactment.entities.TokenStatus
import org.caselogik.service.enactment.entities.WfCase
import org.caselogik.service.enactment.entities.WorkItemToken
import org.caselogik.service.enactment.events.CaseCreated
import org.caselogik.service.utils.EnactmentTest
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify


internal class CaseCtrlTest : EnactmentTest() {


    @Test
    fun onCommand_CreateCase() {

        //Given
        val cs = WfCase()
        cs.id = 1
        val wiToken = WorkItemToken()
        `when`(caseCtrlHelper.createToken(any())).thenReturn(wiToken)
        `when`(caseCtrlHelper.createCase(any(), any())).thenReturn(cs)
        val ctrl = CaseCtrl(processRepo, caseCtrlHelper, noteRepo, eventSender)

        //When
        val cmd = CreateCase(TEST_PROCESS_ID, "json",-1)
        ctrl.onCommand(cmd)

        //Then
        verify(processRepo).getProcess(TEST_PROCESS_ID)
        verify(caseCtrlHelper).createToken(workItem)
        verify(caseCtrlHelper).createCase(TEST_PROCESS_ID, wiToken)

        val event = arg<CaseCreated>()
        verify(eventSender).send(event.cap())
        assertEquals(1L, event.value.caseId)

    }

    //--------------------------------
    @Test
    fun onCommand_CreateCaseNote() {

        //Given
        val cs = WfCase()
        cs.id = 1L
        `when`(caseCtrlHelper.fetchCase(1L)).thenReturn(cs)

        val ctrl = CaseCtrl(processRepo, caseCtrlHelper, noteRepo, eventSender)

        //When
        val cmd = CreateCaseNote("note content", "nbiswas", 1L)
        ctrl.onCommand(cmd)

        //Then
        assertEquals(1, cs.caseNotes.size)
        val note = cs.caseNotes[0]
        assertEquals("note content", note.content)
        assertEquals("nbiswas", note.authorId)
    }

    //--------------------------------
    @Test
    fun onCommand_AppendToCaseNote() {

        //Given
        val cs = WfCase()
        cs.id = 1L
        `when`(caseCtrlHelper.fetchCase(1L)).thenReturn(cs)

        val note1 = CaseNote().apply { id = 1; content="content"; authorId="A" }
        val note2 = CaseNote().apply { id = 2; content="part 1"; authorId="B" }
        cs.caseNotes.addAll(listOf(note1, note2))


        val ctrl = CaseCtrl(processRepo, caseCtrlHelper, noteRepo, eventSender)

        //When
        val cmd = AppendToCaseNote("part 2", "C", 2, 1)
        ctrl.onCommand(cmd)

        //Then
        assertEquals(3, cs.caseNotes.size)
        val note = cs.caseNotes[2]
        assertEquals("part 2", note.content)
        assertEquals("C", note.authorId)
        assertEquals(2L, note.parentNoteId)
    }

    //--------------------------------
    @Test
    fun onCommand_CancelCase() {
    }

    //--------------------------------
    @Test
    fun onEvent_CaseCreated() {

        //Given
        val cs = WfCase()
        cs.id = 1L
        cs.processId = TEST_PROCESS_ID
        cs.tokens.add(WorkItemToken().apply { id = 1L; status = TokenStatus.Active })
        `when`(caseCtrlHelper.fetchCase(1L)).thenReturn(cs)
        val ctrl = CaseCtrl(processRepo, caseCtrlHelper, noteRepo, eventSender)

        //When
        val evt = CaseCreated(1)
        ctrl.loadEvent(evt)
        ctrl.onEvent(evt)

        //Then
        val caseId = arg<Long>()
        //val wait = arg<Boolean>()
        verify(caseCtrlHelper).fetchCase(caseId.capture())
        assertEquals(1L, caseId.value)


        val procId = arg<String>()
        verify(processRepo).getProcess(procId.cap())
        assertEquals(TEST_PROCESS_ID, procId.value)

        val token = arg<WorkItemToken>()
        verify(start).transitionIn(token.cap())
        assertEquals(1L, token.value.id)
        assertEquals(TokenStatus.Active, token.value.status)

    }

}

