package org.caselogik.service.utils

import org.caselogik.api.WorkBatch
import org.caselogik.api.WorkItem
import org.caselogik.service.definition.entities.Start
import org.caselogik.service.definition.entities.WfProcess
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.controllers.helpers.CaseCtrlHelper
import org.caselogik.service.enactment.events.EventSender
import org.caselogik.service.enactment.repositories.CaseBatchRepo
import org.caselogik.service.enactment.repositories.CaseRepo
import org.caselogik.service.enactment.repositories.NoteRepo
import org.caselogik.service.enactment.repositories.TokenRepo
import org.junit.Before
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

abstract class EnactmentTest {

    @Mock
    lateinit var processRepo: ProcessRepo

    @Mock
    lateinit var caseCtrlHelper: CaseCtrlHelper

    @Mock
    lateinit var tokenRepo: TokenRepo

    @Mock
    lateinit var caseRepo: CaseRepo

    @Mock
    lateinit var caseBatchRepo: CaseBatchRepo

    @Mock
    lateinit var noteRepo: NoteRepo

    @Mock
    lateinit var eventSender: EventSender

    @Mock
    lateinit var start: Start

    @Mock
    lateinit var process: WfProcess

    val workItem = TestWI()

    val workBatch = TestWB()

    @Before
    fun createMocks() {
        MockitoAnnotations.initMocks(this)
        setupTestProcessRepo()
    }

    //--------------------------
    protected val TEST_PROCESS_ID = "testProcess"

    private fun setupTestProcessRepo(){
        `when`(processRepo.getProcess(anyString())).thenReturn(process)
        `when`(process.start).thenReturn(start)
        `when`(process.id).thenReturn(TEST_PROCESS_ID)
        `when`(start.workDataTypeName).thenReturn("org.caselogik.utils.TestWI")
        `when`(start.toWorkData(anyString())).thenReturn(workItem)

    }

    //---------------------------
    protected fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    @Suppress("UNCHECKED_CAST")
    protected fun <T> uninitialized(): T = null as T

    inline protected fun <reified T> arg(): ArgumentCaptor<T>{
        return ArgumentCaptor.forClass(T::class.java)
    }

    protected fun <T> ArgumentCaptor<T>.cap(): T {
        return this.capture()
    }

}

//===============================
class TestWI : WorkItem {

    private var _key: String? = null
    override fun getKey(): String? = _key
    fun setKey(id: String?) { _key = id }
}

//===============================
class TestWB : WorkBatch {

    private var _key: String? = null
    override fun getKey(): String? = _key
    override fun setKey(id: String?) { _key = id }

    //--------------------------------------------------
    private var _items: MutableList<TestWI> = mutableListOf()
    override fun getItems(): MutableList<TestWI> = _items
    override fun setItems(workItems: MutableList<out WorkItem>) {
        _items = workItems.map{ it as TestWI }.toMutableList()
    }
}