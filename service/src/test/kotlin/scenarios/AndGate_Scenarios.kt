package scenarios

import org.caselogik.service.enactment.entities.CaseStatus
import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess


class AndGate_Scenarios : BaseScenarioTest() {

    @Test
    @Transactional
    fun andGate_with_no_rule() {

        val proc = TestProcess("and_split_1", "SimpleWork")
                .withNodes("""
                     andGate('and_1')
                     end('end_1')
                    """)
                .withFlow("""
                    start >> N('and_1')-['ok'] >> N('end_1')
                             N('and_1')-['notOK'] >> end
                    """)

        loadProcess(proc)
        printFlow()

        createCase(simpleWork(ok = true, key="C1"))
        assertTokensState(listOf(
                "Token(type=WorkItemToken, node=and_1, status=Inactive)",
                "Token(type=WorkItemToken, node=end_1, status=Dead)",
                "Token(type=WorkItemToken, node=end, status=Dead)"
        ))

        assertCaseStatus(CaseStatus.Completed)

        assertCaseLog(":CaseCreated:, start:TokenRouted:and_1, and_1:TokenSplit:, and_1:TokenSplit:, and_1:TokenRouted:end_1, and_1:TokenRouted:end, :CaseCompleted:\n")
    }

    //----------------------------------------------------
    @Test
    @Transactional
    fun andGate_with_rule() {
        val proc = TestProcess("and_split_2", "SimpleWork")
                .withNodes("""
                     andGate('and_1'){
                        rule {
                            if (_workData.ok) _goVia('ok', 'somewhatOK')
                            else _goVia('notOK')
                        }
                     }
                     end('end_1')
                     end('end_2')
                     end('end_3')
                    """)
                .withFlow("""
                    start >> N('and_1')-['ok'] >> N('end_1')
                             N('and_1')-['notOK'] >> N('end_2')
                             N('and_1')-['somewhatOK'] >> N('end_3')
                    """)

        loadProcess(proc)
        printFlow()

        createCase(simpleWork(ok = true, key="C1"))
        assertTokensState(listOf(
                "Token(type=WorkItemToken, node=and_1, status=Inactive)",
                "Token(type=WorkItemToken, node=end_1, status=Dead)",
                "Token(type=WorkItemToken, node=end_3, status=Dead)"
        ))

        createCase(simpleWork(ok = false, key="C2"))
        assertTokensState(listOf(
                "Token(type=WorkItemToken, node=and_1, status=Inactive)",
                "Token(type=WorkItemToken, node=end_1, status=Dead)",
                "Token(type=WorkItemToken, node=end_3, status=Dead)",
                "Token(type=WorkItemToken, node=and_1, status=Inactive)",
                "Token(type=WorkItemToken, node=end_2, status=Dead)"
        ))

        assertCaseLog(":CaseCreated:, start:TokenRouted:and_1, and_1:TokenSplit:, and_1:TokenRouted:end_2, :CaseCompleted:")
    }
}