package scenarios

import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess

class Batch_Scenarios : BaseScenarioTest() {

    @Test
    @Transactional
    fun test1() {
        val proc = TestProcess("batch_1", "Payment")
                .withNodes("""
                     batch('batch_1'){
                        when = onEvent('endOfDay')
                        workBatch = PaymentBatch
                        rule {
                            _batchKey = _workItem.bankCode
                        }
                    }
                    """)
                .withFlow("""
                    start >> N('batch_1') >> end
                    """)

        loadProcess(proc)
        printFlow()
        createCase(payment(amount = 10F, bankCode = "HSBC1", key="C1"))
        createCase(payment(amount = 20F, bankCode = "HSBC2", key="C2"))
        createCase(payment(amount = 30F, bankCode = "HSBC1", key="C3"))
        createCase(payment(amount = 40F, bankCode = "HSBC1", key="C4"))
        createCase(payment(amount = 50F, bankCode = "HSBC1", key="C5"))
        createCase(payment(amount = 60F, bankCode = "HSBC2", key="C6"))
        createCase(payment(amount = 70F, bankCode = "HSBC2", key="C7"))

        assertTokensState(listOf(
                "Token(type=WorkItemToken, node=batch_1, status=Active)",
                "Token(type=WorkItemToken, node=batch_1, status=Active)",
                "Token(type=WorkItemToken, node=batch_1, status=Active)",
                "Token(type=WorkItemToken, node=batch_1, status=Active)",
                "Token(type=WorkItemToken, node=batch_1, status=Active)",
                "Token(type=WorkItemToken, node=batch_1, status=Active)",
                "Token(type=WorkItemToken, node=batch_1, status=Active)"
        ))

        fireExternalEvent("endOfDay","2019-05-27")

        assertTokensState(listOf(
                "Token(type=WorkItemToken, node=end, status=Dead)",
                "Token(type=WorkItemToken, node=end, status=Dead)",
                "Token(type=WorkItemToken, node=end, status=Dead)",
                "Token(type=WorkItemToken, node=end, status=Dead)",
                "Token(type=WorkItemToken, node=end, status=Dead)",
                "Token(type=WorkItemToken, node=end, status=Dead)",
                "Token(type=WorkItemToken, node=end, status=Dead)",
                "Token(type=WorkBatchToken, node=end, status=Dead)",
                "Token(type=WorkBatchToken, node=end, status=Dead)"
        ))

        assertCaseLog(":CaseCreated:, start:TokenRouted:batch_1, :CaseCompleted:")
    }
}