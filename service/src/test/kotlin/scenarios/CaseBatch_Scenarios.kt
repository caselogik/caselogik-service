package scenarios

import junit.framework.TestCase.assertEquals
import org.caselogik.service.enactment.entities.CaseBatchStatus
import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess

class CaseBatch_Scenarios : BaseScenarioTest() {

    @Test
    @Transactional
    fun test1() {

        val proc = TestProcess("casebatch_1", "PaymentBatch")
                .withNodes("""
                     task('createBacsFile') {
                        swimlane = 'default'
                        execution {
                            by = human
                            url = 'http://localhost:8081/createBacsFile'
                        }
                    }
                   """)
                .withFlow("""
                    start >> N('createBacsFile') >> end
                    """)
        loadProcess(proc)
        printFlow()
        createCaseBatch(paymentBatch())

        assertTokensState(listOf(
                "Token(type=WorkItemToken, node=createBacsFile, status=Inactive)",
                "Token(type=WorkItemToken, node=createBacsFile, status=Inactive)",
                "Token(type=WorkItemToken, node=createBacsFile, status=Inactive)",
                "Token(type=WorkBatchToken, node=createBacsFile, status=Active)"
                ))

        val caseBatch1 = getAllCaseBatches()[0]
        assertEquals("Active", caseBatch1.status)
        assertEquals(3, caseBatch1.cases.count())

        val task = getAllTasks()[0]
        activateTask(task)
        completeTask(task)

        assertTokensState(listOf(
                "Token(type=WorkItemToken, node=end, status=Dead)",
                "Token(type=WorkItemToken, node=end, status=Dead)",
                "Token(type=WorkItemToken, node=end, status=Dead)",
                "Token(type=WorkBatchToken, node=end, status=Dead)"
                ))


        assertCaseBatchStatus(CaseBatchStatus.Completed)

        //TODO assertCaseLog("")
    }
}