package scenarios

import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess

class CaseNote_Scenarios : BaseScenarioTest() {

    //----------------------------------------------------
    @Test
    @Transactional
    fun createCaseNote() {
        val proc = TestProcess("case_note_1", "SimpleWork")
                .withNodes("""
                    wait('wait_1') {
                        until = onEvent('someEvent')
                    }                    """)
                .withFlow("""
                    start >> N('wait_1') >> end
                    """)

        loadProcess(proc)
        printFlow()

        createCase(simpleWork(ok = true, key="C1"))

        createCaseNote("Hello", "nb")

        val cases = getAllCases()
        assertEquals(1, cases[0].caseNotes.size)
        val note = cases[0].caseNotes[0]
        assertEquals("Hello", note.content)
        assertEquals("nb", note.authorId)

        val noteId = note.id

        appendToCaseNote("Hello again", "pb", noteId)

        val cases2 = getAllCases()
        assertEquals(2, cases2[0].caseNotes.size)
        val note2 = cases2[0].caseNotes[1]
        assertEquals("Hello again", note2.content)
        assertEquals("pb", note2.authorId)


    }

}