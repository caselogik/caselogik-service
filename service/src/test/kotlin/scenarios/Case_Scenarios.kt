package scenarios

import org.caselogik.api.commands.wfcase.CancelCase
import org.caselogik.service.enactment.entities.CaseStatus
import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess

class Case_Scenarios : BaseScenarioTest() {

    @Test
    @Transactional
    fun test1() {
        val proc = TestProcess("task_3", "Payment")
                .withNodes("""
                     task('evaluateRisk') {
                        swimlane = 'default'
                        execution {
                            by = human
                            url = 'http://localhost:8081/evaluateRisk'
                        }
                        priority {
                            rule {
                                if(_workData.amount > 10.0) _priority = 1
                                else _priority = 2
                            }
                        }
                    }
                   """)
                .withFlow("""
                        start >> N('evaluateRisk') >> end
                   """)

        loadProcess(proc)
        printFlow()

        //Start the process
        createCase(payment(amount=10.5F, key="C1"))

        caseCtrl.onCommand(CancelCase(thisCaseId))

        assertCaseStatus(CaseStatus.Cancelled)

        assertTokensState(listOf("Token(type=WorkItemToken, node=evaluateRisk, status=Dead)"))

        assertWorkData(listOf("WorkItem{ type='task_3.Payment', json='{\"key\":\"C1\",\"bankCode\":\"HSBC1234\",\"amount\":10.5}', isCancelled=true }"))

        assertCaseLog(":CaseCreated:, start:TokenRouted:evaluateRisk, evaluateRisk:TaskCreated:")
    }
}