package scenarios

import junit.framework.TestCase.assertEquals
import org.caselogik.api.commands.task.RecordTaskFailed
import org.caselogik.api.commands.task.RetryFailedTask
import org.caselogik.service.enactment.entities.CaseStatus
import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess


class ExternalPartyTaskNode_Scenarios : BaseScenarioTest() {

    @Test
    @Transactional
    fun executeTask() {

        val proc = TestProcess("task_1", "Payment")
                .withNodes("""
                     task('collectPayments') {
                        swimlane = 'default'
                        execution {
                            by = externalParty
                        }
                    }
                   """)
                .withFlow("""
                        start >> N('collectPayments') >> end
                   """)

        loadProcess(proc)

        //Start process
        createCase(payment(amount = 10.5F, key="C1"))

        //Task gets created and allocated
        assertTaskState(listOf(
                "Task(nodeId='collectPayments', status='Active', workerType='ExternalParty', workerId='collectPayments', workDataType='Payment')"
        ))

        //Complete task
        completeTask(getOnlyTask())

        assertTaskState(listOf(
                "Task(nodeId='collectPayments', status='Completed', workerType='None', workerId='null', workDataType='Payment')"
        ))

        //Case completes
        assertCaseStatus(CaseStatus.Completed)
        assertTokensState(listOf("Token(type=WorkItemToken, node=end, status=Dead)"))

        assertCaseLog(":CaseCreated:, start:TokenRouted:collectPayments, collectPayments:TaskCreated:, collectPayments:TaskCompleted:, collectPayments:TokenRouted:end, :CaseCompleted:")
    }

}