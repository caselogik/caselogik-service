package scenarios

import junit.framework.TestCase.assertEquals
import org.caselogik.api.commands.task.ClaimTaskInQueue
import org.caselogik.api.commands.task.RecordTaskReferralCompleted
import org.caselogik.api.commands.task.ReferTask
import org.caselogik.service.enactment.entities.CaseStatus
import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess


class HumanTaskNode_Scenarios : BaseScenarioTest() {

    @Test
    @Transactional
    fun allocateActivateAndComplete() {

        val proc = TestProcess("task_2", "Payment")
                .withNodes("""
                     task('evaluateRisk') {
                        swimlane = 'default'
                        execution {
                            by = human
                            url = 'http://localhost:8081/evaluateRisk'
                        }
                    }
                   """)
                .withFlow("""
                        start >> N('evaluateRisk') >> end
                   """)

        loadProcess(proc)
        printFlow()

        //Start the process
        createCase(payment(amount = 10.5F, key = "C1"))

        //Task gets created
        assertTaskState(listOf(
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='None', workerId='null', workDataType='Payment')"
        ))

        var tasksInQ = getTasksInQueue("task_2", "evaluateRisk")
        assertEquals(1, tasksInQ.size)

        //Task allocated to user
        allocateTask(tasksInQ[0], "nbiswas")

        assertTaskState(listOf(
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='Human', workerId='nbiswas', workDataType='Payment')"
        ))

        val tasksInWl = getTasksInWorklist("nbiswas")
        assertEquals(1, tasksInWl.size)
        tasksInQ = getTasksInQueue("task_1", "evaluateRisk")
        assertEquals(0, tasksInQ.size)

        //Task activated
        activateTask(tasksInWl[0])

        assertTaskState(listOf(
                "Task(nodeId='evaluateRisk', status='Active', workerType='Human', workerId='nbiswas', workDataType='Payment')"
        ))

        //task completed with updated workitem
        completeTask(tasksInWl[0], payment(amount = 30.2F, key = "C1"))

        assertTaskState(listOf(
                "Task(nodeId='evaluateRisk', status='Completed', workerType='None', workerId='null', workDataType='Payment')"
        ))

        val updatedWiJson = getAllTasks()[0].workDataJson
        assertEquals("""{"key":"C1","bankCode":"HSBC1234","amount":10.5}""", updatedWiJson)

        //Case completes
        assertCaseStatus(CaseStatus.Completed)
        assertTokensState(listOf("Token(type=WorkItemToken, node=end, status=Dead)"))

        assertCaseLog(":CaseCreated:, start:TokenRouted:evaluateRisk, evaluateRisk:TaskCreated:, evaluateRisk:TaskAllocated:, evaluateRisk:TaskActivated:, evaluateRisk:TaskCompleted:, evaluateRisk:TokenRouted:end, :CaseCompleted:")

    }


    //-----------------------------------------------------------
    @Test
    @Transactional
    fun taskClaim() {
        val proc = TestProcess("task_1", "Payment")
                .withNodes("""
                     task('evaluateRisk') {
                        swimlane = 'default'
                        execution {
                            by = human
                            url = 'http://localhost:8081/evaluateRisk'
                        }
                    }
                   """)
                .withFlow("""
                        start >> N('evaluateRisk') >> end
                   """)

        loadProcess(proc)


        //Start 4 cases
        createCase(payment(amount = 10.5F, key = "C1"))
        createCase(payment(amount = 10.5F, key = "C2"))
        createCase(payment(amount = 10.5F, key = "C3"))
        createCase(payment(amount = 10.5F, key = "C4"))

        //nbiswas claims 1st task
        val cmd1 = ClaimTaskInQueue( humanWorker("nbiswas"))
        cmd1.processId = "task_1"
        cmd1.taskNodeId = "evaluateRisk"
        taskCtrl.onCommand(cmd1)


        assertTaskState(listOf(
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='Human', workerId='nbiswas', workDataType='Payment')",
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='None', workerId='null', workDataType='Payment')",
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='None', workerId='null', workDataType='Payment')",
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='None', workerId='null', workDataType='Payment')"
        ))

        //ibiswas claims next task
        val cmd2 = ClaimTaskInQueue(humanWorker("ibiswas"))
        cmd2.processId = "task_1"
        cmd2.taskNodeId = "evaluateRisk"
        taskCtrl.onCommand(cmd2)

        assertTaskState(listOf(
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='Human', workerId='nbiswas', workDataType='Payment')",
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='Human', workerId='ibiswas', workDataType='Payment')",
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='None', workerId='null', workDataType='Payment')",
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='None', workerId='null', workDataType='Payment')")
        )

        //The priority of the 4th task is upped, so the next claim should get it over the 3rd one
        val tasksInQueue1 = getAllTasks().sortedBy { it.createdAt };
        changeTaskPriority(tasksInQueue1[3], 1)
        val cmd3 = ClaimTaskInQueue(humanWorker("nbiswas"))
        cmd3.processId = "task_1"
        cmd3.taskNodeId = "evaluateRisk"
        taskCtrl.onCommand(cmd3)

        assertTaskState(listOf(
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='Human', workerId='nbiswas', workDataType='Payment')",
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='Human', workerId='ibiswas', workDataType='Payment')",
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='None', workerId='null', workDataType='Payment')",
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='Human', workerId='nbiswas', workDataType='Payment')")
        )

        assertCaseLog(":CaseCreated:, start:TokenRouted:evaluateRisk, evaluateRisk:TaskCreated:, evaluateRisk:TaskPriorityChanged:, evaluateRisk:TaskClaimed:")
    }

    //-----------------------------------------------------------
    @Test
    @Transactional
    fun taskReferral() {
        val proc = TestProcess("task_1", "Payment")
                .withNodes("""
                     task('evaluateRisk') {
                        swimlane = 'default'
                        execution {
                            by = human
                            url = 'http://localhost:8081/evaluateRisk'
                        }
                    }
                   """)
                .withFlow("""
                        start >> N('evaluateRisk') >> end
                   """)

        loadProcess(proc)


        //Start case
        createCase(payment(amount = 10.5F, key = "C1"))

        //Allocate task
        var task = getOnlyTask()
        allocateTask(task, "nbiswas")


        assertTaskState(listOf(
                "Task(nodeId='evaluateRisk', status='Inactive', workerType='Human', workerId='nbiswas', workDataType='Payment')"
        ))

        val cmd1 = ReferTask(humanWorker("expert1"))
        cmd1.taskId = task.id
        taskCtrl.onCommand(cmd1)

        task = getOnlyTask()
        assertEquals(true, task.isReferred)
        assertEquals("nbiswas", task.referrer.id)
        assertEquals("expert1", task.worker.id)

        val cmd2 = RecordTaskReferralCompleted(null)
        cmd2.taskId = task.id
        taskCtrl.onCommand(cmd2)

        task = getOnlyTask()
        assertEquals(false, task.isReferred)
        assertEquals(null, task.referrer.id)
        assertEquals("None", task.referrer.type)
        assertEquals("nbiswas", task.worker.id)

        assertCaseLog(":CaseCreated:, start:TokenRouted:evaluateRisk, evaluateRisk:TaskCreated:, evaluateRisk:TaskAllocated:, evaluateRisk:TaskReferred:, evaluateRisk:TaskReferred:")

    }
}