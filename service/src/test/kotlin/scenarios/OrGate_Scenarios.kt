package scenarios

import org.caselogik.service.enactment.entities.CaseStatus
import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess


class OrGate_Scenarios : BaseScenarioTest() {

    @Test
    @Transactional
    fun simple_orGate() {

        val proc = TestProcess("or_gate_1", "SimpleWork")
                .withNodes("""
                    orGate('or_1') {
                        rule {
                            if(_workData.ok) _goVia('ok')
                            else _goVia('notOK')
                        }
                    }
                    end('end_1')
                    """)
                .withFlow("""
                    start >> N('or_1')-['ok'] >> N('end_1')
                             N('or_1')-['notOK'] >> end
                    """)

        loadProcess(proc)
        printFlow()

        createCase(simpleWork(ok=true, key="C1"))

        assertTokensState(listOf(
                "Token(type=WorkItemToken, node=end_1, status=Dead)"
        ))

        assertCaseStatus(CaseStatus.Completed)

        assertCaseLog(":CaseCreated:, start:TokenRouted:or_1, or_1:TokenRouted:end_1, :CaseCompleted:")
    }
}