package scenarios

import org.caselogik.service.enactment.entities.CaseStatus
import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess


class Sync_Scenarios : BaseScenarioTest() {

    @Test
    @Transactional
    fun sync() {

        val proc = TestProcess("sync_1", "SimpleWork")
                .withNodes("""
                    andGate('and_1')
                    sync('sync_1') {
                        splitNode = 'and_1'
                    }
                    wait('wait_1') {
                        until = onEvent('OK')
                    }
                    """)
                .withFlow("""
                    start >> N('and_1') >> N('wait_1') >> N('sync_1')
                             N('and_1') >> N('sync_1') >> end
                    """)

        loadProcess(proc)
        printFlow()

        createCase(simpleWork(ok = true, key="C1"))

        fireExternalEvent("OK", "1")

        assertTokensState(listOf(
                "Token(type=WorkItemToken, node=end, status=Dead)",
                "Token(type=WorkItemToken, node=sync_1, status=Dead)",
                "Token(type=WorkItemToken, node=sync_1, status=Dead)"
        ))

        assertCaseStatus(CaseStatus.Completed)

        assertCaseLog(":CaseCreated:, start:TokenRouted:and_1, and_1:TokenSplit:, and_1:TokenSplit:, and_1:TokenRouted:wait_1, and_1:TokenRouted:sync_1, wait_1:ExternalEventMatched:, wait_1:TokenRouted:sync_1, sync_1:TokenMerged:, sync_1:TokenMerged:, sync_1:TokenRouted:end, :CaseCompleted:")

    }
}