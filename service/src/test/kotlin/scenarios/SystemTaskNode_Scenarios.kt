package scenarios

import junit.framework.TestCase.assertEquals
import org.caselogik.api.commands.task.RecordTaskFailed
import org.caselogik.api.commands.task.RetryFailedTask
import org.caselogik.service.enactment.entities.CaseStatus
import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess


class SystemTaskNode_Scenarios : BaseScenarioTest() {

    @Test
    @Transactional
    fun executeTask() {

        val proc = TestProcess("task_1", "Payment")
                .withNodes("""
                     task('collectPayments') {
                        swimlane = 'default'
                        execution {
                            by = system
                            when = immediately
                            url = 'http://localhost:8080'
                        }
                    }
                   """)
                .withFlow("""
                        start >> N('collectPayments') >> end
                   """)

        loadProcess(proc)

        //Start process
        createCase(payment(amount = 10.5F, key="C1"))

        //Task gets created and allocated
        assertTaskState(listOf(
                "Task(nodeId='collectPayments', status='Active', workerType='System', workerId='collectPayments', workDataType='Payment')"
        ))


        val taskId = getOnlyTask().id

        assertExecRequests(listOf(
                "command='execute', url='http://localhost:8080?taskId=$taskId&workDataKey=C1', taskNode='collectPayments', workDataType='task_1.Payment'"
        ))



        //Complete task
        completeTask(getOnlyTask())

        assertTaskState(listOf(
                "Task(nodeId='collectPayments', status='Completed', workerType='None', workerId='null', workDataType='Payment')"
        ))

        //Case completes
        assertCaseStatus(CaseStatus.Completed)
        assertTokensState(listOf("Token(type=WorkItemToken, node=end, status=Dead)"))

        assertCaseLog(":CaseCreated:, start:TokenRouted:collectPayments, collectPayments:TaskCreated:, collectPayments:TaskActivated:, collectPayments:TaskCompleted:, collectPayments:TokenRouted:end, :CaseCompleted:")
    }

    //-----------------------------------------
    @Test
    @Transactional
    fun retryTask() {

        val proc = TestProcess("task_1", "Payment")
                .withNodes("""
                     task('collectPayments') {
                        swimlane = 'default'
                        execution {
                            by = system
                            when = immediately
                            url = 'http://localhost:8080'
                        }
                    }
                   """)
                .withFlow("""
                        start >> N('collectPayments') >> end
                   """)

        loadProcess(proc)

        createCase(payment(amount = 10.5F, key="C1"))

        var task = getOnlyTask()
        val cmd1 = RecordTaskFailed("NINO missing", null)
        cmd1.taskId = task.id
        taskCtrl.onCommand(cmd1)

        assertTaskState(listOf(
                "Task(nodeId='collectPayments', status='Failed', workerType='System', workerId='collectPayments', workDataType='Payment')"
        ))

        task = getOnlyTask()
        assertEquals("NINO missing", task.executionDetails.lastFailureReason)

        val cmd2 = RetryFailedTask()
        cmd2.taskId = task.id
        taskCtrl.onCommand(cmd2)
        assertTaskState(listOf(
                "Task(nodeId='collectPayments', status='Active', workerType='System', workerId='collectPayments', workDataType='Payment')"
        ))

        val taskId = getOnlyTask().id

        assertExecRequests(listOf(
                "command='execute', url='http://localhost:8080?taskId=$taskId&workDataKey=C1', taskNode='collectPayments', workDataType='task_1.Payment'",
                "command='retry', url='http://localhost:8080?taskId=$taskId&workDataKey=C1', taskNode='collectPayments', workDataType='task_1.Payment'"

        ))

        //Complete task
        completeTask(getOnlyTask())

        //Case completes
        assertCaseStatus(CaseStatus.Completed)
        assertTokensState(listOf("Token(type=WorkItemToken, node=end, status=Dead)"))

        assertCaseLog(":CaseCreated:, start:TokenRouted:collectPayments, collectPayments:TaskCreated:, collectPayments:TaskActivated:, collectPayments:TaskFailed:, collectPayments:TaskActivated:, collectPayments:FailedTaskRetried:, collectPayments:TaskCompleted:, collectPayments:TokenRouted:end, :CaseCompleted:")
    }

}