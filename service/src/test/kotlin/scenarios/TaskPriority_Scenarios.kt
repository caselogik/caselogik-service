package scenarios

import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess

class TaskPriority_Scenarios : BaseScenarioTest() {

    @Test
    @Transactional
    fun test1() {
        val proc = TestProcess("task_1", "Payment")
                .withNodes("""
                     task('evaluateRisk') {
                        swimlane = 'default'
                        execution {
                            by = human
                            url = 'http://localhost:8081/evaluateRisk'
                        }
                        priority {
                            rule {
                                if(_workData.amount > 10.0) _priority = 1
                                else _priority = 2
                            }
                        }
                    }
                   """)
                .withFlow("""
                        start >> N('evaluateRisk') >> end
                   """)

        loadProcess(proc)
        printFlow()

        //Start the process
        createCase(payment(amount=10.5F, key="C1"))
        createCase(payment(amount=5.5F, key="C2"))

        val tasks = getTasksInQueue("task_1", "evaluateRisk")
        assertEquals(1, tasks[0].priority)
        assertEquals(2, tasks[1].priority)

        assertCaseLog(":CaseCreated:, start:TokenRouted:evaluateRisk, evaluateRisk:TaskCreated:")
    }
}