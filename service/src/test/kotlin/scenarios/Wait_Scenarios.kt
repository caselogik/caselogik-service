package scenarios

import org.caselogik.service.enactment.entities.CaseStatus
import org.junit.Test
import org.springframework.transaction.annotation.Transactional
import utils.BaseScenarioTest
import utils.TestProcess


class Wait_Scenarios : BaseScenarioTest() {


    //------------------------------------------------
    @Test
    @Transactional
    fun wait_until_event() {

        val proc = TestProcess("wait_2", "SimpleWork")
                .withNodes("""
                    wait('wait_2') {
                        timeOut = '1h20m'
                        until = onEvent('someEvent')
                    }                    """)
                .withFlow("""
                    start >> N('wait_2') >> end
                    """)

        loadProcess(proc)
        printFlow()

        createCase(simpleWork(ok = true, key="C1"))

        fireExternalEvent("someEvent","someKey")

        assertCaseLog(":CaseCreated:, start:TokenRouted:wait_2, wait_2:ExternalEventMatched:, wait_2:TokenRouted:end, :CaseCompleted:")

    }

    //------------------------------------------------------
    @Test
    @Transactional
    fun wait_until_timeout() {

        val proc = TestProcess("wait_1", "SimpleWork")
                .withNodes("""
                    wait('wait_1') {
                        timeOut = '2s'
                    }                    """)
                .withFlow("""
                    start >> N('wait_1') >> end
                    """)

        loadProcess(proc)
        printFlow()

        createCase(simpleWork(ok = true, key="C1"))

        pause(3)

        assertEventFired("WaitTimedOut")
    }


}