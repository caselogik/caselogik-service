package testworkitems

import org.caselogik.api.WorkItem

class Payment implements WorkItem {
    String key
    String bankCode
    Date date
    float amount
}