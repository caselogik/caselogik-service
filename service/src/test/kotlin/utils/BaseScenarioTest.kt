package utils

import org.caselogik.api.Case
import org.caselogik.api.CaseBatch
import org.caselogik.api.commands.FireExternalEvent
import org.caselogik.api.commands.task.ActivateTask
import org.caselogik.api.commands.task.AllocateTask
import org.caselogik.api.commands.task.ChangeTaskPriority
import org.caselogik.api.commands.task.RecordTaskCompleted
import org.caselogik.api.commands.wfcase.AppendToCaseNote
import org.caselogik.api.commands.wfcase.CreateCase
import org.caselogik.api.commands.wfcase.CreateCaseBatch
import org.caselogik.api.commands.wfcase.CreateCaseNote
import org.caselogik.service.definition.entities.WfProcess
import org.caselogik.service.definition.loader.ProcessLoader
import org.caselogik.service.definition.repositories.ProcessRepo
import org.caselogik.service.enactment.controllers.CaseBatchCtrl
import org.caselogik.service.enactment.controllers.CaseCtrl
import org.caselogik.service.enactment.controllers.ProcessCtrl
import org.caselogik.service.enactment.controllers.TaskCtrl
import org.caselogik.service.enactment.entities.*
import org.caselogik.service.enactment.logging.LogEntry
import org.caselogik.service.enactment.logging.LogRepo
import org.caselogik.service.enactment.mappers.CaseBatchTOMapper
import org.caselogik.service.enactment.mappers.CaseTOMapper
import org.caselogik.service.enactment.mappers.TaskTOMapper
import org.caselogik.service.enactment.mappers.WorkerTOMapper
import org.caselogik.service.enactment.repositories.CaseBatchRepo
import org.caselogik.service.enactment.repositories.CaseRepo
import org.caselogik.service.enactment.repositories.TaskRepo
import org.caselogik.service.enactment.repositories.TokenRepo
import org.junit.Assert.assertEquals
import org.springframework.beans.factory.annotation.Autowired
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import org.caselogik.api.Task as TaskTO
import org.caselogik.api.Worker as WorkerTO


abstract class BaseScenarioTest: BaseTest() {

    @Autowired
    lateinit var caseCtrl: CaseCtrl

    @Autowired
    lateinit var caseBatchCtrl: CaseBatchCtrl

    @Autowired
    lateinit var taskCtrl: TaskCtrl

    @Autowired
    lateinit var processRepo: ProcessRepo

    @Autowired
    lateinit var procLoader: ProcessLoader

    @Autowired
    lateinit var procCtrl: ProcessCtrl

    @Autowired
    lateinit var caseRepo: CaseRepo

    @Autowired
    lateinit var caseBatchRepo: CaseBatchRepo

    @Autowired
    lateinit var taskRepo: TaskRepo

    @Autowired
    lateinit var tokenRepo: TokenRepo

    @Autowired
    lateinit var logRepo: LogRepo

    @PersistenceContext
    lateinit var entityManager: EntityManager

    @Autowired
    lateinit var testEventSender: TestEventSender

    //-----------------------------------------
    var thisCaseId: Long = 0
    var thisCaseBatchId: Long = 0

    protected lateinit var process: WfProcess

    //-----------------------------------------
    fun loadProcess(proc: TestProcess){
        procLoader.loadProcess(proc.build())
        process = processRepo.getProcess(proc.id)
    }

    //-----------------------------------------
    fun printFlow() {
        process.printFlow()
    }

    //----------------------------------------
    override fun _beforeEachTest() {
        procLoader.unloadAllProcesses()
        testEventSender.eventLog.clear()
    }

    //---------------------------------------------------
    fun simpleWork(key: String, ok: Boolean): String {
        return """{"key":"$key", "ok": "${ok.toString()}"}"""

    }
    //----------------------------------------
    fun payment(key: String, amount: Float, bankCode:String="HSBC1234"): String {
        return """{"key":"$key", "amount": "${amount.toString()}", "bankCode":"$bankCode" }"""
    }
    //----------------------------------------
    fun paymentBatch(): String {
        return """
            {
                "key": "HSBC1234",
                "items": [
                        {"key":"PMT1", "amount": "12.10", "bankCode":"HSBC1234" },
                        {"key":"PMT2", "amount": "13.10", "bankCode":"HSBC1234" },
                        {"key":"PMT3", "amount": "14.10", "bankCode":"HSBC1234" }
                        ]
            }


            """.trimIndent()
    }
    //-----------------------------------------
    fun createCase(workItemData: String){
        val resp = caseCtrl.onCommand(CreateCase(process.id, workItemData,-1))
        thisCaseId = resp.caseId
    }

    //---------------------------------------
    fun getThisCase(): WfCase = caseRepo.findById(thisCaseId).get()

    //---------------------------------------
    fun getThisCaseBatch(): org.caselogik.service.enactment.entities.CaseBatch = caseBatchRepo.getById(thisCaseBatchId)

    //-----------------------------------------
    fun createCaseBatch(workItemData: String){
        val resp = caseBatchCtrl.onCommand(CreateCaseBatch(process.id, workItemData, -1))
        thisCaseBatchId = resp.caseBatchId
    }
   //-----------------------------------------
    fun flushToDb() {
        entityManager.flush()
    }

    //-----------------------------------------
    fun assertEventFired(eventName: String){
        val events = testEventSender.eventLog.map{ it.javaClass.simpleName }
        assertEquals(true, events.contains(eventName))
    }

    //-----------------------------------------
    fun assertCaseStatus(status: CaseStatus){
        assertEquals(status, getThisCase().status)
    }

    //-----------------------------------------
    fun assertCaseBatchStatus(status: CaseBatchStatus){
        assertEquals(status, getThisCaseBatch().status)
    }

    //-----------------------------------------
    fun assertTaskState(expected: List<String>){
        val actual = taskRepo.findAll().map{"Task(nodeId='${it.taskNodeId}', status='${it.status}', workerType='${it.worker.type}', workerId='${it.worker.id}', workDataType='${it.token.workData.dataObject.javaClass.simpleName}')"}
        assertEquals(expected.joinToString(", "), actual.joinToString(", "))
    }

    //-----------------------------------------
    fun assertTokensState(expected: List<String>){
        val actual = tokenRepo.findAll().map{ "Token(type=${it.javaClass.simpleName}, node=${it.currentNodeId}, status=${it.status})" }
        assertEquals(expected.joinToString(", "), actual.joinToString(", "))
    }

    //------------------------------------
    fun assertWorkData(expected: List<String>){
        val actual = tokenRepo.findAll().map{ it.workData.toString() }
        assertEquals(expected.joinToString(", "), actual.joinToString(", "))
    }

    //--------------------------------------
    fun getTasksInQueue(processId: String, taskNodeId: String): List<TaskTO>{
        val result = taskRepo.getTasksInQueue(processId, taskNodeId).map{ TaskTOMapper.makeTO(it)}
        return result;
    }

    //--------------------------------------
    fun getTasksInWorklist(workerId: String): List<TaskTO>{
        val result = taskRepo.getTasksInWorklist(workerId).map{ TaskTOMapper.makeTO(it)}
        return result;
    }

    //------------------------------------------------------------------------
    fun completeTask(task: TaskTO, workItemData: String){
        val cmd = RecordTaskCompleted(workItemData)
        cmd.taskId = task.id
        taskCtrl.onCommand(cmd)
    }

    //------------------------------------------------------------------------
    fun completeTask(task: TaskTO){
        val cmd = RecordTaskCompleted(null)
        cmd.taskId = task.id
        taskCtrl.onCommand(cmd)
    }

    //------------------------------------
    fun allocateTask(task: TaskTO, workerId: String){
        val cmd = AllocateTask(humanWorker(workerId))
        cmd.taskId = task.id
        taskCtrl.onCommand(cmd)
    }

    //------------------------------------
    fun getOnlyTask(): TaskTO {
        return TaskTOMapper.makeTO(taskRepo.findAll().iterator().next())
    }

    //------------------------------------
    fun humanWorker(workerId: String): WorkerTO {
        return WorkerTOMapper.fromWorker(Worker.human(workerId))
    }

    //------------------------------------
    fun activateTask(task: TaskTO){
        val cmd = ActivateTask()
        cmd.taskId = task.id
        taskCtrl.onCommand(cmd)
    }

    //------------------------------------
    fun changeTaskPriority(task: TaskTO, priority: Int){
        val cmd = ChangeTaskPriority(priority)
        cmd.taskId = task.id
        taskCtrl.onCommand(cmd)
    }
    //------------------------------------
    fun fireExternalEvent(name: String, key: String){
        procCtrl.onCommand(FireExternalEvent(name, key))
    }

    //---------------------------------------------------
    fun createCaseNote(content: String, authorId: String){
        val cmd = CreateCaseNote(content, authorId, thisCaseId)
        caseCtrl.onCommand(cmd)
    }
    //---------------------------------------------------
    fun appendToCaseNote(content: String, authorId: String, parentNoteId: Long){
        val cmd = AppendToCaseNote(content, authorId, parentNoteId, thisCaseId)
        caseCtrl.onCommand(cmd)
    }

    //---------------------------------------------------
    fun assertCaseLog(expecteds: String){

        val entries = logRepo.findByCaseId(thisCaseId)
        val expectedArr = expecteds.split(",").map { it.trim() }
        val actuals = entries.map{"${it.nodeId?:""}:${it.eventName}:${message(it)}"}

        //println("********\n"+actuals.joinToString (separator = ", " ))

        assertEquals(expectedArr.size, actuals.size)
        val pairs = expectedArr.zip(actuals)
        pairs.forEach{ assertEquals(it.first, it.second)}

    }

    fun message(entry: LogEntry): String? {
        var message = if(entry.toNodeId!=null){ entry.toNodeId }
        else if(entry.guard!=null){ entry.guard}
        else ""

        return message
    }

    //----------------------------------------
    fun getAllTasks(): List<TaskTO>  = taskRepo.findAll().map{ TaskTOMapper.makeTO(it)}
    fun getAllTokens(): List<Token> = tokenRepo.findAll().toList()
    fun getAllCases(): List<Case> = caseRepo.findAll().map{ CaseTOMapper.makeTO(it) }
    fun getAllCaseBatches(): List<CaseBatch> = caseBatchRepo.findAll().map { CaseBatchTOMapper.makeTO(it)}
}
