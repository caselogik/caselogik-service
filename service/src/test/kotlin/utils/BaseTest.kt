package utils

import junit.framework.TestCase.assertEquals
import org.caselogik.service.Application
import org.caselogik.service.enactment.events.EventSender
import org.caselogik.service.enactment.gateways.ExecutionGateway
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(Application::class, TestConfig::class))
abstract class BaseTest {

    @Autowired
    lateinit var eventSender: EventSender

    @Autowired
    lateinit var execGateway: ExecutionGateway

    //-----------------------------------------
    fun pause(secs: Int) {
        Thread.sleep(secs*1000L)
    }

    //------------------------------------
    //overriden in BaseScenarioTest
    protected fun _beforeEachTest(){}

    @Before
    fun beforeEachTest() {
        //val es = eventSender as TestEventSender
        val egw = execGateway as TestExecutionGateway
        egw.loggedRequests.clear()
        _beforeEachTest()
    }


    //-------------------------------------------
    fun assertExecRequests(expected: List<String>){
        val egw = execGateway as TestExecutionGateway
        val actual = egw.loggedRequests
        assertEquals(expected.joinToString(", "), actual.joinToString(", "))
    }
}
