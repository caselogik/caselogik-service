package utils

import org.caselogik.service.enactment.util.cluster.ClusterStateEvaluator

class TestClusterStateEvaluator : ClusterStateEvaluator {
    override fun evaluateClusterState() { }
    override val isSingleton: Boolean = true;
}