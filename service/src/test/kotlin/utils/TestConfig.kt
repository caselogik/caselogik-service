package utils

import org.caselogik.service.enactment.controllers.*
import org.caselogik.service.enactment.events.EventSender
import org.caselogik.service.enactment.gateways.ExecutionGateway
import org.caselogik.service.enactment.logging.Logger
import org.caselogik.service.enactment.util.cluster.ClusterStateEvaluator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import javax.annotation.PostConstruct


@Configuration
class TestConfig {


    //----------------------------------------------------
    private lateinit var eventSender: TestEventSender

    @Bean
    @Primary
    fun eventSender(): EventSender {
        eventSender = TestEventSender()
        return eventSender
    }

    @Autowired
    private lateinit var eventReceiver: TestEventReceiver

    //-----------------------------------
    @PostConstruct
    fun init() {
        eventSender.eventReceiver = eventReceiver
    }

    //----------------------------------------------------
    @Bean
    @Primary
    fun eventReceiver(caseCtrl: CaseCtrl, caseBatchCtrl: CaseBatchCtrl, taskCtrl: TaskCtrl, tokenCtrl: TokenCtrl, processCtrl: ProcessCtrl, logger: Logger): TestEventReceiver {
        return TestEventReceiver(caseCtrl, caseBatchCtrl, taskCtrl, tokenCtrl, processCtrl, logger)
    }

    //-------------------------------------
    @Bean
    @Primary
    fun execGateway(): ExecutionGateway {
        return TestExecutionGateway()
    }

    @Bean
    @Primary
    fun clusterStateEvaluator(): ClusterStateEvaluator {
        return TestClusterStateEvaluator()
    }
}