package utils

import org.caselogik.api.Event
import org.caselogik.api.ExternalEvent
import org.caselogik.service.enactment.controllers.*
import org.caselogik.service.enactment.events.*
import org.caselogik.service.enactment.logging.Logger
import java.lang.Exception

class TestEventSender(): EventSender {

    lateinit var eventReceiver: TestEventReceiver

    val eventLog = mutableListOf<Event>()
    override fun send(event: Event) {
        eventLog.add(event)
        eventReceiver.receive(event)
    }

}
//====================================
class TestEventReceiver (
        private val caseCtrl: CaseCtrl,
        private val caseBatchCtrl: CaseBatchCtrl,
        private val taskCtrl: TaskCtrl,
        private val tokenCtrl: TokenCtrl,
        private val processCtrl: ProcessCtrl,
        private val logger: Logger
        ): EventReceiver {

    //---------------------------------
    override fun receive(event: Event) {

        try {
            when (event) {
                is CaseEvent -> caseCtrl.loadEvent(event)
                is CaseBatchEvent -> caseBatchCtrl.loadEvent(event)
                is TaskEvent -> taskCtrl.loadEvent(event)
                is TokenRoutingEvent -> tokenCtrl.loadEvent(event)
                else -> println("Not loading event $event")
            }

            logger.logEvent(event)

            when (event) {
                is CaseEvent -> caseCtrl.onEvent(event)
                is CaseBatchEvent -> caseBatchCtrl.onEvent(event)
                is TaskEvent -> taskCtrl.onEvent(event)
                is ExternalEvent -> processCtrl.onEvent(event)
                else -> println("Not handling event $event")
            }
        }
        catch(e: Exception){
            e.printStackTrace()
        }


    }

}