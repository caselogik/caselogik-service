package utils

import org.caselogik.service.enactment.gateways.ExecutionGateway
import org.caselogik.service.enactment.gateways.ExecutionStatusUpdateListener
import org.caselogik.service.enactment.mappers.TaskTOMapper
import org.caselogik.service.enactment.entities.Task

class TestExecutionGateway : ExecutionGateway {


    //-------------------------------------------
    override fun setExecStatusUpdateListener(listener: ExecutionStatusUpdateListener) {
    }

    val loggedRequests = ArrayList<String>()

    //-----------------------------------
    override fun executeTask(task: Task) {

        val to = TaskTOMapper.makeTO(task)
        loggedRequests.add("command='execute', url='${to.executionUrl}', taskNode='${to.taskNodeId}', workDataType='${to.workDataType}'")
    }

    //-----------------------------------
    override fun retryFailedTask(task: Task) {
        val to = TaskTOMapper.makeTO(task)
        loggedRequests.add("command='retry', url='${to.executionUrl}', taskNode='${task.taskNodeId}', workDataType='${task.workDataType()}'")
    }

}