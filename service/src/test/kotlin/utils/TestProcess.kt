package utils

import org.caselogik.definition.dsl.ScriptSource
import org.caselogik.definition.dsl.TestScriptSource

class TestProcess(val id: String, private val startWi: String) {

    private var startNode = ""
    init {
        startNode = """
        start {
            workItem = $startWi
        }
        """.trimIndent()

    }

    private val workItems: MutableList<String> = mutableListOf("""
        class SimpleWork implements WorkItem {
            String key
            boolean ok
        }
        class Payment implements WorkItem {
            String key
            String bankCode
            float amount
        }
        class PaymentBatch implements WorkBatch {
            String key
            List<Payment> items
        }
        """.trimIndent())

    private var nodes = ""

    private var flow = ""

    private val variables = HashMap<String, String>()

    private val swimlanes = """
        swimlanes {
            swimlane('default')
        }
        """

    //--------------------------------
    fun withNodes(nodes: String): TestProcess {
        this.nodes += nodes + "\n"
        return this
    }

    fun withFlow(flow: String): TestProcess {
        this.flow = flow
        return this
    }

    fun build(): ScriptSource {
        val builder = StringBuilder()
        builder.append("package ").append(id).append("\n")
        builder.append("import org.caselogik.api.WorkItem").append("\n")
               .append("import org.caselogik.api.WorkBatch").append("\n\n")

        for(wi in workItems){
            builder.append(wi)
        }
        builder.append("\nprocess('").append(id).append("') {\n")
        builder.append(swimlanes)
        builder.append("nodes {\n")
        builder.append(startNode).append("\n")
        builder.append(nodes.trimIndent()).append("\n")
        builder.append("}")
        builder.append("\nflow {\n")
        builder.append(flow.trimIndent()).append("\n")
        builder.append("}\n")
        builder.append("}\n")
        val procStr = builder.toString()
        return TestScriptSource(procStr, "TestProcess")
    }



}

